/*package com.smartdocs.smartportal.web.rest;

import com.smartdocs.smartportal.SmartportalApp;

import com.smartdocs.smartportal.domain.Documents;
import com.smartdocs.smartportal.repository.DocumentsRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

*//**
 * Test class for the DocumentsResource REST controller.
 *
 * @see DocumentsResource
 *//*
@RunWith(SpringRunner.class)
@SpringBootTest(classes = SmartportalApp.class)
public class DocumentsResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_SITE_ID = "AAAAAAAAAA";
    private static final String UPDATED_SITE_ID = "BBBBBBBBBB";

    private static final String DEFAULT_PARENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_PARENT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_MIME_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_MIME_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_SIZE = "AAAAAAAAAA";
    private static final String UPDATED_SIZE = "BBBBBBBBBB";

    private static final String DEFAULT_ARCHIVE_ID = "AAAAAAAAAA";
    private static final String UPDATED_ARCHIVE_ID = "BBBBBBBBBB";

    private static final String DEFAULT_ARCHIVE_DOC_ID = "AAAAAAAAAA";
    private static final String UPDATED_ARCHIVE_DOC_ID = "BBBBBBBBBB";

    @Autowired
    private DocumentsRepository documentsRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    private MockMvc restDocumentsMockMvc;

    private Documents documents;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
            DocumentsResource documentsResource = new DocumentsResource(documentsRepository);
        this.restDocumentsMockMvc = MockMvcBuilders.standaloneSetup(documentsResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    *//**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     *//*
    public static Documents createEntity() {
        Documents documents = new Documents()
                .name(DEFAULT_NAME)
                .title(DEFAULT_TITLE)
                .description(DEFAULT_DESCRIPTION)
                .type(DEFAULT_TYPE)
                .siteId(DEFAULT_SITE_ID)
                .parentId(DEFAULT_PARENT_ID)
                .mimeType(DEFAULT_MIME_TYPE)
                .size(DEFAULT_SIZE)
                .archiveId(DEFAULT_ARCHIVE_ID)
                .archiveDocId(DEFAULT_ARCHIVE_DOC_ID);
        return documents;
    }

    @Before
    public void initTest() {
        documentsRepository.deleteAll();
        documents = createEntity();
    }

    @Test
    public void createDocuments() throws Exception {
        int databaseSizeBeforeCreate = documentsRepository.findAll().size();

        // Create the Documents

        restDocumentsMockMvc.perform(post("/api/documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documents)))
            .andExpect(status().isCreated());

        // Validate the Documents in the database
        List<Documents> documentsList = documentsRepository.findAll();
        assertThat(documentsList).hasSize(databaseSizeBeforeCreate + 1);
        Documents testDocuments = documentsList.get(documentsList.size() - 1);
        assertThat(testDocuments.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDocuments.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testDocuments.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testDocuments.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testDocuments.getSiteId()).isEqualTo(DEFAULT_SITE_ID);
        assertThat(testDocuments.getParentId()).isEqualTo(DEFAULT_PARENT_ID);
        assertThat(testDocuments.getMimeType()).isEqualTo(DEFAULT_MIME_TYPE);
        assertThat(testDocuments.getSize()).isEqualTo(DEFAULT_SIZE);
        assertThat(testDocuments.getArchiveId()).isEqualTo(DEFAULT_ARCHIVE_ID);
        assertThat(testDocuments.getArchiveDocId()).isEqualTo(DEFAULT_ARCHIVE_DOC_ID);
    }

    @Test
    public void createDocumentsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = documentsRepository.findAll().size();

        // Create the Documents with an existing ID
        Documents existingDocuments = new Documents();
        existingDocuments.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restDocumentsMockMvc.perform(post("/api/documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(existingDocuments)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Documents> documentsList = documentsRepository.findAll();
        assertThat(documentsList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllDocuments() throws Exception {
        // Initialize the database
        documentsRepository.save(documents);

        // Get all the documentsList
        restDocumentsMockMvc.perform(get("/api/documents?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(documents.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].siteId").value(hasItem(DEFAULT_SITE_ID.toString())))
            .andExpect(jsonPath("$.[*].parentId").value(hasItem(DEFAULT_PARENT_ID.toString())))
            .andExpect(jsonPath("$.[*].mimeType").value(hasItem(DEFAULT_MIME_TYPE.toString())))
            .andExpect(jsonPath("$.[*].size").value(hasItem(DEFAULT_SIZE.toString())))
            .andExpect(jsonPath("$.[*].archiveId").value(hasItem(DEFAULT_ARCHIVE_ID.toString())))
            .andExpect(jsonPath("$.[*].archiveDocId").value(hasItem(DEFAULT_ARCHIVE_DOC_ID.toString())));
    }

    @Test
    public void getDocuments() throws Exception {
        // Initialize the database
        documentsRepository.save(documents);

        // Get the documents
        restDocumentsMockMvc.perform(get("/api/documents/{id}", documents.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(documents.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.siteId").value(DEFAULT_SITE_ID.toString()))
            .andExpect(jsonPath("$.parentId").value(DEFAULT_PARENT_ID.toString()))
            .andExpect(jsonPath("$.mimeType").value(DEFAULT_MIME_TYPE.toString()))
            .andExpect(jsonPath("$.size").value(DEFAULT_SIZE.toString()))
            .andExpect(jsonPath("$.archiveId").value(DEFAULT_ARCHIVE_ID.toString()))
            .andExpect(jsonPath("$.archiveDocId").value(DEFAULT_ARCHIVE_DOC_ID.toString()));
    }

    @Test
    public void getNonExistingDocuments() throws Exception {
        // Get the documents
        restDocumentsMockMvc.perform(get("/api/documents/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateDocuments() throws Exception {
        // Initialize the database
        documentsRepository.save(documents);
        int databaseSizeBeforeUpdate = documentsRepository.findAll().size();

        // Update the documents
        Documents updatedDocuments = documentsRepository.findOne(documents.getId());
        updatedDocuments
                .name(UPDATED_NAME)
                .title(UPDATED_TITLE)
                .description(UPDATED_DESCRIPTION)
                .type(UPDATED_TYPE)
                .siteId(UPDATED_SITE_ID)
                .parentId(UPDATED_PARENT_ID)
                .mimeType(UPDATED_MIME_TYPE)
                .size(UPDATED_SIZE)
                .archiveId(UPDATED_ARCHIVE_ID)
                .archiveDocId(UPDATED_ARCHIVE_DOC_ID);

        restDocumentsMockMvc.perform(put("/api/documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDocuments)))
            .andExpect(status().isOk());

        // Validate the Documents in the database
        List<Documents> documentsList = documentsRepository.findAll();
        assertThat(documentsList).hasSize(databaseSizeBeforeUpdate);
        Documents testDocuments = documentsList.get(documentsList.size() - 1);
        assertThat(testDocuments.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDocuments.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testDocuments.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testDocuments.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testDocuments.getSiteId()).isEqualTo(UPDATED_SITE_ID);
        assertThat(testDocuments.getParentId()).isEqualTo(UPDATED_PARENT_ID);
        assertThat(testDocuments.getMimeType()).isEqualTo(UPDATED_MIME_TYPE);
        assertThat(testDocuments.getSize()).isEqualTo(UPDATED_SIZE);
        assertThat(testDocuments.getArchiveId()).isEqualTo(UPDATED_ARCHIVE_ID);
        assertThat(testDocuments.getArchiveDocId()).isEqualTo(UPDATED_ARCHIVE_DOC_ID);
    }

    @Test
    public void updateNonExistingDocuments() throws Exception {
        int databaseSizeBeforeUpdate = documentsRepository.findAll().size();

        // Create the Documents

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDocumentsMockMvc.perform(put("/api/documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(documents)))
            .andExpect(status().isCreated());

        // Validate the Documents in the database
        List<Documents> documentsList = documentsRepository.findAll();
        assertThat(documentsList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteDocuments() throws Exception {
        // Initialize the database
        documentsRepository.save(documents);
        int databaseSizeBeforeDelete = documentsRepository.findAll().size();

        // Get the documents
        restDocumentsMockMvc.perform(delete("/api/documents/{id}", documents.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Documents> documentsList = documentsRepository.findAll();
        assertThat(documentsList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Documents.class);
    }
}
*/