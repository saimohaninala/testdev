package com.smartdocs.smartportal.smartstore;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.Iterator;
import java.util.Optional;

import javax.inject.Inject;

import org.springframework.stereotype.Component;

import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.service.RepositoryService;
import com.smartdocs.smartportal.service.impl.RepositoryServiceImpl;
import com.smartdocs.smartportal.web.rest.AdminAccessFunctions;

public class FileSystemAPI extends AdminAccessFunctions{
	
	String rootPath=null;
	String company,  system,  contRep;
	Repository repository;
	@Inject
	RepositoryService repositoryService;
	


	/*public FileSystemAPI(String company, String system, String contRep) {
		super();
		this.company = company;
		this.system = system;
		this.contRep = contRep;
	}*/
	
/*	public FileSystemAPI( String system, String contRep) {
		super();
		
		this.system = system;
		this.contRep = contRep;
	}
	*/
	
	
	
	public FileSystemAPI( Repository repository) {
		super();
		this.repository=repository;
		
	}
	

	@Override
	public String putCert(String repositoryId,String authId, InputStream is,String rootPath) throws Exception {
		// TODO Auto-generated method stub
		//String rootPath	="G:\\SmartStoreRep";
		
		URL url=createFile(rootPath+"/"+"certificate", authId,is);
		return "success";
	}

	@Override
	public byte[] getDocInputStreamURL(String repositoryName, String docId, String compId,String rootPath)
			throws IOException {

	    InputStream in=null;
	    byte[] b=null;
	 try {
		 File f= new File(rootPath+File.separator+"smartstore"+File.separator+repositoryName+File.separator+docId+File.separator+compId);
		 in=new FileInputStream(f);
		 b=new byte[in.available()];
		 in.read(b);
		 
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		b=new byte[0];
	}finally{
		
		if(null!=in)
			in.close();
	}
	//byte[] b=new byte[in.available()];
	 return b;
 	}

	
	@Override
	public File getDocResourcePath(String repositoryName,String systemId, String docId, String compId,String rootPath)
			throws IOException {
		File f=null;
	 f= new File("smartstore"+File.separator+systemId+File.separator+repositoryName+File.separator+docId+File.separator+compId);
		 return f;
 	}
	
	@Override
	public File getDocPath(String repositoryName,String systemId, String docId, String compId,String rootPath)
			throws IOException {
		File f=null;
	 f= new File(rootPath+File.separator+"smartstore"+File.separator+systemId+File.separator+repositoryName+File.separator+docId+File.separator+compId);
		 return f;
 	}
	
	
	@Override
	public long getDocContentLength(String repositoryName,String systemId, String docId, String compId,String rootPath)
			throws IOException {

		long fileSize = 0;
	    byte[] b=null;
	 try {
		 File f= new File(rootPath+File.separator+"smartstore"+File.separator+systemId+File.separator+repositoryName+File.separator+docId+File.separator+compId);
		 System.out.println("in getdocs method1111------"+f.getPath());
		 Path filePath = Paths.get(f.getPath());
		 FileChannel fileChannel=null;
		 fileChannel = FileChannel.open(filePath);
		  fileSize = fileChannel.size();
			System.out.println(fileSize + " bytes");
			fileChannel.close();
		 System.out.println("ended-----");
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		b=new byte[0];
	}
	 return fileSize;
 	}
	
	
	
	@Override
	public boolean appendDocFile(InputStream inputStream,  String repoName, String docId,
			String compId, String filEextension) throws IOException {
		 // String filename= "data"; 
		  boolean append = false;
		  OutputStream out = null;
		try{
		 out= new FileOutputStream(rootPath+File.separator+"smartstore"+File.separator+repoName+File.separator+docId+File.separator+compId+filEextension.trim(),true);
		
	    byte[] buffer = new byte[4096];  
	    for (int n; (n = inputStream.read(buffer)) != -1; )   
	        out.write(buffer, 0, n);
	   
		    //FileWriter fw = new FileWriter(SSConstants.rootpath+"/"+companyName+"/smartstore/"+repoName+"/"+docId+"/"+compId,true);
	    append=true;
		}catch(Exception e){
			
		}finally {
		    if (out != null) {
		        out.flush();
		        out.close();
		    }
		}
		   
		
		return append;
}

	@Override
	public boolean DeleteDoc( String repoName, String docId, String compId,String rootPath) {
		 File dir=null;
		 boolean flag=false;
		 if(null==compId || compId.trim().length()==0){
		   dir=new File(rootPath+File.separator+"smartstore"+File.separator+repoName+File.separator+docId);
		   flag=deleteDir(dir);
		 }else{
			 flag=deleteComponent(repoName,docId,compId,rootPath);
		 }
		 return flag;
		
	 }

	@Override
	public URL createcomponent(String contentType, String repoName, String docId, String fileName,
			InputStream in,String rootPath) throws Exception {
		boolean success = false;
		File component=null;
		 URL url = null;
		 OutputStream out=null;
		 
		 System.out.println("fileName:"+fileName);
		 
		 
		try {
			(new File(rootPath+File.separator+"smartstore"+File.separator+repoName+File.separator+docId)).mkdirs();
			if(fileName.contains("/")){
				fileName=fileName.replaceAll("/", "&#47;");
			}
			component=new File(rootPath+File.separator+"smartstore"+File.separator+repoName+File.separator+docId+File.separator+fileName);
			
			success=component.createNewFile();
			
			if (success) {
				
				      out = new FileOutputStream(component);
				
			        byte[] buffer = new byte[4096];  
			        for (int n; (n = in.read(buffer)) != -1; )   
			            out.write(buffer, 0, n);
			   
				System.out.println("File did not exist and was created");
				 url= component.toURI().toURL();
			} else {
				System.out.println("File already exists");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
		    if (out != null) {
		        out.flush();
		        out.close();
		    }
		}
		return url;
	 
 }

	@Override
	public boolean updateDocFile(InputStream inputStream, String companyName, String repoName, String docId,
			String compId, String ContentType) throws IOException {

	
boolean flag=false;

	DataOutputStream outs;
	try {
		File file=new File(rootPath+File.separator+companyName+File.separator+"smartstore"+File.separator+repoName+File.separator+docId+File.separator+compId);

		file.createNewFile();
		outs = new DataOutputStream(new FileOutputStream(file,false));
		
		outs.write(convertStreamToString(inputStream).getBytes());
		outs.close();
		flag=true;
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}


	return flag;
}

	@Override
	public String uploadDocFile2(InputStream inputStream, String ContentType, URL destFolderUri, String compId,
			String docId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] getComponentInputStreamUsingResourceIdExt(String resourceId, String extention, String docId,
			String compId) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteDocumentOrComponent(String resourceId) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getRepositoryResourceIdUnderSmartStore(String companyName, String repositoryName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDocumentResourceIdUnderRepository(String resourceId, String docId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public URL createcomponent(String contentType, String resourceId, String fileName, InputStream in)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	
	public URL createFile(String FoldersStructure ,String fileName ,InputStream in){
		boolean success = false;
		File certificate=null;
		 URL url = null;
		 OutputStream out=null;
		try {
			(new File(FoldersStructure)).mkdirs();

			certificate=new File(FoldersStructure+File.separator+fileName);
			success=certificate.createNewFile();
			
				System.out.println(url);
			if (success) {
				
			        out = new FileOutputStream(certificate);
				
			        byte[] buffer = new byte[4096];  
			        for (int n; (n = in.read(buffer)) != -1; )   
			            out.write(buffer, 0, n);
			      //  URL u=certificate.toURL();
			        url= certificate.toURI().toURL();
			      
				System.out.println("File did not exist and was created");  
			} else {
				System.out.println("File already exists");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
		    if (out != null) {
		        try {
					out.flush();
					 out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       
		    }
		}


		return url;
	}
	
	public boolean upload(String FoldersStructure ,String fileName ,InputStream in){
		boolean success = false;
		File certificate=null;
		 URL url = null;
		 OutputStream out=null;
		try {
			(new File(FoldersStructure)).mkdirs();

			certificate=new File(FoldersStructure+File.separator+fileName);
			success=certificate.createNewFile();
			
			
			if (success) {
				
			        out = new FileOutputStream(certificate);
				
			        byte[] buffer = new byte[4096];  
			        for (int n; (n = in.read(buffer)) != -1; )   
			            out.write(buffer, 0, n);
			      //  URL u=certificate.toURL();
			     
			      
				System.out.println("File did not exist and was created");  
			} else {
				System.out.println("File already exists");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
		    if (out != null) {
		        try {
					out.flush();
					 out.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       
		    }
		}


		return success;
	}

	 public  X509Certificate getCertificateFromFileSystem(String  companyName,String authId) throws IOException{
			InputStream in=null;
			X509Certificate certificate = null;
			try{
				 File file = new File(rootPath+File.separator+companyName+File.separator+"certificate"+File.separator+authId);
				
				    in = new FileInputStream(file);

				CertificateFactory factory = CertificateFactory
				.getInstance("X.509");
				Collection collect = factory.generateCertificates(in);
				
				for (Iterator i = collect.iterator(); i.hasNext();) {
					certificate = (X509Certificate) i.next();
				}	
			}catch(Exception exp){
				exp.printStackTrace();	
			}finally {
			    if (in != null) {
			      
			        in.close();
			    }
			}
			return certificate;							 
		}
	 
	 public boolean deleteComponent( String repoName,String docId,String compId,String rootPath) {
		 
	
		 
	      File file=new File(rootPath+File.separator+"smartstore"+File.separator+repoName+File.separator+docId+File.separator+compId);
		
		 if(file.exists()){
		 file.deleteOnExit();
		 return true;
		 }else{
			 return false;
		 }
   }
	 
	 
	 public boolean deleteDir(File dir) {
		 
		 if (dir.isDirectory()) {
		 String[] children = dir.list();
		 for (int i=0; i<children.length; i++) {
			 File childFile=new File(dir, children[i]);
			 boolean success =false;
			 if(childFile.isDirectory()){
				  success = deleteDir(childFile);
			 }else
				 success =childFile.delete(); 
		 /*if (!success) {
		 return false;
		 }*/
		 }
		 }

		 // The directory is now empty so delete it
		 return dir.delete();
		 }
 public boolean checkDocument(String path){
		 
		 return (new File(path).isDirectory());
		 
		
		 
	 }
 
 public static String convertStreamToString(InputStream is) {
	 try{
	    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
	    StringBuilder sb = new StringBuilder();
	    String line = null;
	    while ((line = reader.readLine()) != null) {
	      sb.append(line + "\n");
	    }
	    is.close();
	    return sb.toString();
	  }
	 catch (Exception e) {
		// TODO: handle exception
	}
	 return null;
 }


@Override
public String uploadFile(String smartstore, String systemId, String archiveId, String archiveDocId, String name,
		String mimeType, byte[] body) throws IOException {
	// TODO Auto-generated method stub
	String path=repository.getRootPath()+File.separator+smartstore+File.separator+systemId+File.separator+archiveId+File.separator+archiveDocId;
	if(upload(path, name, new ByteArrayInputStream(body))){
		return smartstore+File.separator+systemId+File.separator+archiveId+File.separator+archiveDocId+File.separator+name;
	}else{
		return null;
		
	}
}


@Override
public byte[] downFile(String path) {
	// TODO Auto-generated method stub
	String filePath=repository.getRootPath()+File.separator+path;
	
	return readBytesFromFile(filePath);
}
private static byte[] readBytesFromFile(String filePath) {

    FileInputStream fileInputStream = null;
    byte[] bytesArray = null;

    try {

        File file = new File(filePath);
        bytesArray = new byte[(int) file.length()];

        //read file into bytes[]
        fileInputStream = new FileInputStream(file);
        fileInputStream.read(bytesArray);

    } catch (IOException e) {
        e.printStackTrace();
    } finally {
        if (fileInputStream != null) {
            try {
                fileInputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    return bytesArray;

}
}
