package com.smartdocs.smartportal.smartstore;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.google.api.client.http.GenericUrl;
import com.smartdocs.smartportal.config.JHipsterProperties;
import com.smartdocs.smartportal.domain.OAuthConstants;
import com.smartdocs.smartportal.domain.OAuthUserConfig;
import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.smartstore.onedrive.OneDriveStringResponseHandler;
import com.smartdocs.smartportal.web.rest.util.HttpClientUtil;



@Component
public class OneDriveOauthUtils {

	
	public String authorize(HttpServletRequest httpServletRequest) {
		
		System.out.println("OneDriveOauthUtils Class Calling");
		
		String url = new StringBuilder(OAuthConstants.ONEDRIVE_AUTHORIZATION_URL).append("?client_id=")
				.append(OAuthUserConfig.ONEDRIVE_CLIENT_ID_VALUE).append("&").append(OAuthConstants.REDIRECT_URI)
				.append("=").append(getRedirectUri(httpServletRequest)).append("&").append(OAuthConstants.RESPONSE_TYPE)
				.append("=code").append("&").append(OAuthConstants.STATE).append("=dev").toString();
		return url;
	}
public static  String authorize(Repository repository) {
		
		System.out.println("OneDriveOauthUtils Class Calling");
		
		String url = new StringBuilder(OAuthConstants.ONEDRIVE_AUTHORIZATION_URL).append("?client_id=")
				.append(repository.getClientId()).append("&").append(OAuthConstants.REDIRECT_URI)
				.append("=").append(repository.getRootPath()).append("&").append(OAuthConstants.RESPONSE_TYPE)
				.append("=code").append("&").append(OAuthConstants.STATE).append("="+repository.getId()).toString();
		return url;
	}
	
	protected String getRedirectUri(HttpServletRequest req) {
		GenericUrl url = new GenericUrl(req.getRequestURL().toString());
		url.setRawPath("/api/onedrive/redirect");
		return url.build();
	}

	
	public JSONObject getAccessToken(Repository repository,String code,HttpServletResponse httpServletResponse,String redirectUri)
			throws JSONException {
		String requestBody = new StringBuilder(OAuthConstants.CLIENT_ID).append("=")
				.append(repository.getClientId()).append("&").append(OAuthConstants.CLIENT_SECRET)
				.append("=").append(repository.getClientSecret()).append("&")
				.append(OAuthConstants.CODE).append("=").append(code).append("&").append(OAuthConstants.REDIRECT_URI)
				.append("=").append(redirectUri).append("&").append(OAuthConstants.RESOURCE).append("=")
				.append(OAuthConstants.ONEDRIVE_RESOURCE_URL)

				.append("&").append(OAuthConstants.GRANT_TYPE).append("=authorization_code").toString();
		
		System.out.println(requestBody);
/*
		HttpPost httpPost = new HttpPost(OAuthConstants.ONEDRIVE_ACCESS_TOKEN_URL);
		ByteArrayEntity byteArrayEntity = new ByteArrayEntity(requestBody.getBytes(),
				ContentType.APPLICATION_FORM_URLENCODED);
		httpPost.setEntity(byteArrayEntity);
		System.out.println("mmmmmmmmmmmmmmmmmmmm");

		HttpResponse httpResponse = HttpClientUtil.executePost(httpPost);
		JSONObject jsonObject = null;
		try {
			if (httpResponse.getStatusLine().getStatusCode() != 200) {
				IOUtils.copy(httpResponse.getEntity().getContent(), httpServletResponse.getOutputStream());
			} else {

				String response = IOUtils.toString(httpResponse.getEntity().getContent());
				jsonObject = new JSONObject(response);
				System.out.println(jsonObject);

			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
*/
		JSONObject jsonObject=null;
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpPost httpPost = new HttpPost(OAuthConstants.ONEDRIVE_ACCESS_TOKEN_URL);
			ByteArrayEntity byteArrayEntity = new ByteArrayEntity(requestBody.getBytes(),
					ContentType.APPLICATION_FORM_URLENCODED);
			httpPost.setEntity(byteArrayEntity);
	

			
			String rawResponse = httpClient.execute(httpPost, new OneDriveStringResponseHandler());
			System.out.println(rawResponse);
			jsonObject = new JSONObject(rawResponse);
			System.out.println(jsonObject);

		

		} catch (Exception e) {
			e.printStackTrace();
			
		}

		
		
		return jsonObject;
	}
	
	public static JSONObject refreshToken(Repository repository,String refreshToken,String redirectURL)
			throws JSONException {
		String requestBody = new StringBuilder(OAuthConstants.CLIENT_ID).append("=")
				.append(repository.getClientId()).append("&").append(OAuthConstants.CLIENT_SECRET)
				.append("=").append(repository.getClientSecret()).append("&")
				.append(OAuthConstants.REFRESH_TOKEN).append("=").append(refreshToken).append("&").append(OAuthConstants.REDIRECT_URI)
				.append("=").append(redirectURL).append("&").append(OAuthConstants.RESOURCE).append("=")
				.append(OAuthConstants.ONEDRIVE_RESOURCE_URL)

				.append("&").append(OAuthConstants.GRANT_TYPE).append("=").append(OAuthConstants.REFRESH_GRANT_TYPE).toString();
		
		System.out.println(requestBody.toString());

		HttpPost httpPost = new HttpPost(OAuthConstants.ONEDRIVE_ACCESS_TOKEN_URL);
		ByteArrayEntity byteArrayEntity = new ByteArrayEntity(requestBody.getBytes(),
				ContentType.APPLICATION_FORM_URLENCODED);
		httpPost.setEntity(byteArrayEntity);
		HttpResponse httpResponse = HttpClientUtil.executePost(httpPost);
		JSONObject jsonObject = null;
		try {
			if (httpResponse.getStatusLine().getStatusCode() != 200) {
				//IOUtils.copy(httpResponse.getEntity().getContent(), httpServletResponse.getOutputStream());
			} else {
				String response = IOUtils.toString(httpResponse.getEntity().getContent());
				jsonObject = new JSONObject(response);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		return jsonObject;
	}
	
	
}
