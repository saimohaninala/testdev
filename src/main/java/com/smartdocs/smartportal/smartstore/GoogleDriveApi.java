package com.smartdocs.smartportal.smartstore;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.api.client.http.AbstractInputStreamContent;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.smartdocs.smartportal.domain.GdriveToken;
import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.repository.GdriveTokenRepository;
import com.smartdocs.smartportal.web.rest.AdminAccessFunctions;
import com.smartdocs.smartportal.web.rest.util.SSConstants;


public class GoogleDriveApi extends AdminAccessFunctions {
	@Inject
	public  GdriveToken gdriveToken;

	public Drive gDriveclient = null;
	// GoogleDriveUtils googleDriveUtils=null;
	public Logger log = LoggerFactory.getLogger("" + GoogleDriveApi.class);

	/*
	 * public GoogleDriveApi(String systemId, String contRepo) { super();
	 * 
	 * if (null != systemId && null != contRepo) { try { this.gDriveclient =
	 * GdriveUtils.loadDriveClient(systemId, contRepo);
	 * 
	 * // System.out.println("loading googledriveclient........."); } catch
	 * (Exception e) { e.printStackTrace(); } } }
	 */
	Repository repository;
	GdriveTokenRepository gdriveTokenRepository;
	
	public GoogleDriveApi(Repository repository ,GdriveTokenRepository gdriveTokenRepository) throws IOException {
		this.repository=repository;
		this.gdriveTokenRepository=gdriveTokenRepository;
		this.gDriveclient = GdriveUtils.loadDriveClient(repository,gdriveTokenRepository);
		

	}

	public Drive getDriveClient() {

		return gDriveclient;
	}

	@Override
	public String putCert(String contRep, String authId, InputStream is, String rootPath) throws Exception {
		// TODO Auto-generated method stub

		com.google.api.services.drive.model.File CompanyFolderEntry = null;
		String CompanyResourceId = getResourceID("Smartdocs", "folder");

		if (CompanyResourceId == null) {
			CompanyFolderEntry = createFolder("Smartdocs");
			CompanyResourceId = CompanyFolderEntry.getId();

		}
		com.google.api.services.drive.model.File CertificateEntry = null;
		String CertificateResourceId = getResourceIDUnderParent(CompanyResourceId, SSConstants.SS_CERTIFICATE_NAME);
		log.info("Certificate Resource Id=" + CertificateResourceId);
		if (null == CertificateResourceId) {
			CertificateEntry = createSubFolders(SSConstants.SS_CERTIFICATE_NAME, CompanyResourceId);
			CertificateResourceId = CertificateEntry.getId();
			log.info("Certificate Resource Id=" + CertificateResourceId);
		}

		// checking the certificagte already exist or not
		String certResId = getResourceIDUnderParent(CertificateResourceId, authId);
		if (certResId != null) {
			log.info("Certificate already uploaded to the Gdrive");
			// response.setStatus(403,"Certificate already uploaded to the
			// Google App Engine");
			return "Certificate already uploaded to the Gdrive";
		}

		com.google.api.services.drive.model.File uploadedEntry = uploadDocFile(is, "binary/octet-stream",
				CertificateResourceId, authId);
		log.info("Uploadded the certificate to Gdrive sucessfully");
		log.info("Document now online @ :" + uploadedEntry.getId());
		return uploadedEntry.getId();

	}

	@Override
	public byte[] getDocInputStreamURL(String repositoryName, String docId, String compId, String rootPath)
			throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean appendDocFile(InputStream inputStream, String repoName, String docId, String compId,
			String ContentType) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean DeleteDoc(String repoName, String docId, String compId, String rootPath) {
		boolean flag = false;
		try {
			String parentId = getResourceIDUnderParent(getResourceID("Smartdocs", "folder"),
					SSConstants.SS_SMARTSTORE_NAME);
			String repoNameId = getResourceIDUnderParent(parentId, repoName);

			String docIdID = null;
			if (compId == null) {
				docIdID = getResourceIDUnderParent(repoNameId, docId);
			} else {
				String subparentId1 = getResourceIDUnderParent(repoNameId, docId);
				docIdID = getResourceIDUnderParent(subparentId1, compId);

			}
			getDriveClient().files().delete(docIdID).execute();
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return flag;
	}

	@Override
	public boolean updateDocFile(InputStream inputStream, String companyName, String repoName, String docId,
			String compId, String ContentType) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String uploadDocFile2(InputStream inputStream, String ContentType, URL destFolderUri, String compId,
			String docId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] getComponentInputStreamUsingResourceIdExt(String resourceId, String extention, String docId,
			String compId) throws IOException {
		// TODO Auto-generated method stub

		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		try {
			getDriveClient().files().get(resourceId).executeMediaAndDownloadTo(buffer);

		} catch (Exception exp) {
			// b=new byte[10];
			exp.printStackTrace();
		}

		return buffer.toByteArray();
	}

	@Override
	public boolean deleteDocumentOrComponent(String resourceId) throws IOException {
		// TODO Auto-generated method stub
		boolean flag = false;

		try {
			getDriveClient().files().delete(resourceId).execute();
			flag = true;
		} catch (Exception exp) {
			exp.printStackTrace();
		}
		return flag;
	}

	@Override
	public String getRepositoryResourceIdUnderSmartStore(String companyName, String repositoryName) {
		String repositoryResourceId = null;
		try {
			String companyresourceId = getResourceID(companyName, "folder");

			if (null == companyresourceId) {
				companyresourceId = createFolder(companyName).getId();
				// System.out.println("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^companyresourceId"+companyresourceId);
			}
			String smartStoreResourceId = getResourceIDUnderParent(companyresourceId, SSConstants.SS_SMARTSTORE_NAME);

			// System.out.println("}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}smartstoreResourceId1"+smartStoreResourceId);
			if (null == smartStoreResourceId) {
				smartStoreResourceId = createSubFolders(SSConstants.SS_SMARTSTORE_NAME, companyresourceId).getId();
			}
			// System.out.println("}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}}smartstoreResourceId2"+smartStoreResourceId);
			repositoryResourceId = getResourceIDUnderParent(smartStoreResourceId, repositoryName);
			System.out.println("repositoryResourceId:" + repositoryResourceId);

			if (null == repositoryResourceId) {
				repositoryResourceId = createSubFolders(repositoryName, smartStoreResourceId).getId();
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return repositoryResourceId;
	}

	@Override
	public String getDocumentResourceIdUnderRepository(String resourceId, String docId) {

		String docIdResourceId = null;
		try {

			docIdResourceId = getResourceIDUnderParent(resourceId, docId);
			if (null == docIdResourceId) {
				docIdResourceId = createSubFolders(docId, resourceId).getId();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return docIdResourceId;
	}

	public URL createcomponent(String contentType, String resourceId, String fileName, InputStream in)
			throws Exception {
		URL url = null;
		System.out.println("createcomponent::::::::::::" + contentType);
		try {
			log.info(contentType + " " + resourceId + " " + fileName);
			if (contentType == null || contentType.trim().length() == 0 || !fileName.contains("."))
				contentType = "binary/octet-stream";
			byte[] bufferByte = new byte[1];
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			while (in.read(bufferByte, 0, bufferByte.length) != -1) {
				buffer.write(bufferByte);
			}
			buffer.flush();
			log.info(contentType + " " + resourceId + " " + fileName);
			com.google.api.services.drive.model.File uploadedEntry = uploadDocFile(
					new ByteArrayInputStream(buffer.toByteArray()), contentType, resourceId, fileName);

			url = new URL(uploadedEntry.getWebContentLink() + ";act;" + uploadedEntry.getId());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return url;
	}

	public File uploadDocFile(final InputStream inputStream, String contentType, String parentId, String title) {

		try {
			log.info(contentType + " " + title + " " + parentId);

			com.google.api.services.drive.model.File body = new com.google.api.services.drive.model.File();

			body.setName(title);
			body.setMimeType(contentType);

			List<String> parents = Arrays.asList(parentId);

			body.setParents(parents);

			com.google.api.client.http.AbstractInputStreamContent in1 = new AbstractInputStreamContent(contentType) {

				@Override
				public boolean retrySupported() {
					// TODO Auto-generated method stub
					return false;
				}

				@Override
				public long getLength() throws IOException {
					// TODO Auto-generated method stub
					return inputStream.available();
				}

				@Override
				public InputStream getInputStream() throws IOException {
					// TODO Auto-generated method stub
					return inputStream;
				}
			};

			com.google.api.services.drive.model.File file = getDriveClient().files().create(body, in1)
					.setFields("id, webContentLink, webViewLink, parents").execute();
			// System.out.println("File ID:::::::::::::: " + file.getId());

			return file;
		} catch (Exception exp) {
			exp.printStackTrace();
		}
		return null;
	}

	public com.google.api.services.drive.model.File createFolder(String title) throws IOException {

		com.google.api.services.drive.model.File folderFile = new com.google.api.services.drive.model.File();
		folderFile.setName(title);
		// folderFile.setDescription("dfasdf");
		folderFile.setMimeType("application/vnd.google-apps.folder");
		com.google.api.services.drive.model.File file1 = getDriveClient().files().create(folderFile).execute();
		System.out.println("File ID: " + file1.getId());

		return file1;
	}

	public String getResourceID(String folderName, String type) throws Exception {

		String resourceId = null;
		try {
			FileList fileList = getDriveClient().files().list()
					.setQ("name = '" + folderName + "' and mimeType='application/vnd.google-apps.folder'").execute();
			java.util.List<com.google.api.services.drive.model.File> list = fileList.getFiles();

			System.out.println("comp;leted=" + list.size());
			for (com.google.api.services.drive.model.File file4 : list) {
				// System.out.println(file4.getId()+" "+file4.getMimeType()+"
				// "+file4.getDescription()+" "+file4.getDownloadUrl());
				resourceId = file4.getId();
			}

		} catch (IOException io) {
			try {
				FileList fileList = getDriveClient().files().list()
						.setQ("name = '" + folderName + "' and mimeType='application/vnd.google-apps.folder'")
						.execute();
				java.util.List<com.google.api.services.drive.model.File> list = fileList.getFiles();

				System.out.println("comp;leted=" + list.size());
				for (com.google.api.services.drive.model.File file4 : list) {

					System.out.println(file4.getId() + " " + file4.getMimeType() + " " + file4.getDescription() + " "
							+ file4.getWebContentLink());
					resourceId = file4.getId();
				}
			} catch (IOException io1) {

			}
		}
		return resourceId;
	}

	public String getResourceIDUnderParent(String parent, String folderName) throws Exception {
		String returnId = null;
		// type should be folder/file old one contents
		System.out.println(parent + "  " + folderName);
		try {// "'"+parent+"' in parents and title contains '"+folderName+"' and
				// trashed = false"
			com.google.api.services.drive.model.FileList parentFileList = getDriveClient().files().list()
					.setQ("'" + parent + "' in parents and trashed = false").execute();
			List<File> fileList = parentFileList.getFiles();
			for (File file : fileList) {
				System.out.println(file.getName());
				if (file.getName().equalsIgnoreCase(folderName)) {
					returnId = file.getId();
				}
			}
		} catch (Exception exp) {
			exp.printStackTrace();
		}
		return returnId;
	}

	public com.google.api.services.drive.model.File createSubFolders(String title, String CompanyResourceId)
			throws IOException {

		System.out.println("CompanyResourceId....createsubfolders......" + CompanyResourceId);
		com.google.api.services.drive.model.File body = new com.google.api.services.drive.model.File();

		body.setName(title);
		body.setMimeType("application/vnd.google-apps.folder");
		List<String> parents = Arrays.asList(CompanyResourceId);
		body.setParents(parents);

		com.google.api.services.drive.model.File file = getDriveClient().files().create(body).execute();

		return file;
	}

	public boolean checkFolder(String title) {
		boolean flag = false;

		return flag;
	}

	public boolean checkFolderUnderParent(String parentId, String title) {
		boolean flag = false;

		return flag;
	}

	public String checkAndCreateDocument(String companyName, String repoName, String docId) throws Exception {
		// Checking company Name directory. If not present create
		return null;
	}

	@Override
	public URL createcomponent(String contentType, String repoName, String docId, String fileName, InputStream in,
			String rootPath) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	/*
	 * @Override public String putCert(String authId, InputStream is) throws
	 * Exception { com.google.api.services.drive.model.File CompanyFolderEntry =
	 * null; String CompanyResourceId = getResourceID("Smartdocs","folder");
	 * log.info("' Resource Id="+CompanyResourceId ); if
	 * (CompanyResourceId==null) { CompanyFolderEntry =
	 * createFolder("Smartdocs"); CompanyResourceId =
	 * CompanyFolderEntry.getId(); log.info("' Resource Id="+CompanyResourceId
	 * +" is created successfully"); } com.google.api.services.drive.model.File
	 * CertificateEntry = null; String CertificateResourceId =
	 * getResourceIDUnderParent(CompanyResourceId,SSConstants.
	 * SS_CERTIFICATE_NAME);
	 * log.info("Certificate Resource Id="+CertificateResourceId ); if
	 * (null==CertificateResourceId) { CertificateEntry =
	 * createSubFolders(SSConstants.SS_CERTIFICATE_NAME,CompanyResourceId);
	 * CertificateResourceId = CertificateEntry.getId();
	 * log.info("Certificate Resource Id="+CertificateResourceId ); }
	 * 
	 * 
	 * //checking the certificagte already exist or not String
	 * certResId=getResourceIDUnderParent(CertificateResourceId,authId);
	 * if(certResId!=null){
	 * log.info("Certificate already uploaded to the Google drive");
	 * //response.setStatus(
	 * 403,"Certificate already uploaded to the Google App Engine"); return
	 * "Certificate already uploaded to the Google drive"; }
	 * 
	 * com.google.api.services.drive.model.File uploadedEntry =
	 * uploadDocFile(is, "binary/octet-stream", CertificateResourceId,authId);
	 * log.info("Uploadded the certificate to google drive sucessfully");
	 * log.info("Document now online @ :"+ uploadedEntry.getId()); return
	 * "success"; }
	 */

	private File getExistsFolder(Drive service, String name, String parentId) throws IOException {
		Drive.Files.List request;
		request = service.files().list();
		String query = "mimeType='application/vnd.google-apps.folder' AND trashed=false AND name='" + name + "' AND '"
				+ parentId + "' in parents";
		log.info(": isFolderExists(): Query= " + query);
		request = request.setQ(query);
		FileList files = request.execute();
		log.info(": isFolderExists(): List Size =" + files.getFiles().size());
		if (files.getFiles().size() == 0) // if the size is zero, then the
											// folder doesn't exist
			return null;
		else
			// since google drive allows to have multiple folders with the same
			// title (name)
			// we select the first file in the list to return
			return files.getFiles().get(0);
	}

	private File createFolder(Drive service, String title, List<String> parents) throws IOException {
		File body = new File();
		body.setName(title);
		body.setParents(parents);
		body.setMimeType("application/vnd.google-apps.folder");
		File file = service.files().create(body).execute();
		return file;

	}

	private File createFile(List<String> parents, String name, String mimeType, InputStream inputStream) {

		try {

			com.google.api.services.drive.model.File fileBody = new com.google.api.services.drive.model.File();

			fileBody.setName(name);
			fileBody.setMimeType(mimeType);

			fileBody.setParents(parents);

			com.google.api.client.http.AbstractInputStreamContent in = new AbstractInputStreamContent(mimeType) {

				@Override
				public boolean retrySupported() {
					// TODO Auto-generated method stub
					return false;
				}

				@Override
				public long getLength() throws IOException {
					// TODO Auto-generated method stub
					return inputStream.available();
				}

				@Override
				public InputStream getInputStream() throws IOException {
					// TODO Auto-generated method stub
					return inputStream;
				}
			};

			com.google.api.services.drive.model.File file = getDriveClient().files().create(fileBody, in)
					.setFields("id, webContentLink, webViewLink, parents").execute();
			// System.out.println("File ID:::::::::::::: " + file.getId());

			return file;
		} catch (Exception exp) {
			exp.printStackTrace();
		}
		return null;

	}

	private List<String> createFoldersPath(Drive service, String... names) throws IOException {
		List<String> listParents = new ArrayList<String>();
		File file = null;
		for (int i = 0; i < names.length; i++) {
			file = getExistsFolder(service, names[i], (file == null) ? "root" : file.getId());
			if (file == null) {
				file = createFolder(service, names[i], listParents);
			}
			listParents.clear();
			listParents.add(file.getId());
		}
		return listParents;
	}

	/*
	 * public String uploadFile1(Drive service, String smartstore, String
	 * systemId, String archiveId, String archiveDocId, String name, String
	 * mimeType, byte[] body) throws IOException { List<String> parents =
	 * createFoldersPath(service, smartstore, systemId, archiveId,
	 * archiveDocId); return createFile(parents, name, mimeType, new
	 * ByteArrayInputStream(body)).getId();
	 * 
	 * }
	 */

	public String uploadFile(String smartstore, String systemId, String archiveId, String archiveDocId, String name,
			String mimeType, byte[] body) throws IOException {

		List<String> parents = createFoldersPath(getDriveClient(), smartstore, systemId, archiveId, archiveDocId);
		return createFile(parents, name, mimeType, new ByteArrayInputStream(body)).getId();
	}

	@Override
	public byte[] downFile(String id) {
		// TODO Auto-generated method stub
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();

		try {
			getDriveClient().files().get(id).executeMediaAndDownloadTo(buffer);

		} catch (Exception exp) {
			// b=new byte[10];
			exp.printStackTrace();
		}

		return buffer.toByteArray();

	}

	@Override
	public long getDocContentLength(String repositoryName,String systemId, String docId, String compId, String rootPath)
			throws IOException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public java.io.File getDocPath(String repositoryName,String systemId, String docId, String compId, String rootPath)
			throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public java.io.File getDocResourcePath(String repositoryName,String systemId, String docId, String compId, String rootPath)
			throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

}
