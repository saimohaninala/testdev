package com.smartdocs.smartportal.smartstore;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.InvalidKeyException;
import org.apache.commons.io.FilenameUtils;
import com.microsoft.azure.storage.*;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.*;
import com.smartdocs.smartportal.config.JHipsterProperties;
import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.web.rest.AdminAccessFunctions;


public class AzureBlobApi extends AdminAccessFunctions{

	
	Repository repository;
	
	JHipsterProperties jHipsterProperties;

	public AzureBlobApi() {
		super();
	}
	
	public AzureBlobApi( Repository repository,JHipsterProperties jHipsterProperties) {
		super();
		this.repository=repository;
		this.jHipsterProperties = jHipsterProperties;
		
	}
	
	@Override
	public String putCert(String contRep, String authId, InputStream is, String rootPath) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] getDocInputStreamURL(String repositoryName, String docId, String compId, String rootPath)
			throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean appendDocFile(InputStream inputStream, String repoName, String docId, String compId,
			String ContentType) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean DeleteDoc(String repoName, String docId, String compId, String rootPath) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public URL createcomponent(String contentType, String repoName, String docId, String fileName, InputStream in,
			String rootPath) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean updateDocFile(InputStream inputStream, String companyName, String repoName, String docId,
			String compId, String ContentType) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String uploadDocFile2(InputStream inputStream, String ContentType, URL destFolderUri, String compId,
			String docId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] getComponentInputStreamUsingResourceIdExt(String resourceId, String extention, String docId,
			String compId) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteDocumentOrComponent(String resourceId) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getRepositoryResourceIdUnderSmartStore(String companyName, String repositoryName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDocumentResourceIdUnderRepository(String resourceId, String docId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public URL createcomponent(String contentType, String resourceId, String fileName, InputStream in)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String uploadFile(String smartstore, String systemId, String archiveId, String archiveDocId, String name,
			String mimeType, byte[] body) throws IOException {
		
		String accountName=repository.getClientId();	
		String accountKey=repository.getClientSecret();	
	  // System.out.println("in azure api file Name is ====="+name+"====mimetype===="+mimeType);
	   CloudBlockBlob docBlob =null;
	  
		try {
			
                     CloudBlobContainer container;
                   //  CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);
                     
                     CloudStorageAccount storageAccount = CloudStorageAccount.parse( "DefaultEndpointsProtocol=https;"+
             			    "AccountName="+accountName+";"+ 
            			    "AccountKey="+accountKey);

                     CloudBlobClient serviceClient = storageAccount.createCloudBlobClient();
                     container = serviceClient.getContainerReference(jHipsterProperties.getAzure().getContainerName());
         			container.createIfNotExists();    	               
         			docBlob = container.getBlockBlobReference(smartstore+"/"+systemId+"/"+archiveId+"/"+archiveDocId+"/"+name);
         			docBlob.getProperties().setContentType(mimeType);      	      
         			docBlob.uploadFromByteArray(body, 0, body.length);
         	     
		} catch (InvalidKeyException e) {	
			e.printStackTrace();
		} catch (URISyntaxException e) {	
			e.printStackTrace();
		} catch (StorageException e) {		
			e.printStackTrace();
		}
		
		return docBlob.getUri().toString();
	}

	@Override
	public byte[] downFile(String id) {
		
		
        String accountName=repository.getClientId();	
		String accountKey=repository.getClientSecret();
		 byte[] convert = null;
		
		 try {
			
        URL url = new URL(id.toString());
       // String fileExension=FilenameUtils.getExtension(url.getPath());
        String fileName= FilenameUtils.getName(url.getPath());
        String path= FilenameUtils.getPath(url.getPath());   
		 String Path=path+fileName;
		 String finalPath = Path.replaceFirst(jHipsterProperties.getAzure().getContainerName()+"/", "");
		// System.out.println("finalPath==="+finalPath);
		 //CloudStorageAccount storageAccount = CloudStorageAccount.parse(storageConnectionString);
		 CloudStorageAccount storageAccount = CloudStorageAccount.parse( "DefaultEndpointsProtocol=https;"+
  			    "AccountName="+accountName+";"+ 
 			    "AccountKey="+accountKey);
		   CloudBlobClient blobClient = storageAccount.createCloudBlobClient();
		   CloudBlobContainer container = blobClient.getContainerReference(jHipsterProperties.getAzure().getContainerName());
		   CloudBlockBlob imgBlob = container.getBlockBlobReference(finalPath);
		   convert = downloadImage(imgBlob);
		//   System.out.println("convert length----"+convert.length);
		
		 }catch (Exception e) {
				e.printStackTrace();
				}
		return  convert;
	}

	private byte[] downloadImage(CloudBlockBlob imgBlob){
    	
    	ByteArrayOutputStream bos = new ByteArrayOutputStream();
    	try {
			imgBlob.download(bos);		
		} catch (StorageException e) {			
			e.printStackTrace();
		}
    	return bos.toByteArray();
    }
	
	 
	@Override
	public long getDocContentLength(String repositoryName,String systemId, String docId, String compId, String rootPath)
			throws IOException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public File getDocPath(String repositoryName,String systemId, String docId, String compId, String rootPath) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public File getDocResourcePath(String repositoryName,String systemId, String docId, String compId, String rootPath)
			throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

}
