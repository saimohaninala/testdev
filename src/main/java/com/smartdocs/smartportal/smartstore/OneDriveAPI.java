package com.smartdocs.smartportal.smartstore;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.google.api.client.util.Clock;
import com.google.api.client.util.Strings;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.smartdocs.smartportal.config.JHipsterProperties;
import com.smartdocs.smartportal.domain.GdriveToken;
import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.repository.GdriveTokenRepository;
import com.smartdocs.smartportal.smartstore.onedrive.DriveItem;
import com.smartdocs.smartportal.smartstore.onedrive.DriveItemsResponse;
import com.smartdocs.smartportal.smartstore.onedrive.OneDriveStringResponseHandler;
import com.smartdocs.smartportal.web.rest.AdminAccessFunctions;

@Component
public class OneDriveAPI extends AdminAccessFunctions {

	public static final String API_HOST = "graph.microsoft.com/v1.0/me";
	public static final String DEFAULT_SCHEME = "https";
	public static final String DRIVES_URL_PATH = "/drives";

	public String accessToken;

	GdriveTokenRepository gdriveTokenRepository;

	JHipsterProperties jHipsterProperties;

	Repository repository = null;

	public OneDriveAPI() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OneDriveAPI(Repository repository, GdriveTokenRepository gdriveTokenRepository,
			JHipsterProperties jHipsterProperties) {
		super();
		this.repository = repository;
		this.gdriveTokenRepository = gdriveTokenRepository;
		this.jHipsterProperties = jHipsterProperties;
		this.accessToken = getActiveAccessToken(repository, repository.getId());
	}

	@Override
	public String putCert(String contRep, String authId, InputStream is, String rootPath) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] getDocInputStreamURL(String repositoryName, String docId, String compId, String rootPath)
			throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean appendDocFile(InputStream inputStream, String repoName, String docId, String compId,
			String ContentType) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean DeleteDoc(String repoName, String docId, String compId, String rootPath) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public URL createcomponent(String contentType, String repoName, String docId, String fileName, InputStream in,
			String rootPath) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean updateDocFile(InputStream inputStream, String companyName, String repoName, String docId,
			String compId, String ContentType) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String uploadDocFile2(InputStream inputStream, String ContentType, URL destFolderUri, String compId,
			String docId) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] getComponentInputStreamUsingResourceIdExt(String resourceId, String extention, String docId,
			String compId) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteDocumentOrComponent(String resourceId) throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getRepositoryResourceIdUnderSmartStore(String companyName, String repositoryName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDocumentResourceIdUnderRepository(String resourceId, String docId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public URL createcomponent(String contentType, String resourceId, String fileName, InputStream in)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String uploadFile(String smartstore, String systemId, String archiveId, String archiveDocId, String name,
			String mimeType, byte[] body) throws IOException {

		URI uri;
		try {
			uri = new URIBuilder().setScheme(DEFAULT_SCHEME).setHost(API_HOST).setPath("/drive/root:/" + smartstore
					+ "/" + systemId + "/" + archiveId + "/" + archiveDocId + "/" + name + ":/" + "/content")

					.build();
		} catch (URISyntaxException e) {
			throw new IllegalStateException("Invalid drives path", e);
		}
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpPut httpPut = new HttpPut(uri);
			httpPut.setHeader("Authorization", "Bearer " + accessToken);
			httpPut.setHeader("Content-Type", mimeType);

			if (body != null) {
				httpPut.setEntity(new ByteArrayEntity(body));
			}
			String rawResponse = httpClient.execute(httpPut, new OneDriveStringResponseHandler());
			System.out.println(rawResponse);

			DriveItem item = new GsonBuilder().create().fromJson(rawResponse, DriveItem.class);
			if (!Strings.isNullOrEmpty(item.id)) {
				return item.id;
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new IOException("Error getting drives", e);
		}
		return null;
	}

	public byte[] downFile1(String id) {

		URI uri = null;
		try {
			List<DriveItem> driveItems = getDriveItem(id);

			DriveItem driveItem = driveItems.get(0);
			if (Strings.isNullOrEmpty(driveItem.downloadUrl)) {
				uri = new URIBuilder().setScheme(DEFAULT_SCHEME).setHost(API_HOST)
						.setPath("/drive/items/" + driveItem.id + "/content").build();
			} else {
				uri = new URI(driveItem.downloadUrl);
			}
		} catch (URISyntaxException e) {
			throw new IllegalStateException("Invalid drives path", e);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpGet httpGet = new HttpGet(uri);
			httpGet.setHeader("Authorization", "Bearer " + accessToken);

			try (

					InputStream inputStream = httpClient.execute(httpGet).getEntity().getContent()) {
				return IOUtils.toByteArray(inputStream);
			}
		} catch (Exception e) {

		}

		return new byte[1024];
	}

	public byte[] downFile(String itemId) {

		URI uri;
		try {
			uri = new URIBuilder().setScheme(DEFAULT_SCHEME).setHost(API_HOST)
					.setPath("/drive/items/" + itemId + "/content")

					.build();
		} catch (URISyntaxException e) {
			throw new IllegalStateException("Invalid drives path", e);
		}
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpGet httpGet = new HttpGet(uri);
			httpGet.setHeader("Authorization", "Bearer " + accessToken);

			try (

					InputStream inputStream = httpClient.execute(httpGet).getEntity().getContent()) {
				return IOUtils.toByteArray(inputStream);
			}
		} catch (Exception e) {

		}

		return new byte[1024];
	}

	public List<DriveItem> getDriveItem(String itemId) throws IOException {
		URI uri;
		try {
			uri = new URIBuilder().setScheme(DEFAULT_SCHEME).setHost(API_HOST).setPath("/drive/items/" + itemId)

					.build();
		} catch (URISyntaxException e) {
			throw new IllegalStateException("Invalid drives path", e);
		}

		return getDriveItemsImpl(uri);
	}

	private List<DriveItem> getDriveItemsImpl(URI uri) throws IOException {
		ImmutableList.Builder<DriveItem> driveItems = new ImmutableList.Builder<>();

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {

			while (uri != null) {
				HttpGet httpGet = new HttpGet(uri);
				httpGet.setHeader("Authorization", "Bearer " + accessToken);
				String rawResponse = httpClient.execute(httpGet, new OneDriveStringResponseHandler());
				DriveItemsResponse driveItemsResponse = new Gson().fromJson(rawResponse, DriveItemsResponse.class);

				driveItems.addAll(Arrays.asList(driveItemsResponse.value));

				uri = driveItemsResponse.nextLink == null ? null : URI.create(driveItemsResponse.nextLink);
			}

			return driveItems.build();
		} catch (Exception e) {
			throw new IOException("Error getting drives", e);
		}
	}

	public String getActiveAccessToken(Repository repository, String id) {
		// TODO Auto-generated method stub
		String accessToken = null;
		OneDriveOauthUtils oauthutil = new OneDriveOauthUtils();
		GdriveToken tokenInfo = gdriveTokenRepository.findOne(id);
		accessToken = tokenInfo.getAccessToken();
		Clock clock = Clock.SYSTEM;
		final Lock lock = new ReentrantLock();
		lock.lock();
		try {
			// check if token will expire in a minute
			Long expiresIn = (tokenInfo.getExpirationTimeMilliseconds() - clock.currentTimeMillis()) / 1000;
			if (tokenInfo.getAccessToken() == null || expiresIn != null && expiresIn <= (60 - 10)) {
				JSONObject jsonObject = oauthutil.refreshToken(repository, tokenInfo.getRefreshToken(),
						jHipsterProperties.getOnedrive().getRedirectUri());
				String accesstoken = (String) jsonObject.get("access_token");
				System.out.println("accesstoken in onedriveoauthtokenService" + accesstoken);

				if (accesstoken == null) {
					// nothing we can do without an access token
				} else {

					tokenInfo.setAccessToken(accesstoken);
					gdriveTokenRepository.save(tokenInfo);

					GdriveToken tokenInfo1 = gdriveTokenRepository.findOne(id);
					accessToken = tokenInfo1.getAccessToken();

					return accessToken;
				}
			} else {
				return tokenInfo.getAccessToken();
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			lock.unlock();
		}

		return accessToken;

	}

	@Override
	public long getDocContentLength(String repositoryName,String systemId, String docId, String compId, String rootPath)
			throws IOException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public File getDocPath(String repositoryName,String systemId, String docId, String compId, String rootPath) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public File getDocResourcePath(String repositoryName,String systemId, String docId, String compId, String rootPath)
			throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

}