package com.smartdocs.smartportal.smartstore;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets.Details;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.Drive;

import com.google.api.services.drive.model.File;
import com.smartdocs.smartportal.config.JHipsterProperties;
import com.smartdocs.smartportal.domain.OAuthUserConfig;
import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.repository.GdriveTokenRepository;
import com.smartdocs.smartportal.web.rest.MongoRepoDataStoreFactory;
@Service
public class GdriveUtils {

	private static Drive service;
	private static final String APPLICATION_NAME = "oauth drive";

	private static HttpTransport httpTransport;
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	static GoogleClientSecrets clientSecrets;
	static GoogleAuthorizationCodeFlow flow;
	Credential credential;
	
	@Inject
    public static GdriveTokenRepository gdrivetokenRepository;
	

	public GdriveUtils() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GdriveUtils(GdriveTokenRepository gdrivetokenRepository) {
		this.gdrivetokenRepository = gdrivetokenRepository;
	}

	static	GoogleAuthorizationCodeFlow newFlow(Repository repository,GdriveTokenRepository gdrivetokenRepository) throws IOException { 
		
	        Details web = new Details();
	        String clientId=	repository.getClientId();
	        String clientSecret=repository.getClientSecret();
	        String redirectUrl=repository.getRootPath();
	        web.setClientId(clientId);
	        web.setClientSecret(clientSecret);
	   List<String> redirectUrls=new ArrayList<String>();
	   redirectUrls.add(redirectUrl);
	   web.setRedirectUris(redirectUrls);

	        return  new GoogleAuthorizationCodeFlow.Builder(new NetHttpTransport(),
	                new JacksonFactory(), new GoogleClientSecrets().setWeb(web),
	                Collections.singleton(DriveScopes.DRIVE)).setDataStoreFactory(new MongoRepoDataStoreFactory(gdrivetokenRepository)).setAccessType("offline").build();
	  }

	public static Drive loadDriveClient(Repository repository,GdriveTokenRepository gdrivetokenRepository) throws IOException {
		Credential credential = newFlow(repository,gdrivetokenRepository)
				.loadCredential(repository.getSystemId() + repository.getRepositoryId());
		return new Drive.Builder(new NetHttpTransport(), new JacksonFactory(), credential).build();
	}

	public static String authorize(Repository repository) throws Exception {

		String redirectUri = repository.getRootPath();
		AuthorizationCodeRequestUrl authorizationUrl;
		if (flow == null) {
			String clientId = repository.getClientId();
			String clientSecret = repository.getClientSecret();

			Details web = new Details();
			web.setClientId(clientId);
			web.setClientSecret(clientSecret);
			clientSecrets = new GoogleClientSecrets().setWeb(web);
			httpTransport = GoogleNetHttpTransport.newTrustedTransport();
			flow = new GoogleAuthorizationCodeFlow.Builder(httpTransport, JSON_FACTORY, clientSecrets,
					Collections.singleton(DriveScopes.DRIVE))
							.setDataStoreFactory(new MongoRepoDataStoreFactory(gdrivetokenRepository))
							.setAccessType("offline").setApprovalPrompt("force").build();
		}
		authorizationUrl = flow.newAuthorizationUrl().setRedirectUri(redirectUri).setState(repository.getId());

		return authorizationUrl.build();
	}

}
