package com.smartdocs.smartportal.smartstore.onedrive;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * A service for accessing the OneDrive files.
 *
 * @author Luke Quinane
 */
public class DriveService {
	static Logger logger = Logger.getLogger(DriveService.class.getName());

	public static final String API_HOST = "graph.microsoft.com/v1.0/me";
	public static final String DEFAULT_SCHEME = "https";
	public static final String DRIVES_URL_PATH = "/drives";

	/**
	 * This class should only be instantiated from the
	 * OneDrive.getDriveService() method.
	 */
	protected DriveService() {
	}

	/**
	 * Gets a list of drives.
	 *
	 * @param accessToken
	 *            the access token.
	 * @return the list of drives.
	 * @throws IOException
	 *             if an error occurs.
	 */
	public List<Drive> getDrives(String accessToken) throws IOException {
		URI uri;
		try {
			uri = new URIBuilder().setScheme(DEFAULT_SCHEME).setHost(API_HOST).setPath(DRIVES_URL_PATH)

					.build();
		} catch (URISyntaxException e) {
			throw new IllegalStateException("Invalid drives path", e);
		}

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpGet httpGet = new HttpGet(uri);
			httpGet.setHeader("Authorization", "Bearer " + accessToken);
			String rawResponse = httpClient.execute(httpGet, new OneDriveStringResponseHandler());

			Gson gson = new GsonBuilder()
					.registerTypeAdapter(Drive[].class, new ResponseValueDeserializer<>(Drive[].class)).create();

			return Arrays.asList(gson.fromJson(rawResponse, Drive[].class));
		} catch (Exception e) {
			throw new IOException("Error getting drives", e);
		}
	}

	/**
	 * Gets the root items for a drive.
	 *
	 * @param accessToken
	 *            the access token.
	 * @param driveId
	 *            the drive Id.
	 * @return the list of items.
	 * @throws IOException
	 *             if an error occurs.
	 */
	public List<DriveItem> getRootItems(String accessToken, String driveId) throws IOException {
		URI uri;
		try {
			uri = new URIBuilder().setScheme(DEFAULT_SCHEME).setHost(API_HOST)
					.setPath("/drives/" + driveId + "/root/children")

					.build();
		} catch (URISyntaxException e) {
			throw new IllegalStateException("Invalid drives path", e);
		}

		return getDriveItemsImpl(uri, accessToken);
	}

	/**
	 * Gets the folder items.
	 *
	 * @param accessToken
	 *            the access token.
	 * @param folderId
	 *            the folder Id to look under.
	 * @return the list of drive items.
	 * @throws IOException
	 *             if an error occurs.
	 */
	public List<DriveItem> getChildItems(String accessToken, String folderId) throws IOException {
		URI uri;
		try {
			uri = new URIBuilder().setScheme(DEFAULT_SCHEME).setHost(API_HOST)
					.setPath("/drive/items/" + folderId + "/children/")

					.build();
		} catch (URISyntaxException e) {
			throw new IllegalStateException("Invalid drives path", e);
		}

		return getDriveItemsImpl(uri, accessToken);
	}

	/**
	 * Gets the drive items.
	 *
	 * @param uri
	 *            the URI to fetch from.
	 * @return the list of items.
	 * @throws IOException
	 *             if an error occurs.
	 */
	private List<DriveItem> getDriveItemsImpl(URI uri, String accessToken) throws IOException {
		ImmutableList.Builder<DriveItem> driveItems = new ImmutableList.Builder<>();

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {

			while (uri != null) {
				HttpGet httpGet = new HttpGet(uri);
				httpGet.setHeader("Authorization", "Bearer " + accessToken);
				String rawResponse = httpClient.execute(httpGet, new OneDriveStringResponseHandler());
				DriveItemsResponse driveItemsResponse = new Gson().fromJson(rawResponse, DriveItemsResponse.class);

				driveItems.addAll(Arrays.asList(driveItemsResponse.value));

				uri = driveItemsResponse.nextLink == null ? null : URI.create(driveItemsResponse.nextLink);
			}

			return driveItems.build();
		} catch (Exception e) {
			throw new IOException("Error getting drives", e);
		}
	}

	/**
	 * Downloads the content for an item.
	 *
	 * @param accessToken
	 *            the access token.
	 * @param driveItem
	 *            the item.
	 * @param outputStream
	 *            the stream to write to.
	 * @throws IOException
	 *             if an error occurs.
	 */
	public void downloadItemContent(String accessToken, DriveItem driveItem, OutputStream outputStream)
			throws IOException {

		URI uri;
		try {
			if (Strings.isNullOrEmpty(driveItem.downloadUrl)) {
				uri = new URIBuilder().setScheme(DEFAULT_SCHEME).setHost(API_HOST)
						.setPath("/drive/items/" + driveItem.id + "/content").build();
			} else {
				uri = new URI(driveItem.downloadUrl);
			}
		} catch (URISyntaxException e) {
			throw new IllegalStateException("Invalid drives path", e);
		}

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpGet httpGet = new HttpGet(uri);
			httpGet.setHeader("Authorization", "Bearer " + accessToken);

			try (InputStream inputStream = httpClient.execute(httpGet).getEntity().getContent()) {
				IOUtils.copyLarge(inputStream, outputStream, new byte[0x10000]);
			}
		} catch (Exception e) {
			throw new IOException("Error getting drives", e);
		}
	}

	public DriveItem createFolder(String accessToken, String name, String locationFolderId) throws IOException {

		URI uri;
		try {
			uri = new URIBuilder().setScheme(DEFAULT_SCHEME).setHost(API_HOST)
					.setPath("/drive/items/" + locationFolderId + "/children")

					.build();
		} catch (URISyntaxException e) {
			throw new IllegalStateException("Invalid drives path", e);
		}
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpPost httpPost = new HttpPost(uri);
			httpPost.setHeader("Authorization", "Bearer " + accessToken);
			DriveItem driveItem = new DriveItem();
			
			driveItem.name = name;
			FolderFacet folderFacet=new FolderFacet();
			folderFacet.childCount=0;
			driveItem.folder = folderFacet;
			Gson gsonText = new GsonBuilder().create();

			StringEntity entity = new StringEntity(gsonText.toJson(driveItem));
			httpPost.setEntity(entity);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");
			String rawResponse = httpClient.execute(httpPost, new OneDriveStringResponseHandler());


			return new GsonBuilder().create().fromJson(rawResponse, DriveItem.class);


		} catch (Exception e) {
			e.printStackTrace();
			throw new IOException("Error getting drives", e);
		}

	}
	
	public DriveItem uploadFile(String accessToken, String locationFolderId, String name,String mimeType,byte[] body) throws IOException {

		URI uri;
		try {
			uri = new URIBuilder().setScheme(DEFAULT_SCHEME).setHost(API_HOST)
					.setPath("/drive/items/" + locationFolderId + ":/"+name+":/"+"/content")

					.build();
		} catch (URISyntaxException e) {
			throw new IllegalStateException("Invalid drives path", e);
		}
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpPut httpPut = new HttpPut(uri);
			httpPut.setHeader("Authorization", "Bearer " + accessToken);
			httpPut.setHeader("Content-Type",mimeType);

			
			if (body != null) {
				httpPut.setEntity(new ByteArrayEntity(body));
		    }			String rawResponse = httpClient.execute(httpPut, new OneDriveStringResponseHandler());
			System.out.println(rawResponse);


			return new GsonBuilder().create().fromJson(rawResponse, DriveItem.class);


		} catch (Exception e) {
			e.printStackTrace();
			throw new IOException("Error getting drives", e);
		}

	}
	//PUT https://graph.microsoft.com/v1.0/me/drive/root:/foo/bar/baz/file.txt:/content

	public DriveItem uploadFile(String accessToken,String  smartstore,String systemId, String  archiveId,String archiveDocId, String name,String mimeType,byte[] body) throws IOException {

		URI uri;
		try {
			uri = new URIBuilder().setScheme(DEFAULT_SCHEME).setHost(API_HOST)
					.setPath("/drive/root:/" + smartstore + "/"+systemId + "/"+archiveId +"/"+archiveDocId + "/"+name+":/"+"/content")

					.build();
		} catch (URISyntaxException e) {
			throw new IllegalStateException("Invalid drives path", e);
		}
		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpPut httpPut = new HttpPut(uri);
			httpPut.setHeader("Authorization", "Bearer " + accessToken);
			httpPut.setHeader("Content-Type",mimeType);

			
			if (body != null) {
				httpPut.setEntity(new ByteArrayEntity(body));
		    }			String rawResponse = httpClient.execute(httpPut, new OneDriveStringResponseHandler());
			System.out.println(rawResponse);


			return new GsonBuilder().create().fromJson(rawResponse, DriveItem.class);


		} catch (Exception e) {
			e.printStackTrace();
			throw new IOException("Error getting drives", e);
		}

	}
	
	

	

}
