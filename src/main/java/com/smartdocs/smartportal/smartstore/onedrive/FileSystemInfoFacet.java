package com.smartdocs.smartportal.smartstore.onedrive;

/**
 * Drive file system info.
 *
 * @author Luke Quinane
 */
public class FileSystemInfoFacet {
    /**
     * The created date/time.
     */
    public String createdDateTime;

    /**
     * The last modified date/time.
     */
    public String lastModifiedDateTime;
}
