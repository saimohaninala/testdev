package com.smartdocs.smartportal.smartstore.onedrive;

/**
 * An identity in a {@link Drive}.
 *
 * @author Luke Quinane
 */
public class Identity {
    /**
     * The display name.
     */
    public String displayName;

    /**
     * The ID.
     */
    public String id;
}
