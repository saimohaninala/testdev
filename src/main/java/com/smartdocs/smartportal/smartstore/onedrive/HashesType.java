package com.smartdocs.smartportal.smartstore.onedrive;

/**
 * Drive file hashes.
 *
 * @author Luke Quinane
 */
public class HashesType {
    /**
     * The CRC32 value for the file data.
     */
    public String crc32Hash;

    /**
     * The SHA-1 value for the file data.
     */
    public String sha1Hash;
}
