package com.smartdocs.smartportal.smartstore.onedrive;

/**
 * Location information for a drive item.
 */
public class LocationFacet {
    /**
     * The altitude for the item (in feet).
     */
    public double altitude;

    /**
     * The latitude.
     */
    public double latitude;

    /**
     * The longitude.
     */
    public double longitude;
}
