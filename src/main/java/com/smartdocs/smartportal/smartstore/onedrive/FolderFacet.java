package com.smartdocs.smartportal.smartstore.onedrive;

/**
 * A drive folder.
 *
 * @author Luke Quinane
 */
public class FolderFacet {
    /**
     * The child count.
     */
    public int childCount;
}
