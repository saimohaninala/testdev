package com.smartdocs.smartportal.smartstore.onedrive;

/**
 * A reference to an item.
 *
 * @author Luke Quinane
 */
public class ItemReference {
    /**
     * The parent Id.
     */
    public String id;

    /**
     * The drive Id.
     */
    public String driveId;

    /**
     * The path to the item.
     */
    public String path;
}
