package com.smartdocs.smartportal.smartstore.onedrive;

/**
 * Deletion information for a drive item.
 */
public class DeletedFacet {
    public String state;
}
