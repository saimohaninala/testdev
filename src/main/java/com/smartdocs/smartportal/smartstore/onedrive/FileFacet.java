package com.smartdocs.smartportal.smartstore.onedrive;

/**
 * A drive file.
 *
 * @author Luke Quinane
 */
public class FileFacet {
    /**
     * The file MIME type.
     */
    public String mimeType;

    /**
     * The file hashes.
     */
    public HashesType hashes;
}
