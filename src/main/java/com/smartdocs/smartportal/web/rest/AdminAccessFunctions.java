package com.smartdocs.smartportal.web.rest;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.google.api.services.drive.Drive;
import com.smartdocs.smartportal.config.JHipsterProperties;
import com.smartdocs.smartportal.constant.SmartstoreConstant;
import com.smartdocs.smartportal.domain.GdriveToken;
import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.repository.GdriveTokenRepository;
import com.smartdocs.smartportal.smartstore.AzureBlobApi;
import com.smartdocs.smartportal.smartstore.FileSystemAPI;
import com.smartdocs.smartportal.smartstore.GoogleDriveApi;
import com.smartdocs.smartportal.smartstore.OneDriveAPI;



@Service
public abstract class AdminAccessFunctions {
	


	
/*	public static AdminAccessFunctions getInstance(String system, String contRep, String storageType) {
		if (storageType.equalsIgnoreCase(SmartstoreConstant.SS_GDRIVE_STORAGE_SYSTEM)) {

			return new GoogleDriveApi(system,contRep);
		} else {

			return new FileSystemAPI( system,  contRep);

		}

	}*/
	

	public static AdminAccessFunctions getInstance(Repository repository,GdriveTokenRepository gdriveTokenRepository,JHipsterProperties jHipsterProperties) throws IOException {
		if (repository.getStorageType().equalsIgnoreCase(SmartstoreConstant.SS_GDRIVE_STORAGE_SYSTEM)) {

			return new GoogleDriveApi(repository, gdriveTokenRepository);
		} else if (repository.getStorageType().equalsIgnoreCase(SmartstoreConstant.SS_ONEDRIVE_STORAGE_SYSTEM)) {
                     
			return new OneDriveAPI(repository,gdriveTokenRepository,jHipsterProperties);
		} else if (repository.getStorageType().equalsIgnoreCase(SmartstoreConstant.SS_AZURE_BLOBSTORAGE)) {
            
	     return new AzureBlobApi(repository, jHipsterProperties);
     } 
		else
			return new FileSystemAPI(repository);
		
	}
	
	
	
	
	
	
	
	
	

		public abstract String putCert(String contRep,String authId, InputStream is,String rootPath) throws Exception;

		public abstract byte[] getDocInputStreamURL( String repositoryName, String docId, String compId,String rootPath)
				throws IOException;

		public abstract boolean appendDocFile(InputStream inputStream,  String repoName, String docId,
				String compId, String ContentType) throws IOException;

		// public abstract boolean updateDocFile(InputStream inputStream,String
		// companyName, String repoName,String docId,String compId,String
		// ContentType)throws IOException, ServiceException ;
		public abstract boolean DeleteDoc( String repoName, String docId, String compId,String rootPath);

		public abstract URL createcomponent(String contentType, String repoName, String docId,
				String fileName, InputStream in,String rootPath) throws Exception;

		public abstract boolean updateDocFile(InputStream inputStream, String companyName, String repoName, String docId,
				String compId, String ContentType) throws IOException;

		public abstract String uploadDocFile2(InputStream inputStream, String ContentType, URL destFolderUri, String compId,
				String docId) throws Exception;

		public abstract byte[] getComponentInputStreamUsingResourceIdExt(String resourceId, String extention, String docId,
				String compId) throws IOException;

		public abstract boolean deleteDocumentOrComponent(String resourceId) throws IOException;

		public abstract String getRepositoryResourceIdUnderSmartStore(String companyName, String repositoryName);

		public abstract String getDocumentResourceIdUnderRepository(String resourceId, String docId);

		public abstract URL createcomponent(String contentType, String resourceId, String fileName, InputStream in)
				throws Exception;
		
		public abstract String uploadFile(String  smartstore,String systemId, String  archiveId,String archiveDocId, String name,String mimeType,byte[] body) throws IOException;
		
		public abstract byte[] downFile(String id);

		public abstract long getDocContentLength(String repositoryName,String systemId, String docId, String compId, String rootPath)
				throws IOException ;

		public abstract File getDocPath(String repositoryName,String systemId, String docId, String compId, String rootPath) throws IOException;

		public abstract File getDocResourcePath(String repositoryName,String systemId, String docId, String compId, String rootPath)
				throws IOException; 
	}
