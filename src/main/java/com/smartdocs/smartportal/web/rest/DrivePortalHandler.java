package com.smartdocs.smartportal.web.rest;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.json.JSONException;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.api.client.util.DateTime;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.Drive.Files;

import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.google.api.services.drive.model.Permission;
import com.google.api.services.drive.model.PermissionList;
import com.smartdocs.smartportal.config.JHipsterProperties;
import com.smartdocs.smartportal.domain.Document;
import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.domain.Systems;
import com.smartdocs.smartportal.repository.GdriveTokenRepository;
import com.smartdocs.smartportal.service.DocumentService;
import com.smartdocs.smartportal.service.RepositoryService;
import com.smartdocs.smartportal.service.SystemsService;
import com.smartdocs.smartportal.smartstore.GdriveUtils;


@RestController
@RequestMapping("/api/portal")
@EnableScheduling
public class DrivePortalHandler {
	
	@Inject
	DocumentService documentService;
	@Inject
	SystemsService systemsService;
	@Inject
	RepositoryService repositoryService;
	@Inject
	GdriveTokenRepository gdrivetokenRepository;
	@Inject
	JHipsterProperties jhipsterProperties;
	
	 @GetMapping("/edit")
	 public String getShareDocument(@RequestParam(value = "repository") String repository,@RequestParam(value = "archiveDocId") String archiveDocId,@RequestParam(value = "email") String email) throws IOException  {
		Document document= documentService.findOneByDocId(archiveDocId);
		Repository repo=repositoryService.findOneByrepositoryId(repository);
		String resourceId=document.getResourceId();
		String systemId=document.getSystemId();
		System.out.println("/edit Function Calling"+archiveDocId);
		Permission newPermission = new Permission();
		newPermission.setType("user");
		newPermission.setRole("writer");
		newPermission.setEmailAddress(email);
		Drive driveService = GdriveUtils.loadDriveClient(repo,gdrivetokenRepository );
		File file = driveService.files().get(resourceId).setFields("webViewLink,modifiedTime").execute();
		Permission p=driveService.permissions().create(resourceId, newPermission).setSendNotificationEmail(false).execute();
		System.out.println("permission Id:" +  p.getId());
		//https://stackoverflow.com/questions/48140317/how-to-get-shareable-link-of-the-uploaded-file-to-google-drive-using-google-driv
	    String webviewLink=	file.getWebViewLink();
	    System.out.println("GET MODIFIED TIME"+  file.getModifiedTime());
	    System.out.println(webviewLink);
		 return webviewLink;
	 }
	 
	 @GetMapping("/getonedriveshareLink")
	 public String getShareOnedriveDocument(@RequestParam(value = "repository") String repository,@RequestParam(value = "archiveDocId") String archiveDocId,@RequestParam(value = "email") String email) throws IOException, JSONException {
/*		 Document document= documentService.findOneByDocId(archiveDocId);
		 String resourceId=document.getResourceId();
		 Repository repo=repositoryService.findOneByrepositoryId(repository);
		 
		 System.out.println("Repository>>>>>>>>>>>>>>>>>"+repo.getRepositoryId());
		 
			AdminAccessFunctions accessFunctions=AdminAccessFunctions.getInstance(repo, gdrivetokenRepository,jhipsterProperties);
			//String sharedItemLink=accessFunctions.getSharedItemLink(resourceId);
			System.out.println("sharedItemLink>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+sharedItemLink);
*/		 return null;
	 }	
	 
	 
	 /*
	 @GetMapping("/getRevokedSharedDocument")
	 @Scheduled(cron = "0 0 0/2 * * ?")
	 public String getRevokedSharedDocument() throws IOException  {
		 java.util.List<File> result = new ArrayList<File>(); 
		
		Systems systems= systemsService.findOneBySystemAuthId("CN=AA7");
		Drive driveService = GdriveUtils.loadDriveClient(systems.getSystemId(),systems.getRepository().get(0));
		 
		 Files.List request = driveService.files().list();
	
		 do {
             try {
               FileList files = request.execute();
               result.addAll(files.getFiles());

               request.setPageToken(files.getNextPageToken());
             } catch (IOException e) {
               System.out.println("An error occurred: " + e);
               request.setPageToken(null);
             }
           } while (request.getPageToken() != null &&
                    request.getPageToken().length() > 0);  
		 
		 for(File file:result) {
			 System.out.println("iddddddddddddddd"+file.getId()+"hhhhhhhhhhhhhh"+file.getName());
			     String id=file.getId();
			      File f=driveService.files().get(id).setFields("modifiedTime,permissions").execute();
			      DateTime datetime= f.getModifiedTime();
			      DateTime now = new DateTime(System.currentTimeMillis());
			      PermissionList permissonlist=   driveService.permissions().list(file.getId()).execute();
			   List<Permission> permission=  permissonlist.getPermissions();
			   for(Permission p:permission) {
				 String permissionId=  p.getId();
				 String role=p.getRole();
				 if(role.equalsIgnoreCase("owner")) {
				  //
				 }else {
				
				 long duration =now.getValue()- datetime.getValue()  ;
			     long diffHours = duration / (60 * 60 * 1000) % 24;
			     System.out.println("diffHours"+diffHours);
			     if(diffHours >=1) {
						  driveService.permissions().delete(file.getId(), permissionId).execute();
				}
				   
			   }
			     
			   }
			
			 }
		 return String.valueOf(result.size());
	 }
	*/
	

}
