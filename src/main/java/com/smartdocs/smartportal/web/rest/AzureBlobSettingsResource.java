package com.smartdocs.smartportal.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.smartdocs.smartportal.constant.SmartstoreConstant;
import com.smartdocs.smartportal.domain.AzureBlobSettings;
import com.smartdocs.smartportal.repository.AzureBlobSettingsRepository;
import com.smartdocs.smartportal.web.rest.util.HeaderUtil;
import com.smartdocs.smartportal.web.rest.util.PaginationUtil;
import com.smartdocs.smartportal.web.rest.util.ResponseUtil;

import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api")
public class AzureBlobSettingsResource {
    private final Logger log = LoggerFactory.getLogger(CompanyResource.class);
    
	@Inject
	private AzureBlobSettingsRepository azureBlobSettingsRepository;

	
	@PostMapping("/azureblobsettings")
    @Timed
    public ResponseEntity<AzureBlobSettings> create(@RequestBody AzureBlobSettings azureBlobSettings) throws URISyntaxException {
        log.debug("REST request to save Systems : {}", azureBlobSettings);
        azureBlobSettings.setId(SmartstoreConstant.SS_AZURE_BLOBSTORAGE);
        AzureBlobSettings result=azureBlobSettingsRepository.save(azureBlobSettings);
        
        return ResponseEntity.created(new URI("/api/azureblobsettings/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("azureBlobSettings", result.getId().toString()))
                .body(result);

 

}
	@PutMapping("/azureblobsettings")
    @Timed
    public ResponseEntity<AzureBlobSettings> update(@RequestBody AzureBlobSettings azureBlobSettings) throws URISyntaxException {
	 log.debug("REST request to save AzureBlobSettings : {}", azureBlobSettings);
	 if (azureBlobSettings.getId() == null) {
         return create(azureBlobSettings);
       }
	 AzureBlobSettings result  =  azureBlobSettingsRepository.save(azureBlobSettings);
       return ResponseEntity.ok()
           .headers(HeaderUtil.createEntityUpdateAlert("AzureBlobSettings", result.getId().toString()))
           .body(result);
    }
	
	@GetMapping("/azureblobsettings")
    @Timed
    public ResponseEntity<List<AzureBlobSettings>> getAllAzureBlobSettings(@ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of AzureBlobSettings");
        Page<AzureBlobSettings> page = azureBlobSettingsRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/azureblobsettings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
	
	 @GetMapping("/azureblobsettings/{id}")
	 @Timed
	 public ResponseEntity<AzureBlobSettings> getAzureBlobSettings(@PathVariable String id) {
	        log.debug("REST request to get AzureBlobSettings : {}", id);
	        AzureBlobSettings azureBlobSettings = azureBlobSettingsRepository.findOne(id);
	        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(azureBlobSettings));
	 }
	 
	 @DeleteMapping("/azureblobsettings/{id}")
	 @Timed
	 public ResponseEntity<Void> deleteSmartstoreSettings(@PathVariable String id) {
	        log.debug("REST request to delete azureBlobSettings : {}", id);
	        azureBlobSettingsRepository.delete(id);
	        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("azureBlobSettings", id.toString())).build();
	 }

}
