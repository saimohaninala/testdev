package com.smartdocs.smartportal.web.rest;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.google.common.net.InternetDomainName;
import com.smartdocs.smartportal.service.AcceessFuncitonsAPI;
import com.smartdocs.smartportal.service.AdminFuncitonsAPI;
import com.smartdocs.smartportal.util.TenantContextHolder;


public class SmartStoreServlet extends HttpServlet {
	
	@Inject
	AcceessFuncitonsAPI accessFunctions;
	@Inject
	AdminFuncitonsAPI adminFunctions;
	
	   public void init() throws ServletException {
		   SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, getServletContext());
	   }
	
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) 
            throws IOException {
    	String tenantId=request.getParameter("tenant");
    	//String 
    	if(tenantId==null){
    		tenantId=extractSubdomain(request);
    		if(tenantId==null)
    		tenantId="smartdocs";
    	}
        TenantContextHolder.setTenantId(tenantId);

		
		try {
		response=	parseQueryString(request,response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	  public HttpServletResponse parseQueryString(HttpServletRequest request, HttpServletResponse response)throws Exception {
		  String queryString = request.getQueryString();
		  if (queryString.startsWith("get")){
	    		
	    		response = accessFunctions.Get(request,response);
	    	} else if (queryString.startsWith("docGet")){
	    		
	    		response = accessFunctions.DocGet(request, response);
	    	} else if (queryString.startsWith("delete")){
	    		
	    		response = accessFunctions.Delete(request, response);
	    	} else if (queryString.startsWith("append")){
	    		
	    		response = accessFunctions.Append(request, response);
	    	} else if (queryString.startsWith("attrSearch")){
	    		response = accessFunctions.AttrSearch(request, response);
	    	} else if (queryString.startsWith("mCreate")){
	    		response = accessFunctions.Mcreate(request, response);
	    	} else if (queryString.startsWith("search")){
	    		response = accessFunctions.Search(request, response);
	        } else if (queryString.startsWith("update")){
		    	try {
					response = accessFunctions.Update(request, response);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		    } else if (queryString.startsWith("info")){
	    		//response = aSCSAtion.Info(request,response);
	    		response = accessFunctions.Info(request,response);
	    	} else if(queryString.startsWith("create")){    		
	    	//	response = aSCSAtion.createDoc(request,response);
	    		response = accessFunctions.createDoc(request,response);
	    	} else if(queryString.startsWith("putCert")){
	    		//System.out.println("i am called - In Handler2");
	    		//response = aSCSAdminAction.setPutCert(request,response);
	    		response = adminFunctions.setPutCert(request,response);
	    	} else if(queryString.startsWith("serverInfo")){    		
	    		//response = aSCSAdminAction.serverInfo(request,response);
	    		response = adminFunctions.serverInfo(request,response);
	    	} else if(queryString.startsWith("restore")){
	    		response = accessFunctions.Restore(request,response);
	    	}
		  
		  return response;
		  
	  }


	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(req, resp);
	}


	@Override
	protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(req, resp);
	}
	
    String extractSubdomain(ServletRequest req) {
        return InternetDomainName.from(req.getServerName()).parts().get(0);
      }


}
