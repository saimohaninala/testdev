package com.smartdocs.smartportal.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.smartdocs.smartportal.domain.BrandSetting;
import com.smartdocs.smartportal.service.BrandService;
import com.smartdocs.smartportal.web.rest.util.HeaderUtil;


@RestController
@RequestMapping("/api")
public class BrandSettingResource {

	  @Inject
	   public BrandService brandservice;
	
    private final Logger log = LoggerFactory.getLogger(BrandSettingResource.class);

    private static final String ENTITY_NAME = "BrandSetting";
    
	private static final String BUSINESSOBJECTS_SEQ_KEY = "BrandSettings";
	
	 @PutMapping("/savebrandSetting")
	    @Timed
	    public ResponseEntity<BrandSetting> savebrandSetting(@RequestBody BrandSetting brand) throws URISyntaxException {
	        log.debug("REST request to save BrnadSetting : {}", brand);
	        BrandSetting result = brandservice.save(brand);
	        return ResponseEntity.ok()
	            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
	            .body(result);
	    }
	 
	 @PostMapping("/updatebrandsetting")
	    @Timed
	    public ResponseEntity<BrandSetting> updatebrandsetting(@RequestBody BrandSetting brand) throws URISyntaxException {
	        log.debug("REST request to update BrnadSetting : {}", brand);
	        BrandSetting result = brandservice.save(brand);
	        return ResponseEntity.ok()
	            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
	            .body(result);
	    }
	 
	 @GetMapping("/getbrandsetting")
	    @Timed
	    public ResponseEntity<List<BrandSetting>> getbrandsetting() throws URISyntaxException {
	        log.debug("REST request to get BrnadSetting : {}");
	        List<BrandSetting> result = brandservice.get();
	        return ResponseEntity.ok()
	            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, ""))
	            .body(result);
	    }
}
