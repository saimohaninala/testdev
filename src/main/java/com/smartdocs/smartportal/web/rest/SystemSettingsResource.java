package com.smartdocs.smartportal.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.smartdocs.smartportal.config.JHipsterProperties;
import com.smartdocs.smartportal.domain.SiteSetting;
import com.smartdocs.smartportal.domain.SystemSettings;
import com.smartdocs.smartportal.repository.SequenceDao;
import com.smartdocs.smartportal.repository.SystemSettingsRepository;
import com.smartdocs.smartportal.security.SecurityUtils;
import com.smartdocs.smartportal.service.SystemSettingsService;
import com.smartdocs.smartportal.service.dto.UserDTO;
import com.smartdocs.smartportal.web.rest.errors.ErrorVM;
import com.smartdocs.smartportal.web.rest.util.HeaderUtil;
import com.smartdocs.smartportal.web.rest.util.PaginationUtil;
import com.smartdocs.smartportal.web.rest.util.ResponseUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.aspectj.weaver.bcel.AtAjAttributes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

/**
 * REST controller for managing Binder.
 */
@RestController
@RequestMapping("/api")
public class SystemSettingsResource {

	private final Logger log = LoggerFactory.getLogger(SystemSettingsResource.class);

	private static final String ENTITY_NAME = "systemSettings";
	// private static final String DOCUMENTTYPE_SEQ_KEY = "User";

	@Inject
	private  final SystemSettingsService systemSettingsService;

	@Inject
	private  final SystemSettingsRepository systemSettingsRepository;

	@Inject
	private JHipsterProperties jHipsterProperties;

	


    public SystemSettingsResource(SystemSettingsService systemSettingsService,
			SystemSettingsRepository systemSettingsRepository) {
		super();
		this.systemSettingsService = systemSettingsService;
		this.systemSettingsRepository = systemSettingsRepository;
	}

	/**
     * POST  /systemSettings : Create a new systemSettings.
     *
     * @param systemSettings the documentType to create
     * @return the ResponseEntity with status 201 (Created) and with body the new systemSettings, or with status 400 (Bad Request) if the systemSettings has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/systemSettings")
    @Timed
	public ResponseEntity<?> createSystemSettings(@RequestBody SystemSettings systemSetting) throws URISyntaxException {
		log.debug("REST request to save SystemSettings : {}", systemSetting);
        
        if (systemSettingsService.findOneBySystem(systemSetting.getSystem().toLowerCase()).isPresent()) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("systemSettingsManagement", "systemSettingsexists", "SystemSettings already in use"))
					.body(null);
		} 
       
        SystemSettings systemSettings=new SystemSettings();
        systemSettings.setId("9999");
//        systemSettings.setId(systemSetting.getName().toLowerCase());
		
       
		systemSettings.setSystem(systemSetting.getSystem());
        systemSettings.setMenu(systemSetting.getMenu());
        systemSettings.setActiveMenu(systemSetting.getActiveMenu());       
        systemSettings.setSmartstore(systemSetting.getSmartstore());       
        systemSettings.setSolr(systemSetting.getSolr());       
        systemSettings.setPasswordPolicy(systemSetting.getPasswordPolicy());
        systemSettings.setSite(systemSetting.getSite());
        
        
        SystemSettings result = systemSettingsService.save(systemSettings);
        
        return ResponseEntity.created(new URI("/api/systemSettings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /systemSettings : Updates an existing SystemSettings.
     *
     * @param SystemSettings the SystemSettings to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated SystemSettings,
     * or with status 400 (Bad Request) if the SystemSettings is not valid,
     * or with status 500 (Internal Server Error) if the SystemSettings couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/systemSettings")
    @Timed
    public ResponseEntity<SystemSettings> updateSystemSettings(@RequestBody SystemSettings systemSettings) throws URISyntaxException {
        log.debug("REST request to update SystemSettings : {}", systemSettings);
        systemSettings.setId("9999");
       /* if (systemSettings.getId() == null) {
          //  return createSystemSettings(systemSettings);
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("systemSettingsManagement", "systemSettingsdoesnotexists", "SystemSettings does not exist"))
					.body(null);
        }*/
        SystemSettings result = systemSettingsService.save(systemSettings);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, systemSettings.getId().toString()))
            .body(result);
    }

	@GetMapping("/systemSettings")
	@Timed
	public ResponseEntity<List<SystemSettings>> getAllSystemSettings(@ApiParam Pageable pageable)
			throws URISyntaxException {
		log.debug("REST request to get a page of SystemSettings");
		Page<SystemSettings> page = systemSettingsService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/systemSettings");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /systemSettings/:id : get the "id" SystemSettings.
	 *
	 * @param id
	 *            the id of the SystemSettings to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         SystemSettings, or with status 404 (Not Found)
	 */
	@GetMapping("/systemSettings/{id}")
	@Timed
	public ResponseEntity<SystemSettings> getSystemSettings(@PathVariable String id) {
		log.debug("REST request to get SystemSettings : {}", id);
		SystemSettings systemSettings = systemSettingsService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(systemSettings));
	}

	/**
	 * DELETE /SystemSettings/:id : delete the "id" SystemSettings.
	 *
	 * @param id
	 *            the id of the SystemSettings to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/systemSettings/{id}")
	@Timed
	public ResponseEntity<Void> deleteSystemSettings(@PathVariable String id) {
		log.debug("REST request to delete SystemSettings : {}", id);
		systemSettingsService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}


}