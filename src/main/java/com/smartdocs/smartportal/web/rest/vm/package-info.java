/**
 * View Models used by Spring MVC REST controllers.
 */
package com.smartdocs.smartportal.web.rest.vm;
