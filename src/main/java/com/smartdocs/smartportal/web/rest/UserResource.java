package com.smartdocs.smartportal.web.rest;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.smartdocs.smartportal.config.Constants;

import com.smartdocs.smartportal.domain.User;

import com.smartdocs.smartportal.repository.UserRepository;
import com.smartdocs.smartportal.security.AuthoritiesConstants;
import com.smartdocs.smartportal.security.SecurityUtils;

import com.smartdocs.smartportal.service.UserService;
import com.smartdocs.smartportal.service.dto.UserDTO;
import com.smartdocs.smartportal.web.rest.util.HeaderUtil;
import com.smartdocs.smartportal.web.rest.util.PaginationUtil;

import com.smartdocs.smartportal.web.rest.vm.ManagedUserVM;


import io.swagger.annotations.ApiParam;

/**
 * REST controller for managing users.
 *
 * <p>
 * This class accesses the User entity, and needs to fetch its collection of
 * authorities.
 * </p>
 * <p>
 * For a normal use-case, it would be better to have an eager relationship
 * between User and Authority, and send everything to the client side: there
 * would be no View Model and DTO, a lot less code, and an outer-join which
 * would be good for performance.
 * </p>
 * <p>
 * We use a View Model and a DTO for 3 reasons:
 * <ul>
 * <li>We want to keep a lazy association between the user and the authorities,
 * because people will quite often do relationships with the user, and we don't
 * want them to get the authorities all the time for nothing (for performance
 * reasons). This is the #1 goal: we should not impact our users' application
 * because of this use-case.</li>
 * <li>Not having an outer join causes n+1 requests to the database. This is not
 * a real issue as we have by default a second-level cache. This means on the
 * first HTTP call we do the n+1 requests, but then all authorities come from
 * the cache, so in fact it's much better than doing an outer join (which will
 * get lots of data from the database, for each HTTP call).</li>
 * <li>As this manages users, for security reasons, we'd rather have a DTO
 * layer.</li>
 * </ul>
 * <p>
 * Another option would be to have a specific JPA entity graph to handle this
 * case.
 * </p>
 */
@RestController
@RequestMapping("/api")
public class UserResource {

	private final Logger log = LoggerFactory.getLogger(UserResource.class);

	@Inject
	private UserRepository userRepository;
/*	
	@Inject
	GroupRepository groupRepository;*/
	
	

	/*@Inject
	private MailService mailService;*/

	@Inject
	private UserService userService;

	/**
	 * POST /users : Creates a new user.
	 * <p>
	 * Creates a new user if the login and email are not already used, and sends
	 * an mail with an activation link. The user needs to be activated on
	 * creation.
	 * </p>
	 *
	 * @param managedUserVM
	 *            the user to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new user, or with status 400 (Bad Request) if the login or email
	 *         is already in use
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/users")
	@Timed
	@Secured(AuthoritiesConstants.ADMIN)
	public ResponseEntity<?> createUser(@RequestBody UserDTO userDTO) throws URISyntaxException {
		log.debug("REST request to save User : {}", userDTO);

		// ManagedUserVM managedUserVM=(ManagedUserVM)userDTO;
		

		ManagedUserVM managedUserVM = new ManagedUserVM(userDTO.getLogin(), userDTO.getLogin(), userDTO.getPassword(),
				userDTO.getFirstName(), userDTO.getLastName(), userDTO.getEmail(), userDTO.isActivated(),
				userDTO.getLangKey(), userDTO.getAuthorities(), SecurityUtils.getCurrentUserLogin(), ZonedDateTime.now(),SecurityUtils.getCurrentUserLogin(), ZonedDateTime.now(),userDTO.getCurrency(),userDTO.getDecimalSeparator(),userDTO.getLanguage(),userDTO.getDateFormat(),userDTO.getTimeZone());


		// Lowercase the user login before comparing with database
		if (userRepository.findOneByLogin(managedUserVM.getLogin()
				.toLowerCase()).isPresent()) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("userManagement", "userexists", "Login already exists"))
					.body(null);
		} else if (userRepository.findOneByEmail(managedUserVM.getEmail()).isPresent()) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("userManagement", "emailexists", "Email already exists"))
					.body(null);
		} else {
			User newUser = userService.createUser(managedUserVM);
			//mailService.sendCreationEmail(newUser);
			return ResponseEntity.created(new URI("/api/users/" + newUser.getLogin()))
					.headers(HeaderUtil.createAlert("userManagement.created", newUser.getLogin())).body(newUser);
		}
	}

	/**
	 * PUT /users : Updates an existing User.
	 *
	 * @param managedUserVM
	 *            the user to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         user, or with status 400 (Bad Request) if the login or email is
	 *         already in use, or with status 500 (Internal Server Error) if the
	 *         user couldn't be updated
	 */
	@PutMapping("/users")
	@Timed
	@Secured(AuthoritiesConstants.ADMIN)
	public ResponseEntity<ManagedUserVM> updateUser(@RequestBody ManagedUserVM managedUserVM) {
		log.debug("REST request to update User : {}", managedUserVM);
		Optional<User> existingUser = userRepository.findOneByEmail(managedUserVM.getEmail());
		if (existingUser.isPresent() && (!existingUser.get().getId().equals(managedUserVM.getId()))) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("userManagement", "emailexists", "E-mail already in use"))
					.body(null);
		}
		existingUser = userRepository.findOneByLogin(managedUserVM.getLogin().toLowerCase());
		if (existingUser.isPresent() && (!existingUser.get().getId().equals(managedUserVM.getId()))) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("userManagement", "userexists", "Login already in use"))
					.body(null);
		}
		
		userService.updateUser(managedUserVM.getId(), managedUserVM.getLogin(), managedUserVM.getFirstName(),
				managedUserVM.getLastName(), managedUserVM.getEmail(), managedUserVM.isActivated(),
				managedUserVM.getLangKey(), managedUserVM.getAuthorities(),managedUserVM.getCurrency(),managedUserVM.getDecimalSeparator(),managedUserVM.getLanguage(),managedUserVM.getDateFormat(),managedUserVM.getTimeZone());

		return ResponseEntity.ok().headers(HeaderUtil.createAlert("userManagement.updated", managedUserVM.getLogin()))
				.body(new ManagedUserVM(userService.getUserWithAuthorities(managedUserVM.getId())));
	}

	/**
	 * GET /users : get all users.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the ResponseEntity with status 200 (OK) and with body all users
	 * @throws URISyntaxException
	 *             if the pagination headers couldn't be generated
	 */
	@GetMapping("/users")
	@Timed
	public ResponseEntity<List<ManagedUserVM>> getAllUsers(@ApiParam Pageable pageable) throws URISyntaxException {
		Page<User> page = userRepository.findAll(pageable);
		List<ManagedUserVM> managedUserVMs = page.getContent().stream().map(ManagedUserVM::new)
				.collect(Collectors.toList());
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/users");
		return new ResponseEntity<>(managedUserVMs, headers, HttpStatus.OK);
	}

	/**
	 * GET /users/:login : get the "login" user.
	 *
	 * @param login
	 *            the login of the user to find
	 * @return the ResponseEntity with status 200 (OK) and with body the "login"
	 *         user, or with status 404 (Not Found)
	 */
	@GetMapping("/users/{login:" + Constants.LOGIN_REGEX + "}")
	@Timed
	public ResponseEntity<ManagedUserVM> getUser(@PathVariable String login) {
		log.debug("REST request to get User : {}", login);
		return userService.getUserWithAuthoritiesByLogin(login).map(ManagedUserVM::new)
				.map(managedUserVM -> new ResponseEntity<>(managedUserVM, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /users/:login : delete the "login" User.
	 *
	 * @param login
	 *            the login of the user to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/users/{login:" + Constants.LOGIN_REGEX + "}")
	@Timed
	@Secured(AuthoritiesConstants.ADMIN)
	public ResponseEntity<Void> deleteUser(@PathVariable String login) {
		log.debug("REST request to delete User: {}", login);
		userService.deleteUser(login);
		return ResponseEntity.ok().headers(HeaderUtil.createAlert("userManagement.deleted", login)).build();
	}

	/*@GetMapping("/users/search")
	@Timed
	public List<Object> search(@ApiParam(value = "Search String") @RequestParam String q,@ApiParam(value = "type") @RequestParam String type) throws URISyntaxException {
		String regexp = q;
		System.out.println(regexp + "nnnnn");
		List<Object> resultList = new ArrayList<Object>();
		
		if(type.equalsIgnoreCase("User")) {
			Map<String,User> users = new HashMap<>();
			List<User> users1 = userRepository.findByFirstNameLikeOrderByIdAsc(regexp);
			// List<User> users1=userRepository.findByFirstNameStartingWith(q);
			for (User user : users1) {
				users.put(user.getLogin(), user);
			}
			List<User> users2 = userRepository.findByLastNameLikeOrderByIdAsc(regexp);
			for (User user : users2) {
				users.put(user.getLogin(), user);
			}
			List<User> users3 = userRepository.findByLoginLikeOrderByIdAsc(regexp);
			for (User user : users3) {
				users.put(user.getLogin(), user);
			}
			List<User> users4 = userRepository.findByEmailLikeOrderByIdAsc(regexp);
			for (User user : users4) {
				users.put(user.getLogin(), user);
			}
			
		    
			

			ArrayList<UserSearchVM> arList = new ArrayList<UserSearchVM>();

			for (Map.Entry<String, User> map : users.entrySet()) {
	       User user=map.getValue();
				arList.add(new UserSearchVM(user.getLogin(), user.getFirstName(), user.getLastName(), user.getEmail()));

			}

			resultList.add(arList);	
		}else if(type.equalsIgnoreCase("Group")) {
			List<GroupSearchVM> groupSearchResult = new ArrayList<GroupSearchVM>();
			List<Group> listGroup = groupRepository.findByQuery(regexp);
			
			for(Group group: listGroup) {
				
				GroupSearchVM groupSearchVm = new GroupSearchVM();
				
				BeanUtils.copyProperties(group, groupSearchVm);
				groupSearchResult.add(groupSearchVm);
			}
			
			resultList.add(groupSearchResult);
			
		}
		
		
		
		return resultList;

	}*/
	@PostMapping("/createuploadUserList")
	 @Timed
	 @Secured(AuthoritiesConstants.ADMIN)
	 public ResponseEntity<?> uploadUserList(@RequestBody List<UserDTO> list) throws URISyntaxException, IOException { 
	  //System.out.println("UploadUserList in User Resource:"+list.size());
	  
	  List<ManagedUserVM> uservmList=new ArrayList<ManagedUserVM>();
	  
	  Set<String> authorites=new HashSet<String>();
	  authorites.add("ROLE_USER");
	  
	  for(UserDTO userDTO:list) {
	   
	   userDTO.setPassword("Smartdocs@123");
	   userDTO.setAuthorities(authorites);
	   userDTO.setActivated(true);
	   
	   ManagedUserVM managedUserVM = new ManagedUserVM(userDTO.getLogin().trim(), userDTO.getLogin().trim(), userDTO.getPassword(),
	     userDTO.getFirstName().trim(), userDTO.getLastName().trim(), userDTO.getEmail().trim(), userDTO.isActivated(),
	     userDTO.getLangKey(), userDTO.getAuthorities(), SecurityUtils.getCurrentUserLogin(), 
	     ZonedDateTime.now(),SecurityUtils.getCurrentUserLogin(), ZonedDateTime.now(),userDTO.getCurrency(),
	     userDTO.getDecimalSeparator(),userDTO.getLanguage(),userDTO.getDateFormat(),userDTO.getTimeZone());
	   
	   
	   if (userRepository.findOneByLogin(managedUserVM.getLogin().toLowerCase()).isPresent()) {
	    return ResponseEntity.badRequest()
	      .headers(HeaderUtil.createFailureAlert("userManagement", "userexists", "Login already exists--->"+ managedUserVM.getLogin()))
	      .body(null);
	   } else if (userRepository.findOneByEmail(managedUserVM.getEmail()).isPresent()) {
	    return ResponseEntity.badRequest()
	      .headers(HeaderUtil.createFailureAlert("userManagement", "emailexists", "Email already exists--->"+managedUserVM.getEmail()))
	      .body(null);
	   }
	   
	   
	   uservmList.add(managedUserVM);
	  }//end for each loop
	  
	  
	  List<User> usersFinalList=userService.createuploadUser(uservmList);  
	   return ResponseEntity.ok()
	     .headers(HeaderUtil.createEntityCreationAlert("UploadUsers",usersFinalList.toString())).build();
	  
	 }
	
	
	
}
