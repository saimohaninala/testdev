package com.smartdocs.smartportal.web.rest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import javax.inject.Inject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.smartdocs.smartportal.service.FileStorageDao;
import com.smartdocs.smartportal.service.impl.FileStorageDaoImpl;

@RestController
@RequestMapping("/test")
public class RestControllerAPIs {
	 @Inject
	  GridFsTemplate gridFsTemplate;

	
	@GetMapping("/save")
	public String saveFiles() throws FileNotFoundException {
		DBObject metaData = new BasicDBObject();
		metaData.put("organization", "JavaSampleApproach");
		
		/**
		 * 1. save an image file to MongoDB
		 */
		
		// Get input file
		InputStream iamgeStream = new FileInputStream("C:\\Users\\Mahesh\\Desktop\\test.txt");
		
		metaData.put("type", "image");
		//return fileStorageDao.store(iamgeStream, "sample.txt", "application/txt", metaData);
		
		return this.gridFsTemplate
			    .store(iamgeStream, "sample.txt", "application/txt", metaData).getId()
			    .toString();

		
	}


}
