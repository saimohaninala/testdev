package com.smartdocs.smartportal.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.smartdocs.smartportal.domain.Company;
import com.smartdocs.smartportal.service.CompanyService;
import com.smartdocs.smartportal.web.rest.util.HeaderUtil;
import com.smartdocs.smartportal.web.rest.util.PaginationUtil;
import com.smartdocs.smartportal.web.rest.util.ResponseUtil;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

/**
 * REST controller for managing BusinessObjects.
 */
@RestController
@RequestMapping("/api")
public class CompanyResource {

    private final Logger log = LoggerFactory.getLogger(CompanyResource.class);

    private static final String ENTITY_NAME = "company";
    
    @Inject
	private CompanyService companyService;
   
	
	@PostMapping("/company")
    @Timed
    public ResponseEntity<Company> createCompany(@RequestBody Company companyrequest) throws URISyntaxException {
        log.debug("REST request to save Systems : {}", companyrequest);
        
        
        if (companyService.findOneByName(companyrequest.getName().toLowerCase()).isPresent()) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("CompanyRequestManagement", "company", "company already in use"))
					.body(null);
		} 
        
        Company company=new Company();
        company.setId(companyrequest.getName());
        company.setName(companyrequest.getName());
        company.setCompanyId(companyrequest.getCompanyId());
        company.setDescription(companyrequest.getDescription());
        company.setAddress(companyrequest.getAddress());
        company.setContact(companyrequest.getContact());
        Company result  =  companyService.save(company);
        
       
        return ResponseEntity.created(new URI("/api/company/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
	
	@PutMapping("/company")
    @Timed
    public ResponseEntity<Company> updateCompany(@RequestBody Company company) throws URISyntaxException {
	 log.debug("REST request to save company : {}", company);
	 if (company.getId() == null) {
         //  return createBinder(binder);
       }
	 Company result  =  companyService.save(company);
       return ResponseEntity.ok()
           .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
           .body(result);
    }
	
	@GetMapping("/company")
    @Timed
    public ResponseEntity<List<Company>> getAllCompanies(@ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of company");
        Page<Company> page = companyService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/company");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
	
	 @GetMapping("/company/{companyId}")
	 @Timed
	 public ResponseEntity<Company> getCompany(@PathVariable String companyId) {
	        log.debug("REST request to get company : {}", companyId);
	        Company company = companyService.findOneByCompanyId(companyId);
	        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(company));
	 }
	 
	 @DeleteMapping("/company/{id}")
	 @Timed
	 public ResponseEntity<Void> deleteCompany(@PathVariable String id) {
	        log.debug("REST request to delete company : {}", id);
	        companyService.delete(id);
	        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	 }
	
	
}

