package com.smartdocs.smartportal.web.rest;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Collection;
import com.codahale.metrics.annotation.Timed;
import com.google.gson.Gson;
import com.mongodb.gridfs.GridFSDBFile;
import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.domain.Systems;
import com.smartdocs.smartportal.service.MimeTypeService;
import com.smartdocs.smartportal.service.RepositoryService;
import com.smartdocs.smartportal.service.SystemsService;

import com.smartdocs.smartportal.service.util.Base64;
import com.smartdocs.smartportal.web.rest.util.HeaderUtil;
import com.smartdocs.smartportal.web.rest.util.PaginationUtil;
import com.smartdocs.smartportal.web.rest.util.ResponseUtil;
import com.smartdocs.smartportal.web.rest.vm.CertificateVM;

import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api")
public class SystemsResource {

	private final Logger log = LoggerFactory.getLogger(SystemsResource.class);
	private static final String ENTITY_NAME = "systems";

	@Inject
	private SystemsService systemsService;
	@Inject
	private RepositoryService repositoryService;
	@Inject
	GridFsTemplate gridFsTemplate;

	@PostMapping("/systems")
	@Timed
	public ResponseEntity<Systems> createSystems(@RequestBody Systems systemsrequest) throws URISyntaxException {
		log.debug("REST request to save Systems : {}", systemsrequest);

		if (systemsService.findOneBySystemName(systemsrequest.getSystemName().toLowerCase()).isPresent()) {
			return ResponseEntity.badRequest().headers(
					HeaderUtil.createFailureAlert("systemsManagement", "systemerxists", "System already in use"))
					.body(null);
		}

		Systems systems = new Systems();
		systems.setId(systemsrequest.getSystemId().toUpperCase());
		systems.setSystemId(systemsrequest.getSystemId().toUpperCase());
		systems.setSystemName(systemsrequest.getSystemName());
		systems.setSystemType(systemsrequest.getSystemType());
		systems.setDescription(systemsrequest.getDescription());
		systems.setAuthId(systemsrequest.getAuthId());
		systems.setAccessMode(systemsrequest.getAccessMode());
		systems.setExpiration(systemsrequest.getExpiration());
		systems.setCertificate(systemsrequest.getCertificate());
		systems.setCertificateStatus(systemsrequest.getCertificateStatus());

		Systems result = systemsService.save(systems);
		return ResponseEntity.created(new URI("/api/systems/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString())).body(result);
	}

	@PutMapping("/systems")
	@Timed
	public ResponseEntity<Systems> updateSystems(@RequestBody Systems systems) throws URISyntaxException {
		log.debug("REST request to save systems : {}", systems);
		if (systems.getId() == null) {
			// return createBinder(binder);
		}
		Systems result = systemsService.save(systems);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, systems.getId().toString()))
				.body(result);
	}

	@GetMapping("/systems")
	@Timed
	public ResponseEntity<List<Systems>> getAllSystems(@ApiParam Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of Systems");
		Page<Systems> page = systemsService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/systems");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@GetMapping("/systems/{id}")
	@Timed
	public ResponseEntity<Systems> getSystemsById(@PathVariable String id) {
		log.debug("REST request to get Systems : {}", id);
		Systems systems = systemsService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(systems));
	}

	@DeleteMapping("/systems/{id}")
	@Timed
	public ResponseEntity<Void> deleteSystems(@PathVariable String id) {
		log.debug("REST request to delete Systems : {}", id);
		systemsService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	@GetMapping("/systems/{id}/repositories")
	@Timed
	public List<Repository> getRepositoriesBySystemId(@PathVariable String id) {
		return repositoryService.findOneBySystemId(id);
	}

	/*
	 * @GetMapping("/systems/authId/{authId}") public
	 * ResponseEntity<CertificateVM> getCertificateFromFileSystem(@PathVariable
	 * String authId) throws CertificateException, IOException { InputStream
	 * in=null;
	 * 
	 * X509Certificate certificate = null;
	 * 
	 * System.out.println(authId);
	 * 
	 * List<Repository> repository= repositoryService.findAllList();
	 * 
	 * Repository repo= repository.get(0);
	 * 
	 * try {
	 * 
	 * File file =new
	 * File(repo.getRootPath()+File.separator+"certificate"+File.separator+
	 * authId); in = new FileInputStream(file);
	 * 
	 * CertificateFactory factory = CertificateFactory .getInstance("X.509");
	 * Collection collect = factory.generateCertificates(in);
	 * 
	 * for (Iterator i = collect.iterator(); i.hasNext();) { certificate =
	 * (X509Certificate) i.next(); }
	 * 
	 * }catch(Exception e) { e.printStackTrace(); }finally { if (in != null) {
	 * 
	 * in.close(); } }
	 * 
	 * 
	 * CertificateVM vm=new CertificateVM();
	 * vm.setCertificate(certificate.toString());
	 * 
	 * 
	 * return ResponseEntity.ok()
	 * .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, "")) .body(vm);
	 * }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @GetMapping("/systems/gdrive/authId/{authId}") public
	 * ResponseEntity<CertificateVM> getCertificateFromGdrive(@PathVariable
	 * String authId) throws CertificateException, IOException { InputStream
	 * in=null;
	 * 
	 * X509Certificate certificate = null;
	 * 
	 * System.out.println(authId);
	 * 
	 * Systems systems= systemsService.findOneBySystemAuthId(authId);
	 * 
	 * 
	 * List<Repository> repository= repositoryService.findAllList();
	 * 
	 * Repository repo= repository.get(0);
	 * 
	 * try {
	 * 
	 * AdminAccessFunctions
	 * accessFunctions=AdminAccessFunctions.getInstance(authId,
	 * repo.getRepositoryId(), repo.getStorageType());
	 * 
	 * 
	 * byte[]
	 * byteData=accessFunctions.getComponentInputStreamUsingResourceIdExt(
	 * systems.getCertificateResourceId(), "", "", "");
	 * 
	 * InputStream inStream = new ByteArrayInputStream(byteData);
	 * 
	 * CertificateFactory factory = CertificateFactory .getInstance("X.509");
	 * Collection collect = factory.generateCertificates(inStream);
	 * 
	 * for (Iterator i = collect.iterator(); i.hasNext();) { certificate =
	 * (X509Certificate) i.next(); }
	 * 
	 * }catch(Exception e) { e.printStackTrace(); }finally { if (in != null) {
	 * 
	 * in.close(); } }
	 * 
	 * 
	 * CertificateVM vm=new CertificateVM();
	 * vm.setCertificate(certificate.toString());
	 * 
	 * 
	 * return ResponseEntity.ok()
	 * .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, "")) .body(vm);
	 * }
	 */

	/*
	 * @GetMapping("/systems/getcertificate/authId/{authId}") public
	 * ResponseEntity<CertificateVM>
	 * getCertificateFromFileSystemAndGdrive(@PathVariable String authId) throws
	 * CertificateException, IOException { InputStream in=null; X509Certificate
	 * certificate = null; System.out.println(authId); List<Repository>
	 * repository= repositoryService.findAllList(); Repository repo=
	 * repository.get(0); Systems systems=
	 * systemsService.findOneBySystemAuthId(authId); try {
	 * 
	 * if(repo.getStorageType().equalsIgnoreCase("gdrive")) {
	 * AdminAccessFunctions
	 * accessFunctions=AdminAccessFunctions.getInstance(authId,
	 * repo.getRepositoryId(), repo.getStorageType()); byte[] byteData= null; in
	 * = new ByteArrayInputStream(byteData); }else { File file =new
	 * File(repo.getRootPath()+File.separator+"certificate"+File.separator+
	 * authId); in = new FileInputStream(file); } CertificateFactory factory =
	 * CertificateFactory .getInstance("X.509"); Collection collect =
	 * factory.generateCertificates(in); for (Iterator i = collect.iterator();
	 * i.hasNext();) { certificate = (X509Certificate) i.next(); }
	 * 
	 * }catch(Exception e) { e.printStackTrace(); }finally { if (in != null) {
	 * in.close(); } }
	 * 
	 * CertificateVM vm=new CertificateVM();
	 * vm.setCertificate(certificate.toString()); return ResponseEntity.ok()
	 * .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, "")) .body(vm);
	 * }
	 */

	@PutMapping("/systems/authId/{authId}/status/{status}")
	public ResponseEntity<Systems> updateSystemAfterCertificateAccept(@PathVariable String authId,
			@PathVariable String status) throws CertificateException, IOException {

		Systems system = systemsService.findOneBySystemAuthId(authId);
		system.setCertificateStatus(status);
		Systems result = systemsService.save(system);

		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, system.getId().toString()))
				.body(result);
	}

	@GetMapping("/systems/certificates/{id}")
	public ResponseEntity<CertificateVM> getCertficateBySystemId(@PathVariable String id)
			throws CertificateException, IOException {
		InputStream in = null;
		X509Certificate certificate = null;

		GridFSDBFile file = gridFsTemplate.findOne(new Query(Criteria.where("filename").is(id)));
		System.out.println(file);
		in = file.getInputStream();
		try {

			CertificateFactory factory = CertificateFactory.getInstance("X.509");
			Collection collect = factory.generateCertificates(in);
			for (Iterator i = collect.iterator(); i.hasNext();) {
				certificate = (X509Certificate) i.next();
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
			}
		}

		CertificateVM vm = new CertificateVM();
		vm.setCertificate(certificate.toString());
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, "")).body(vm);
	}

}
