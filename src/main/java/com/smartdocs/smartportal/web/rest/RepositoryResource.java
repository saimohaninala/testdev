package com.smartdocs.smartportal.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import com.codahale.metrics.annotation.Timed;
import com.smartdocs.smartportal.config.JHipsterProperties;
import com.smartdocs.smartportal.constant.SmartstoreConstant;
import com.smartdocs.smartportal.domain.Company;
import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.service.CompanyService;
import com.smartdocs.smartportal.service.RepositoryService;
import com.smartdocs.smartportal.smartstore.GdriveUtils;
import com.smartdocs.smartportal.smartstore.OneDriveOauthUtils;
import com.smartdocs.smartportal.web.rest.util.HeaderUtil;
import com.smartdocs.smartportal.web.rest.util.PaginationUtil;
import com.smartdocs.smartportal.web.rest.util.ResponseUtil;

import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api")
public class RepositoryResource {
	private final Logger log = LoggerFactory.getLogger(RepositoryResource.class);

	private static final String ENTITY_NAME = "repository";

	@Inject
	private RepositoryService repositoryService;
	@Inject
	private JHipsterProperties jHipsterProperties;

	@PostMapping("/repository")
	@Timed
	public Repository createRepository(@RequestBody Repository repository,HttpServletResponse httpResponse) throws Exception {
		log.debug("REST request to save repository : {}", repository);
		Repository result=null;

		System.out.println("Root Path:" + repository.getRootPath());

		if (repositoryService.findOneByName(repository.getName().toUpperCase()).isPresent()) {
			
		}

		Repository rep = new Repository();
		rep.setId(repository.getSystemId().toUpperCase() + repository.getRepositoryId().toUpperCase());
		rep.setName(repository.getName().toUpperCase());
		rep.setRepositoryId(repository.getRepositoryId().toUpperCase());
		rep.setDescription(repository.getDescription().toUpperCase());
		rep.setpVersion(repository.getpVersion());
		rep.setStorageType(repository.getStorageType());
		rep.setClientId(repository.getClientId());
		rep.setClientSecret(repository.getClientSecret());
		rep.setRootPath(repository.getRootPath());
		rep.setSystemId(repository.getSystemId().toUpperCase());
		result = repositoryService.save(rep);
		
		if (repository.getStorageType().equalsIgnoreCase(SmartstoreConstant.SS_GDRIVE_STORAGE_SYSTEM)) {
			rep.setRootPath(jHipsterProperties.getGdrive().getRedirectUri());
	if(result!=null)
			result.setRootPath(GdriveUtils.authorize(rep)); ;
			
		} else if (repository.getStorageType().equalsIgnoreCase(SmartstoreConstant.SS_ONEDRIVE_STORAGE_SYSTEM)) {
			rep.setRootPath(jHipsterProperties.getOnedrive().getRedirectUri());
			if(result!=null)

			result.setRootPath(OneDriveOauthUtils.authorize(rep)); ;

		
		}else{
			  return result;
		}
     return result;
	}

	@GetMapping("/repository")
	@Timed
	public ResponseEntity<List<Repository>> getAllRepositories(@ApiParam Pageable pageable) throws URISyntaxException {
		log.debug("REST request to get a page of repository");
		Page<Repository> page = repositoryService.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/repository");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	@PutMapping("/repository")
	@Timed
	public ResponseEntity<Repository> updateRepository(@RequestBody Repository rep) throws URISyntaxException {
		log.debug("REST request to save repository : {}", rep);
		if (rep.getId() == null) {

		}
		Repository result = repositoryService.save(rep);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
				.body(result);
	}

	@GetMapping("/repository/{repositoryId}")
	@Timed
	public ResponseEntity<Repository> getRepository(@PathVariable String repositoryId) {
		log.debug("REST request to get repository : {}", repositoryId);
		Repository repository = repositoryService.findOneByrepositoryId(repositoryId);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(repository));
	}

	@DeleteMapping("/repository/{id}")
	@Timed
	public ResponseEntity<Void> deleteRepository(@PathVariable String id) {
		log.debug("REST request to delete repository : {}", id);
		repositoryService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

}
