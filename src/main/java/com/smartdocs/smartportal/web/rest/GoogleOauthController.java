package com.smartdocs.smartportal.web.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.util.SystemPropertyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import com.google.api.client.auth.oauth2.AuthorizationCodeRequestUrl;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets.Details;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;

import com.google.api.services.drive.Drive.Files;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.smartdocs.smartportal.config.JHipsterProperties;
import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.repository.GdriveTokenRepository;
import com.smartdocs.smartportal.repository.RepositoryRepository;
import com.smartdocs.smartportal.service.RepositoryService;
import com.smartdocs.smartportal.service.UserService;

@RestController
@RequestMapping("/api/google")
public class GoogleOauthController {
	

    @Inject
    private JHipsterProperties jHipsterProperties;

    @Inject
    private UserService userService;
    @Inject
    private GdriveTokenRepository gdrivetokenRepository;
    @Inject
 		private RepositoryRepository repositoryRepository;


    private static final String APPLICATION_NAME = "oauth drive";
	private static HttpTransport httpTransport;
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
    private static com.google.api.services.drive.Drive service;

    GoogleClientSecrets clientSecrets;
    GoogleAuthorizationCodeFlow flow;
    Credential credential;

 

    @GetMapping("/login/drive")
    public RedirectView googleConnectionStatus(HttpServletRequest request) throws Exception {
        return new RedirectView(authorize(request));
    }
    
    @GetMapping("/redirect")
    public String oauth2Callback(@RequestParam(value = "code") String code,@RequestParam(value = "state") String state) {
             String result="Failed";
             System.out.println("test");
        // System.out.println("code->" + code + " userId->" + userId + "
        // query->" + query);
            Repository repository= repositoryRepository.findOne(state);
            
        JSONObject json = new JSONObject();
        JSONArray arr = new JSONArray();
      
      
        try {
        	 String redirectUri= jHipsterProperties.getGdrive().getRedirectUri();
            TokenResponse response = newFlow(repository).newTokenRequest(code).setGrantType("authorization_code").setRedirectUri(redirectUri).execute();
            System.out.println(response.getAccessToken()+">>>>>>>>>>>>>>>>");
            System.out.println(response.getRefreshToken()+">>>>>>>>>>>>>>");
            credential = newFlow(repository).createAndStoreCredential(response, state);
            if(credential!=null){
            	result="Successfully Authenticated, Please Close The Brower";
            }else{
            	repositoryRepository.delete(state);
            	
            }
        }catch (Exception e) {
        	e.printStackTrace();
        	result=e.getMessage();
		}
           
        return result;
        
    }

    private String authorize(HttpServletRequest request) throws Exception {
    	
    	 String redirectUri= jHipsterProperties.getGdrive().getRedirectUri();
        AuthorizationCodeRequestUrl authorizationUrl;
        if (flow == null) {
      //  String clientId=	jHipsterProperties.getGdrive().getClientId();
      //  String clientSecret=jHipsterProperties.getGdrive().getClientSecret();
       
        	   
            Details web = new Details();
         //   web.setClientId(clientId);
           // web.setClientSecret(clientSecret);
            clientSecrets = new GoogleClientSecrets().setWeb(web);
            httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            flow = new GoogleAuthorizationCodeFlow.Builder(httpTransport, JSON_FACTORY, clientSecrets,
                    Collections.singleton(DriveScopes.DRIVE)).setDataStoreFactory(new MongoRepoDataStoreFactory(gdrivetokenRepository)).setAccessType("offline").setApprovalPrompt("force").build();
        }
        authorizationUrl = flow.newAuthorizationUrl().setRedirectUri(redirectUri).setState(request.getParameter("repository"));

        System.out.println("drive authorizationUrl ->" + authorizationUrl.build());
        return authorizationUrl.build();
    }
    
    private String authorize(Repository repository) throws Exception {
    	
    	 String redirectUri= jHipsterProperties.getGdrive().getRedirectUri();
        AuthorizationCodeRequestUrl authorizationUrl;
        if (flow == null) {
        String clientId=	repository.getClientId();
        String clientSecret=repository.getClientSecret();
       
        	   
            Details web = new Details();
            web.setClientId(clientId);
            web.setClientSecret(clientSecret);
            clientSecrets = new GoogleClientSecrets().setWeb(web);
            httpTransport = GoogleNetHttpTransport.newTrustedTransport();
            flow = new GoogleAuthorizationCodeFlow.Builder(httpTransport, JSON_FACTORY, clientSecrets,
                    Collections.singleton(DriveScopes.DRIVE)).setDataStoreFactory(new MongoRepoDataStoreFactory(gdrivetokenRepository)).setAccessType("offline").setApprovalPrompt("force").build();
        }
        authorizationUrl = flow.newAuthorizationUrl().setRedirectUri(redirectUri).setState(repository.getId());

       
        return authorizationUrl.build();
    }
     GoogleAuthorizationCodeFlow newFlow(Repository repository) throws IOException { 
        Details web = new Details();
        String clientId=	repository.getClientId();
      String clientSecret=repository.getClientSecret();
        
        web.setClientId(clientId);
        web.setClientSecret(clientSecret);

        return  new GoogleAuthorizationCodeFlow.Builder(new NetHttpTransport(),
                new JacksonFactory(), new GoogleClientSecrets().setWeb(web),
                Collections.singleton(DriveScopes.DRIVE)).setDataStoreFactory(new MongoRepoDataStoreFactory(gdrivetokenRepository)).setAccessType("offline").build();
      }

    
   /* public  void main(String a[]) throws IOException{
        Credential credential = newFlow().loadCredential("userID");
        java.util.List<File> result = new ArrayList<File>();

        service = new com.google.api.services.drive.Drive.Builder(new NetHttpTransport(),
                new JacksonFactory(), credential)
                .setApplicationName(APPLICATION_NAME).build();
      System.out.println(service);
        Files.List request = service.files().list();

        do {
              try {
                FileList files = request.execute();
                result.addAll(files.getFiles());

                request.setPageToken(files.getNextPageToken());
              } catch (IOException e) {
                System.out.println("An error occurred: " + e);
                request.setPageToken(null);
              }
            } while (request.getPageToken() != null &&
                     request.getPageToken().length() > 0);



         System.out.println(result.size());
    	
    }*/

    
    
}