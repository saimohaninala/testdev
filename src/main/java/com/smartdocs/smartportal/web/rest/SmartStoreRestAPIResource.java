/*package com.smartdocs.smartportal.web.rest;


import java.io.IOException;

import java.io.UnsupportedEncodingException;

import java.net.URLDecoder;


import javax.inject.Inject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.codahale.metrics.annotation.Timed;
import com.smartdocs.smartportal.constant.SmartstoreConstant;
import com.smartdocs.smartportal.domain.Company;

import com.smartdocs.smartportal.service.AcceessFuncitonsAPI;
import com.smartdocs.smartportal.service.AdminFuncitonsAPI;
import com.smartdocs.smartportal.service.CompanyService;






@RestController
@RequestMapping("/api")
public class SmartStoreRestAPIResource {
	private final Logger log = LoggerFactory.getLogger(SmartStoreRestAPIResource.class);

	@Inject
	CompanyService companyService;
	@Inject
	AdminFuncitonsAPI adminFunctionsAPI;
	@Inject
	AcceessFuncitonsAPI accessFunctionsAPI;
	
	public String serverName=null;

	@RequestMapping(value="/smartdocs", method = {RequestMethod.GET,RequestMethod.POST})
	@Timed
	public void smartstore(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
	

		log.info("Request came from " + request.getLocalAddr() + ":" + request.getLocalPort());
		log.info("In doPost Method of the SmartStoreHandler " + request.getQueryString());

		StringBuffer url = request.getRequestURL();

		
		
		
		int index = url.indexOf("smartdocs");
		System.out.println("url:::::::::::::::::::::::::::::::::::::::"+url);
		String companyName = decode(url.substring(url.lastIndexOf("/") + 1));
		System.out.println(companyName);
		
		Company company = companyService.findOne(companyName);
		
		System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"+company.getName());
		
		System.out.println("Company Name:"+company.getName());
		
		if (company == null || company.equals("")) {
			log.info("compnay Name ='" + companyName + "' is not found in Datastore");
			response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
			response.setHeader(SmartstoreConstant.xErrorDescription,
					"compnay Name ='" + companyName + "' is not found in Datastore");
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
					"compnay Name ='" + companyName + "' is not found in Datastore");
			return;
		} else {
			request.setAttribute("compnay", company);
			response = parseQueryString(request, response);
		}

	}

	public HttpServletResponse parseQueryString( HttpServletRequest request, HttpServletResponse response)
			throws IOException {
		
		
		String queryString = request.getQueryString();
		
		System.out.println("queryString:"+queryString);
		
		
		
		
		
		if (queryString.startsWith("get")) {
			
			
		response=accessFunctionsAPI.Get(request, response);
			
			
			
		} else if (queryString.startsWith("docGet")) {
			
		response=accessFunctionsAPI.DocGet(request, response);
			
		} else if (queryString.startsWith("delete")) {
			response=accessFunctionsAPI.Delete(request, response);
		} else if (queryString.startsWith("append")) {
			
		} else if (queryString.startsWith("attrSearch")) {

		} else if (queryString.startsWith("mCreate")) {
			
		} else if (queryString.startsWith("search")) {
			
		} else if (queryString.startsWith("update")) {
			
		} else if (queryString.startsWith("info")) {
		response=	accessFunctionsAPI.Info(request, response);
			
		} else if (queryString.startsWith("create")) {
		
			
			
			try {
				response=accessFunctionsAPI.createDoc(request, response) ;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			
		} else if (queryString.startsWith("putCert")) {
			
		
		
			try {
		response=adminFunctionsAPI.setPutCert(request, response);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		} else if (queryString.startsWith("serverInfo")) {
			
		response=adminFunctionsAPI.serverInfo(request, response);
			
			
			
		} else if (queryString.startsWith("UpdateMetaData")) {
			
			
			

		} else if (queryString.startsWith("UpdateDocumentMetaData")) {

		} else {
			// response.setStatus(500, "Method not found");
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Method not found");
		}
				return response;

	}

	public String decode(String url) {
		try {
			String prevURL = "";
			String decodeURL = url;
			while (!prevURL.equals(decodeURL)) {
				prevURL = decodeURL;
				decodeURL = URLDecoder.decode(decodeURL, "UTF-8");
			}
			return decodeURL;
		} catch (UnsupportedEncodingException e) {
			return "Issue while decoding" + e.getMessage();
		}
	}

}
*/