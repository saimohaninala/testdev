package com.smartdocs.smartportal.web.rest;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Object to return as body in JWT Authentication.
 */
public class JWTTokenMobile {

    private String idToken;
    
    private String userId;
    
    private String email;

    public JWTTokenMobile() {
		super();
		// TODO Auto-generated constructor stub
	}

	public JWTTokenMobile(String idToken) {
        this.idToken = idToken;
    }

  


	

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@JsonProperty("id_token")
    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }
    
    
}
