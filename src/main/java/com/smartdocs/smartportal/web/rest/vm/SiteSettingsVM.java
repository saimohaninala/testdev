package com.smartdocs.smartportal.web.rest.vm;

import java.util.Map;



public class SiteSettingsVM {
	

	
	
	private String siteSettingsName;
	
	
	
	 private Map<String,String> siteSettingsValue;

	
	public String getSiteSettingsName() {
		return siteSettingsName;
	}
	public void setSiteSettingsName(String siteSettingsName) {
		this.siteSettingsName = siteSettingsName;
	}
	public Map<String, String> getSiteSettingsValue() {
		return siteSettingsValue;
	}
	public void setSiteSettingsValue(Map<String, String> siteSettingsValue) {
		this.siteSettingsValue = siteSettingsValue;
	}
	
	
	
	
	
	
}
