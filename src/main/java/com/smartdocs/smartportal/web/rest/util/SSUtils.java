package com.smartdocs.smartportal.web.rest.util;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimeZone;

import javax.inject.Inject;

import org.apache.log4j.Logger;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;

import com.smartdocs.smartportal.domain.Certificate;
import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.domain.Systems;
import com.smartdocs.smartportal.service.CertificateService;
import com.smartdocs.smartportal.service.impl.CertificateServiceImpl;
import com.smartdocs.smartportal.service.util.Base64;



public class SSUtils {
	
	
	
	
	static Logger logger=Logger.getLogger(SSUtils.class+"");
	public static X509Certificate getX509Certificate1(String publicKey){
		X509Certificate certificate=null;
		try{
			logger.info("publicKey algorith="+publicKey);			
			 CertificateFactory factory=CertificateFactory.getInstance("X.509");
			
			  certificate=(X509Certificate) factory.generateCertificate(new ByteArrayInputStream(Base64.decode(publicKey.replaceAll("\n", ""))));	
			  logger.info(certificate);
		}catch (Exception e) {
			e.printStackTrace();
		}//System.out.println(certificate);
		return certificate;
	}
	
	
	
	public static X509Certificate getX509Certificate(InputStream in){
		X509Certificate certificate=null;
		try{
			
			 CertificateFactory factory=CertificateFactory.getInstance("X.509");
			
				
				Collection collect = factory.generateCertificates(in);
				for (Iterator i = collect.iterator(); i.hasNext();) {
					certificate = (X509Certificate) i.next();
				}
			 logger.info(certificate);
		}catch (Exception e) {
			e.printStackTrace();
		}//System.out.println(certificate);
		return certificate;
	}
	
	
	
	
	
	
	
	
	
	public static String checkPre_Conditions1(String authId,String secKey,String appendedText,String expiratoin,String repositoryId,Repository repositoryInfo,Systems system){
		String message=null;
		
		X509Certificate certificate=getX509Certificate1(system.getCertificate());
		try{
		
			if(repositoryInfo==null)
				message="Repository '"+repositoryId+"' is not configured on the company ''";
			
			//Checking url expiration
			/*if(repositoryInfo.getUrl_expiration().equalsIgnoreCase("Active")){
				if(!SSUtils.checkURLExpiration(expiratoin)){
					if(message==null)
						message="Url is expirred";
					else 
						message="\nUrl is expirred";
				}
			}*/
			
			
			certificate.checkValidity(new Date(System.currentTimeMillis()));
			
			//checking the certificate is validate from UI or not
			if(!system.getCertificateStatus().equalsIgnoreCase("Accept")){
				if(message==null)
					message="Certificate is not validated from UI";
				else
					message=message+"\nCertificate is not validated from UI";				
			}
			
			
			boolean flag=SeckeyValidation.validateSecKey(secKey, appendedText, certificate);
			
			System.out.println("SSutils88:"+flag);
			
			//SecKey validation
			if(!SeckeyValidation.validateSecKey(secKey, appendedText, certificate)){ 
				if(message==null)
					message="\nSecKey is not validated with appendedText='"+appendedText+"' secKey='"+secKey+"'";
				else
					message=message+"\nSecKey is not validated with appendedText='"+appendedText+"' secKey='"+secKey+"'";
			}
			
		}catch (Exception e) {
			message="Certificate is Expired :"+e.getMessage();
		}
		//System.out.println(message);
		return message;
	}
	
	public static String checkPre_Conditions(String authId,String secKey,String appendedText,String expiratoin,String repositoryId,Repository repositoryInfo,Systems system,InputStream certificatein){
		String message=null;
		
		X509Certificate certificate=getX509Certificate(certificatein);
		try{
		
			if(repositoryInfo==null)
				message="Repository '"+repositoryId+"' is not configured on the company ''";
			
		
			
			
			certificate.checkValidity(new Date(System.currentTimeMillis()));
			
			//checking the certificate is validate from UI or not
			if(!system.getCertificateStatus().equalsIgnoreCase("Accept")){
				if(message==null)
					message="Certificate is not validated from UI";
				else
					message=message+"\nCertificate is not validated from UI";				
			}
			
			
			boolean flag=SeckeyValidation.validateSecKey(secKey, appendedText, certificate);
			
			System.out.println("SSutils88:"+flag);
			
			//SecKey validation
			if(!SeckeyValidation.validateSecKey(secKey, appendedText, certificate)){ 
				if(message==null)
					message="\nSecKey is not validated with appendedText='"+appendedText+"' secKey='"+secKey+"'";
				else
					message=message+"\nSecKey is not validated with appendedText='"+appendedText+"' secKey='"+secKey+"'";
			}
			
		}catch (Exception e) {
			message="Certificate is Expired :"+e.getMessage();
		}
		//System.out.println(message);
		return message;
	}
	public static String attrSearchResults(String patternString,String caseSensitive,String temp,int fromOffsetInt,int toOffset){

		
		int count=0;
		String result="";
		System.out.println("patternString=="+patternString+" fromOffset='"+fromOffsetInt+"' toOffset='"+toOffset+"'");
		//System.out.println(patternString+" "+temp);
		String[] patternArray=patternString.split("#");
		String[][] pattern=new String[patternArray.length][3];
		for (int i = 0; i < patternArray.length; i++) {
			String[] temp9=patternArray[i].split("\\+");
			try{
			pattern[i][0]=temp9[0];
			pattern[i][1]=temp9[1];
			pattern[i][2]=temp9[2];
			}catch (Exception e) {
				try{
				temp9=patternArray[i].split(" ");
				pattern[i][0]=temp9[0];
				pattern[i][1]=temp9[1];
				pattern[i][2]=temp9[2];
				}catch (Exception e1) {
					// TODO: handle exception
				}
				
			}
		}
		
		
		/*String temp = "0 72 DPRL\n" + "73 0 DKEYclient 0 3\n"
				+ "73 0 DKEYcompany_code 3 5\n"
				+ "73 0 DKEYaccount_number 8 7\n"
				+ "73 0 DKEYcustomer_name 15 25\n"
				+ "73 138 DAIN00100010147119Broeselplc\n"
				+ "211 120 DAIN001000020147129Obelixpfdkjkgjdkgjsdfdsfadsfasdfadsfadsdfddsfadsfadsfasdfasfasdfafdsfadskgskfdjgksdfjskj;ljfc\n" + "1147 1 DEPL";
		*/
		/*StringTokenizer fileTokenizer1=new StringTokenizer(temp,"DKEY");
		
		String dkeyTable[][]=new String[(fileTokenizer1.countTokens()/2)][3];
		*/
		//int key=0;
		//System.out.println(fileTokenizer1.countTokens()/2);
		StringTokenizer fileTokenizer=new StringTokenizer(temp,"\n");
		
		while(fileTokenizer.hasMoreTokens()){
			String line=fileTokenizer.nextToken();
			
			
			if(line.indexOf("DAIN")!=-1){
				String param1=line.substring(0, line.indexOf(" "));
				line=line.substring(line.indexOf(" ")+1);
				String param2=line.substring(0, line.indexOf(" "));
				line=line.substring(line.indexOf(" ")+1);
				//System.out.println(param1+"$"+param2+"$"+line);
				line=line.substring(4);
				//System.out.println(param1+"$"+param2+"$"+line);
				boolean flag=true;
				for (int i = 0; i < pattern.length; i++) {
					String expPattern=line.substring(Integer.parseInt(pattern[i][0]),Integer.parseInt(pattern[i][0])+Integer.parseInt(pattern[i][1]));
					expPattern=expPattern.trim();
					if(null==caseSensitive ||caseSensitive.equalsIgnoreCase("n")){
						if(!expPattern.equalsIgnoreCase(pattern[i][2])){
							flag=false;
						}
					}else{
						if(!expPattern.equals(pattern[i][2])){
							flag=false;
						}
					}
				}
				
				if(flag){
					if(fromOffsetInt==0 && toOffset==-1){
						count++;
						result=result+";"+param1+";"+param2;
					}else if(fromOffsetInt>0 && toOffset==-1){
						try{int param1Int=Integer.parseInt(param1.trim());
						if(param1Int>fromOffsetInt){
							count++;
							result=result+";"+param1+";"+param2;
						}
						}catch (Exception e) {}
					}else if(toOffset!=-1){
						try{int param1Int=Integer.parseInt(param1.trim());
						if(fromOffsetInt>=param1Int && param1Int>=toOffset){
							count++;
							result=result+";"+param1+";"+param2;
						}
						}catch (Exception e) {}
							
					}
					
				}
				
				/*switch (pattern.length) {
				case 1:
					String expPattern=line.substring(Integer.parseInt(pattern[0][0]),Integer.parseInt(pattern[0][0])+Integer.parseInt(pattern[0][1]));
					expPattern=expPattern.trim();
					//System.out.println(expPattern);
					//System.out.println(line);
					//System.out.println(expPattern+"$"+pattern[0][2]);
					if(expPattern.equals(pattern[0][2])){
						count++;
							result=result+";"+param1+";"+param2;
					}
					break;
				case 2:
					String expPattern1=line.substring(Integer.parseInt(pattern[0][0]),Integer.parseInt(pattern[0][0])+Integer.parseInt(pattern[0][1]));
					expPattern1=expPattern1.trim();
					//System.out.println(line.length());
					String expPattern2=line.substring(Integer.parseInt(pattern[1][0]),Integer.parseInt(pattern[1][0])+Integer.parseInt(pattern[1][1]));
					expPattern2=expPattern2.trim();
					//System.out.println(expPattern1+"$"+expPattern2);
					//System.out.println(line);
					if(expPattern1.equals(pattern[0][2]) && expPattern2.equals(pattern[1][2])){
						count++;
							result=result+";"+param1+";"+param2;
					}
					break;
				case 3:
					String expPattern3=line.substring(Integer.parseInt(pattern[0][0]),Integer.parseInt(pattern[0][0])+Integer.parseInt(pattern[0][1]));
					expPattern3=expPattern3.trim();
					String expPattern4=line.substring(Integer.parseInt(pattern[1][0]),Integer.parseInt(pattern[1][0])+Integer.parseInt(pattern[1][1]));
					expPattern4=expPattern4.trim();
					String expPattern5=line.substring(Integer.parseInt(pattern[2][0]),Integer.parseInt(pattern[2][0])+Integer.parseInt(pattern[2][1]));
					expPattern5=expPattern5.trim();
					//System.out.println(expPattern1);
					//System.out.println(line);
					if(expPattern3.equals(pattern[0][2]) && expPattern4.equals(pattern[1][2]) && expPattern5.equals(pattern[2][2])){
						count++;
							result=result+";"+param1+";"+param2;
					}
					break;
					

				
				}*/
				
			}
			/*
			String line=fileTokenizer.nextToken();
			//System.out.println(line);
			String linetemp[]=line.split(" ");
			
			
			if(linetemp[2].startsWith("DPRL")){
				
				
			}else if(linetemp[2].startsWith("DKEY")){
				
				dkeyTable[key][0]=linetemp[2].replace("DKEY", "");
				dkeyTable[key][1]=linetemp[3];
				dkeyTable[key][2]=linetemp[4];
				key++;
				//System.out.println("DKEY="+table[0]+" "+table[1]+" "+table[2]+" "+table[3]+" "+table[4]+" "+table[5]);
				
				
				
				
			}else if(linetemp[2].startsWith("DAIN")){
				for (int i = 0; i < pattern.length; i++) {
					if(linetemp[2].contains(pattern[i][2])){
						count++;
						
							result=result+";"+linetemp[0]+";"+linetemp[1];
					//	System.out.println(linetemp[2].indexOf(pattern[i][2])+" "+pattern[i][0]);
						if(linetemp[2].indexOf(pattern[i][2])-1==Integer.parseInt(pattern[i][0]))
						System.out.println(linetemp[2]);
					}
				}
				
				
				
				
			}else if(linetemp[2].startsWith("DEPL")){
				
				
				//System.out.println("DEPL="+table[0]+" "+table[1]+" "+table[2]);
			}
			
			
		*/}
		
		
		System.out.println("*****************"+(count+result+";"));
	return count+result+";";
	}
	 public static String convertStreamToString(InputStream is) {
		 try{
		    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		    StringBuilder sb = new StringBuilder();
		    String line = null;
		    while ((line = reader.readLine()) != null) {
		      sb.append(line + "\n");
		    }
		    is.close();
		    return sb.toString();
		  }
		 catch (Exception e) {
			// TODO: handle exception
		}
		 return null;
	 }
	 public static String convertLocalTimeToUTC() throws Exception{
		 final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		    sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
		    
		    return  sdf.format(new Date());
		    

		 
	 }
	 // expired=false
	 // unexpired=true
	 public static boolean checkURLExpiration(String expiration){
		 boolean flag=false;
		 try{
			 
			 int urlYear=Integer.parseInt(expiration.substring(0,4));
			 int urlMonth=Integer.parseInt(expiration.substring(4,6));
			 int urlDay=Integer.parseInt(expiration.substring(6,8));
			 int urlHH=Integer.parseInt(expiration.substring(8,10));
			 int urlMM=Integer.parseInt(expiration.substring(10,12));
			 int urlSS=Integer.parseInt(expiration.substring(12,14));
			// System.out.println(urlYear+" "+urlMonth+" "+urlDay+" "+urlHH+" "+urlMM+" "+urlSS);
			 
			 String local=convertLocalTimeToUTC();
			 int localYear=Integer.parseInt(local.substring(0,4));
			 int localMonth=Integer.parseInt(local.substring(4,6));
			 int localDay=Integer.parseInt(local.substring(6,8));
			 int localHH=Integer.parseInt(local.substring(8,10));
			 int localMM=Integer.parseInt(local.substring(10,12));
			 int localSS=Integer.parseInt(local.substring(12,14));
			 //System.out.println(localYear+" "+localMonth+" "+localDay+" "+localHH+" "+localMM+" "+localSS);
			 
			 if(urlYear>localYear)
			  return true;
			 else if(urlYear==localYear){
				 if(urlMonth>localMonth)
					 return true;
				 else if(urlMonth==localMonth){
					 if(urlDay>localDay)
						 return true; 
					 else  if(urlDay==localDay){
						 if(urlHH>localHH)
							 return true;
						 else if(urlHH==localHH){
							 if(urlMM>localMM)
								 return true;
							 else{
								 if(urlSS<localSS)
									 return true;
								 else if(urlSS==localSS){
									 return true;
								 }
							 }
						 }
					 }
				 }
			 }
			// flag=true;
		 }catch (Exception e) {
			e.printStackTrace();
		}
		 return flag;
	 }
	 public static void main(String[] args) {
		try {
			System.out.println("expired="+checkURLExpiration("20000816072605"));
			System.out.println("unexpired="+checkURLExpiration("20130816072605"));
			System.out.println("expired="+checkURLExpiration("20110816072605"));
			System.out.println("unexpired="+checkURLExpiration("20111016072605"));			
			System.out.println("expired="+checkURLExpiration("20110919062605"));
			System.out.println("expired="+checkURLExpiration("20110919082605"));
			
			//System.out.println("expired="+checkURLExpiration("20110919082605"));
			//System.out.println("expired="+checkURLExpiration("20110919082605"));
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	 public byte[] readInputStream(InputStream in){
		 ByteArrayOutputStream out=new ByteArrayOutputStream();
		 try{
			 byte[] buffer = new byte[4500];  
			 for (int n; (n = in.read(buffer)) != -1; )   
	            out.write(buffer, 0, n);
		 }catch (Exception e) {
			e.printStackTrace();
		}
		 return out.toByteArray();
	 }
	 public static Map<String, String> processDescription(String desc){
		 Map<String, String> map=new HashMap<String, String>();
		 StringTokenizer tokenizer=new StringTokenizer(desc,",");
		 while (tokenizer.hasMoreElements()) {
			String object = (String) tokenizer.nextElement();
			String temp[]=object.split("=");
			map.put(temp[0], temp[1]);
		}
		 return map;
	 }
	/* public static String getDescription(Map<String, String> map,SapDocDataInfo sapDocDataInfo,String type){
		 String newDesc="";
		 try{
			 if(type.equalsIgnoreCase("doc")){
					if(map.containsKey("orgType")){
						newDesc=newDesc+"orgType="+map.get("orgType");
						if((map.get("orgType")+";").indexOf(sapDocDataInfo.getOrgType()+";")==-1)
							newDesc=newDesc+";"+sapDocDataInfo.getOrgType();
						
					}
					if(map.containsKey("orgId")){
						newDesc=newDesc+",orgId="+map.get("orgId");
						if((map.get("orgId")+";").indexOf(sapDocDataInfo.getOrgId()+";")==-1)
							newDesc=newDesc+";"+sapDocDataInfo.getOrgId();
						
					}
			 }else{
				 if(map.containsKey("objectType")){
						newDesc=newDesc+"objectType="+map.get("objectType");
						if((map.get("objectType")+";").indexOf(sapDocDataInfo.getObjectType()+";")==-1)
							newDesc=newDesc+";"+sapDocDataInfo.getObjectType();
						
					}
					if(map.containsKey("objectId")){
						newDesc=newDesc+",objectId="+map.get("objectId");
						if((map.get("objectId")+";").indexOf(sapDocDataInfo.getObjectId()+";")==-1)
							newDesc=newDesc+";"+sapDocDataInfo.getObjectId();
						
					}
					if(map.containsKey("documentType")){
						newDesc=newDesc+",documentType="+map.get("documentType");
						if((map.get("documentType")+";").indexOf(sapDocDataInfo.getDocumentType()+";")==-1)
							newDesc=newDesc+";"+sapDocDataInfo.getDocumentType();
						
					}
					if(map.containsKey("archId")){
						newDesc=newDesc+",archId="+map.get("archId");
						if((map.get("archId")+";").indexOf(sapDocDataInfo.getArchiveId()+";")==-1)
							newDesc=newDesc+";"+sapDocDataInfo.getArchiveId();
						
					}
	 				
			 }
		 }catch (Exception e) {
			e.printStackTrace();
		}
		 return newDesc;
	 }*/
}
