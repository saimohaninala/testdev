package com.smartdocs.smartportal.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.smartdocs.smartportal.domain.Company;
import com.smartdocs.smartportal.domain.SmartstoreSettings;
import com.smartdocs.smartportal.service.CompanyService;
import com.smartdocs.smartportal.service.SmartstoreSettingsService;
import com.smartdocs.smartportal.web.rest.util.HeaderUtil;
import com.smartdocs.smartportal.web.rest.util.PaginationUtil;
import com.smartdocs.smartportal.web.rest.util.ResponseUtil;

import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api")
public class SmartstoreSettingsResource {
	private final Logger log = LoggerFactory.getLogger(SmartstoreSettingsResource.class);

    private static final String ENTITY_NAME = "smartstoresettings";
    
    @Inject
	private SmartstoreSettingsService smartstoreSettingsService;
   
	
	@PostMapping("/smartstoresettings")
    @Timed
    public ResponseEntity<SmartstoreSettings> createSmartstoreSetting(@RequestBody SmartstoreSettings request) throws URISyntaxException {
        log.debug("REST request to save SmartstoreSettings : {}", request);
        
        
        if (smartstoreSettingsService.findOneByKey(request.getKey().toLowerCase()).isPresent()) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("CompanyRequestManagement", "company", "company already in use"))
					.body(null);
		} 
        
        SmartstoreSettings smartstoresettings=new SmartstoreSettings();
        smartstoresettings.setId(request.getKey());
        smartstoresettings.setKey(request.getKey());
        smartstoresettings.setDescription(request.getDescription());
        smartstoresettings.setValue(request.getValue());
        SmartstoreSettings result  =  smartstoreSettingsService.save(smartstoresettings);
        
       
        return ResponseEntity.created(new URI("/api/smartstoresettings/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
	
	@PutMapping("/smartstoresettings")
    @Timed
    public ResponseEntity<SmartstoreSettings> updateSmartstoreSettings(@RequestBody SmartstoreSettings smartstoresettings) throws URISyntaxException {
	 log.debug("REST request to save smartstoresettings : {}", smartstoresettings);
	 if (smartstoresettings.getId() == null) {
         //  return createBinder(binder);
       }
	 SmartstoreSettings result  =  smartstoreSettingsService.save(smartstoresettings);
       return ResponseEntity.ok()
           .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
           .body(result);
    }
	
	@GetMapping("/smartstoresettings")
    @Timed
    public ResponseEntity<List<SmartstoreSettings>> getAllSmartstoreSettings(@ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of smartstoresettings");
        Page<SmartstoreSettings> page = smartstoreSettingsService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/smartstoresettings");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
	
	 @GetMapping("/smartstoresettings/{id}")
	 @Timed
	 public ResponseEntity<SmartstoreSettings> getSmartstoreSettings(@PathVariable String id) {
	        log.debug("REST request to get smartstoresettings : {}", id);
	        SmartstoreSettings smartstoresettings = smartstoreSettingsService.findOne(id);
	        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(smartstoresettings));
	 }
	 
	 @DeleteMapping("/smartstoresettings/{id}")
	 @Timed
	 public ResponseEntity<Void> deleteSmartstoreSettings(@PathVariable String id) {
	        log.debug("REST request to delete smartstoresettings : {}", id);
	        smartstoreSettingsService.delete(id);
	        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	 }
	
}
