package com.smartdocs.smartportal.web.rest.vm;

public class CertificateVM {
	
	private String certificate;

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}
	
	

}
