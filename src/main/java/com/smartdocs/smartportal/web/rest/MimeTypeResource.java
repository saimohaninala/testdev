package com.smartdocs.smartportal.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.smartdocs.smartportal.domain.Company;
import com.smartdocs.smartportal.domain.MimeType;
import com.smartdocs.smartportal.service.CompanyService;
import com.smartdocs.smartportal.service.MimeTypeService;
import com.smartdocs.smartportal.web.rest.util.HeaderUtil;
import com.smartdocs.smartportal.web.rest.util.PaginationUtil;
import com.smartdocs.smartportal.web.rest.util.ResponseUtil;

import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/api")
public class MimeTypeResource {

    private final Logger log = LoggerFactory.getLogger(MimeTypeResource.class);

    private static final String ENTITY_NAME = "mimetype";
    
    @Inject
	private MimeTypeService mimetypeService;
   
	
	@PostMapping("/mimetype")
    @Timed
    public ResponseEntity<MimeType> createMimeType(@RequestBody  MimeType mimetype) throws URISyntaxException {
        log.debug("REST request to save mimetype : {}", mimetype);
        
        
        if (mimetypeService.findOneBymimeType(mimetype.getMimeType().toLowerCase()).isPresent()) {
			return ResponseEntity.badRequest()
					.headers(HeaderUtil.createFailureAlert("MimeTypeRequestManagement", "mimetype", "mimetype already in use"))
					.body(null);
		} 
        
        MimeType mimeType=new MimeType();
        mimeType.setId(mimetype.getId());
        mimeType.setMimeType(mimetype.getMimeType());
        mimeType.setFileExtension(mimetype.getFileExtension());
        MimeType result  =  mimetypeService.save(mimeType);
        
       
        return ResponseEntity.created(new URI("/api/mimetype/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }
	
	@PutMapping("/mimetype")
    @Timed
    public ResponseEntity<MimeType> updateMimeType(@RequestBody MimeType mimeType) throws URISyntaxException {
	 log.debug("REST request to save mimetype : {}", mimeType);
	 if (mimeType.getId() == null) {
         //  return createBinder(binder);
       }
	 MimeType result  =  mimetypeService.save(mimeType);
       return ResponseEntity.ok()
           .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
           .body(result);
    }
	
	@GetMapping("/mimetype")
    @Timed
    public ResponseEntity<List<MimeType>> getAllMimeTypes(@ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get a page of company");
        Page<MimeType> page = mimetypeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/mimetype");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
	
	 @GetMapping("/mimetype/{mimeType}")
	 @Timed
	 public ResponseEntity<MimeType> getMimeType(@PathVariable String mimeType) {
	        log.debug("REST request to get mimetype : {}", mimeType);
	        MimeType mimetype = mimetypeService.findOneByMimeType(mimeType);
	        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(mimetype));
	 }
	 
	 @DeleteMapping("/mimetype/{id}")
	 @Timed
	 public ResponseEntity<Void> deleteMimeType(@PathVariable String id) {
	        log.debug("REST request to delete mimetype : {}", id);
	        mimetypeService.delete(id);
	        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	 }
	
	
}

