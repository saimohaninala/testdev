package com.smartdocs.smartportal.web.rest.vm;

public class UserDocumentCount {
private String createdBy;
private int total;




public String getCreatedBy() {
	return createdBy;
}
public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
}
public int getTotal() {
	return total;
}
public void setTotal(int total) {
	this.total = total;
}


}
