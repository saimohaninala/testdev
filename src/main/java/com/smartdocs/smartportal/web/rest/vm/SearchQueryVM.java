package com.smartdocs.smartportal.web.rest.vm;

import java.util.List;
import java.util.Map;

public class SearchQueryVM {
	private String siteId;
	private String q;
	private  Map<String,List<String>> fq;
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public String getQ() {
		return q;
	}
	public void setQ(String q) {
		this.q = q;
	}
	public Map<String, List<String>> getFq() {
		return fq;
	}
	public void setFq(Map<String, List<String>> fq) {
		this.fq = fq;
	}
	
	

}
