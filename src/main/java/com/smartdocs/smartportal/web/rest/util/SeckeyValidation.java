package com.smartdocs.smartportal.web.rest.util;

import java.net.URLEncoder;
import java.security.PublicKey;
import java.security.cert.X509Certificate;
import java.security.interfaces.DSAPublicKey;
import java.util.Collection;
import java.util.Iterator;
import org.apache.log4j.Logger;

import org.bouncycastle.cms.CMSProcessable;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.SignerInformation;
import org.bouncycastle.cms.SignerInformationStore;
import org.bouncycastle.jce.provider.JDKKeyFactory;

import com.smartdocs.smartportal.service.util.Base64;

public class SeckeyValidation {
	public static Logger log = Logger.getLogger("" + SeckeyValidation.class);
	public static boolean validateSecKey( String seckey,String appendedText, X509Certificate cert){
		boolean validated=false;
		
		//String append="C1000C296DC6031ED89DAB2BE2F3CD4BF6rudcCN=AA7,OU=I0090160380,OU=SAPWebAS,O=SAPTrustCommunity,C=DE20180621154557";
		
		  try {
			 
			Base64 bs64=new Base64();
			String  appendedTextnew=appendedText.replaceAll("=", "%3D");
			System.out.println("appendedTextnew:"+appendedTextnew);
			CMSProcessable processable = new CMSProcessableByteArray(appendedTextnew.getBytes());
			CMSSignedData s = new CMSSignedData(processable, bs64.decode(seckey));
			// get 1st signer infos
				
				
				           
				SignerInformationStore signers = s.getSignerInfos();
				Collection c = signers.getSigners();
				Iterator it = c.iterator();
				SignerInformation signer = (SignerInformation) it.next();
				PublicKey originalPublicKey=cert.getPublicKey();
				
				System.out.println("public key***************************...."+originalPublicKey);
				
				PublicKey newPublicKey =null;
				//log.info("public key***************************...."+originalPublicKey);
				if(originalPublicKey instanceof DSAPublicKey)
					newPublicKey=originalPublicKey;
				else 
					newPublicKey =	JDKKeyFactory.createPublicKeyFromDERStream( originalPublicKey.getEncoded() );					
				//log.info("new public key***************************...."+newPublicKey);
				// verification
				
			validated = signer.verify( newPublicKey, "SUN");
				log.info("Validate ***********"+validated);

				//System.out.println("Ok = " + test2);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//validated=false;
			e.printStackTrace();
		}

		return validated;
	}



}
