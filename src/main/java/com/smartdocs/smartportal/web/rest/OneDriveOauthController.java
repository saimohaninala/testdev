package com.smartdocs.smartportal.web.rest;

import java.io.IOException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.ContentType;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.MultipartContent;
import com.google.api.client.util.Clock;
import com.smartdocs.smartportal.config.JHipsterProperties;
import com.smartdocs.smartportal.domain.GdriveToken;
import com.smartdocs.smartportal.domain.OAuthConstants;
import com.smartdocs.smartportal.domain.OAuthUserConfig;
import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.repository.GdriveTokenRepository;
import com.smartdocs.smartportal.repository.RepositoryRepository;
import com.smartdocs.smartportal.service.OneDriveClientApi;
import com.smartdocs.smartportal.service.OneDriveOauthTokenService;
import com.smartdocs.smartportal.service.RepositoryService;
import com.smartdocs.smartportal.service.impl.OneDriveClientApiImpl;
import com.smartdocs.smartportal.smartstore.OneDriveOauthUtils;
import com.smartdocs.smartportal.web.rest.util.HttpClientUtil;

@RestController
@RequestMapping("/api/onedrive")
public class OneDriveOauthController {
	
    @Inject
    private  GdriveTokenRepository tokenInfoRepository;
   @Inject
   private OneDriveOauthTokenService tokenService;
   @Inject
   OneDriveClientApi oncedriveClientApi;
   @Inject
   RepositoryRepository repositoryRepository;
   @Inject
    JHipsterProperties jHipsterProperties;

	@GetMapping("/auth")
	public void authenticate(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
			throws IOException {
		OneDriveOauthUtils oauthutil=new OneDriveOauthUtils();
		String url=oauthutil.authorize(httpServletRequest);
		httpServletResponse.sendRedirect(url);

	}

	@GetMapping("/redirect")
	public String redirect(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse)
			throws IOException, JSONException {
		String status="Error";
   	 String redirectUri= jHipsterProperties.getOnedrive().getRedirectUri();

	   String code=httpServletRequest.getParameter("code");
	   String state=httpServletRequest.getParameter("state");
		OneDriveOauthUtils oauthutil=new OneDriveOauthUtils();
		Repository repository=repositoryRepository.findOne(state);
		JSONObject jsonObject = oauthutil.getAccessToken(repository,code,httpServletResponse,redirectUri);
		if (jsonObject == null)
			return status;

		/*
		 * HttpGet httpGet = new HttpGet(OAuthConstants.GOOGLE_API_INFO);
		 * httpGet.setHeader("Authorization", "Bearer " + accessToken);
		 * HttpResponse googleApiInfo = HttpClientUtil.executeGet(httpGet);
		 * IOUtils.copy(googleApiInfo.getEntity().getContent(),
		 * resp.getOutputStream());
		 * 
		 * 
		 */
		Clock clock = Clock.SYSTEM;
		String accessToken = (String) jsonObject.get("access_token");
		String refreshToken = (String) jsonObject.get("refresh_token");
		Long expiresIn = Long.parseLong((String) jsonObject.get("expires_in"));
		expiresIn = clock.currentTimeMillis() + expiresIn * 1000;
		
	
		
		GdriveToken tokenInfo=new GdriveToken();
		tokenInfo.setId(state);
		tokenInfo.setAccessToken(accessToken);
		tokenInfo.setExpirationTimeMilliseconds(expiresIn);
		tokenInfo.setRefreshToken(refreshToken);
		if(tokenInfo.getRefreshToken()!=null){
		tokenInfo=tokenInfoRepository.save(tokenInfo);
		status="Successfully Authenticated, Please Close The Brower";
		}else{
			repositoryRepository.delete(state);
			status="Internal Server Error";
		}


		return status;

	}

	
	
/*	@GetMapping("/getActiveAccessToken")
	public String getActiveAccessToken(@RequestParam(value = "id")  String id)
			throws IOException {
		
		System.out.println(tokenService.getActiveAccessToken(id));
	
			return tokenService.getActiveAccessToken(id);
	}
	
	
	//https://graph.microsoft.com/me/drive
	
	@GetMapping("/graph")
	public String getGraph(@RequestParam(value = "id")  String id)
			throws IOException {
		String token=tokenService.getActiveAccessToken(id);
		  OneDriveClientApi oncedriveClientApi=new OneDriveClientApiImpl(token);
		com.google.api.client.http.HttpResponse response=oncedriveClientApi.get("https://graph.microsoft.com/v1.0/me/drive");
		
		
	
	
			return response.parseAsString();
	}
	@GetMapping("/graph/upload/")
	public String uploadFile(@RequestParam(value = "id")  String id)
			throws IOException {
		String token=tokenService.getActiveAccessToken(id);
		  OneDriveClientApi oncedriveClientApi=new OneDriveClientApiImpl(token);
		  
		  
			ByteArrayContent byteArrayContent=new ByteArrayContent("text/plain", "sample text for download".getBytes());
			MultipartContent.Part part = new MultipartContent.Part(byteArrayContent);
			
	
			com.google.api.client.http.HttpResponse response=oncedriveClientApi.put("https://graph.microsoft.com/v1.0/me/drive/root:/FolderAAA/FileBb.txt:/content", byteArrayContent);		
			
			
			
			
			return response.parseAsString();

	
	}
	*/
	
	public static void main(String a[]){
	}

}
