package com.smartdocs.smartportal.web.rest.util;

public interface SSConstants {
 String SS_SMARTSTORE_NAME="smartstore";
 String SS_CERTIFICATE_NAME="certificate";
 static String rootpath="C://ascs/abc";
 String SS_FILE_STORAGE_SYSTEM="filesystem";
 String SS_GOCS_STORAGE_SYSTEM="gdocs";
 String SS_AZURE_BLOBSTORAGE="azurestorage";
 String SS_GDRIVE_STORAGE_SYSTEM="GoogleDrive";
 String SS_MOSS_STORAGE_SYSTEM="moss";
 String CREATED="created";
 String NOT_CREATED="notcreated";
 
 String xErrorDescription="x-errordescription";
 
 String pVersion="pVersion";
 String contRep="contRep";
 String docId="docId";
 String compId="compId";
 String resultAs="resultAs";
 String accessMode="accessMode";
 String authId="authId";
 String expiration="expiration";
 String secKey="secKey";
 
 String content_Type="Content-Type";
 String boundary="boundary";
 String content_Length="Content-Length";
 String X_dateC="X-dateC";
 String X_timeC="X-timeC";
 String X_dateM="X-dateM";
 String X_timeM="X-timeM";
 String X_numberComps="X-numberComps";
 String X_contentRep="X-contentRep";
 String X_docId="X-docId";
 String X_docStatus="X-docStatus";
 String X_pVersion="X-pVersion";
 String fromOffset="fromOffset";
 String toOffset="toOffset";
 String charset="charset";
 String pattern="pattern";
 String caseSensitive="caseSensitive";
 String numResults="numResults";
 String docProt="docProt";
 String version="version";
 
 String X_Content_Length="X-Content-Length";
 String X_compId="X-compId";
 String X_compDateC="X-compDateC";
 String X_compTimeC="X-compTimeC";
 String X_compDateM="X-compDateM";
 String X_compTimeM="X-compTimeM";
 String X_compStatus="X-compStatus";
 
 String companyInfo="companyInfo";
 
 String CertficateAccept="Accept";
 String CertficateReject="Reject";
 
}
