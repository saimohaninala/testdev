package com.smartdocs.smartportal.web.rest;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.google.api.client.auth.oauth2.StoredCredential;
import com.google.api.client.util.store.AbstractDataStore;
import com.google.api.client.util.store.AbstractDataStoreFactory;
import com.google.api.client.util.store.DataStore;
import com.smartdocs.smartportal.domain.GdriveToken;
import com.smartdocs.smartportal.repository.GdriveTokenRepository;


@Component
public class MongoRepoDataStoreFactory extends AbstractDataStoreFactory {
	  private final GdriveTokenRepository gdrivetokenRepository;

	public MongoRepoDataStoreFactory(GdriveTokenRepository gdrivetokenRepository){
		this.gdrivetokenRepository=gdrivetokenRepository;
	}

	@Override
	protected <V extends Serializable> DataStore<V> createDataStore(String id) throws IOException {
		// TODO Auto-generated method stub
		return new MongoDataStore(this, gdrivetokenRepository, id) ;
	}
static class MongoDataStore<V extends Serializable> extends AbstractDataStore<V> {
	private final GdriveTokenRepository gdivetokenRepository;
	MongoDataStore(MongoRepoDataStoreFactory mongoRepoDataStoreFactory,GdriveTokenRepository gdivetokenRepository,String id){
	      super(mongoRepoDataStoreFactory, id);
	      this.gdivetokenRepository=gdivetokenRepository;

	}
	@Override
	public Set<String> keySet() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Collection<V> values() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public V get(String key) throws IOException {
		// TODO Auto-generated method stub
		StoredCredential storedCredential = new StoredCredential();
		GdriveToken tokenInfo=gdivetokenRepository.findOne(key);
		if(tokenInfo!=null){
			storedCredential.setAccessToken(tokenInfo.getAccessToken());
			storedCredential.setExpirationTimeMilliseconds(tokenInfo.getExpirationTimeMilliseconds());
			storedCredential.setRefreshToken(tokenInfo.getRefreshToken());
			return (V)storedCredential;

		}
		

		return null;
	}
	@Override
	public DataStore<V> set(String key, V value) throws IOException {
		// TODO Auto-generated method stub
		StoredCredential storedCredential = (StoredCredential)value;
		GdriveToken tokenInfo=new GdriveToken();
		System.out.println("key:"+key);
		tokenInfo.setId(key);
		tokenInfo.setAccessToken(storedCredential.getAccessToken());
		tokenInfo.setExpirationTimeMilliseconds(storedCredential.getExpirationTimeMilliseconds());
		tokenInfo.setRefreshToken(storedCredential.getRefreshToken());
		gdivetokenRepository.save(tokenInfo);
		

		return this;
	}
	@Override
	public DataStore<V> clear() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public DataStore<V> delete(String key) throws IOException {
		// TODO Auto-generated method stub
		gdivetokenRepository.delete(key);
		return this;
	}
	
}

}
