package com.smartdocs.smartportal.domain;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.data.annotation.Id;
@org.springframework.data.mongodb.core.mapping.Document(collection = "document")
public class Document implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	private String id;
	private String docId;
    private String archiveId;
	private String archiveDocId;
    private String status;
    private ZonedDateTime createdDateTime=ZonedDateTime.now();;
    private ZonedDateTime lastUpdatedDateTime=ZonedDateTime.now();
	private String fileName;
	private String fileType;
    private String location;
    private String archiveDocIdResourceId;
	private int  contentLength;
	private String contentType;
	private String systemId;
    List<String> components;
    private String resourceId;

    public Document() {
		super();
	}
    
    public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDocId() {
		return docId;
	}
	public void setDocId(String docId) {
		this.docId = docId;
	}
	public String getArchiveId() {
		return archiveId;
	}
	public void setArchiveId(String archiveId) {
		this.archiveId = archiveId;
	}
	public String getArchiveDocId() {
		return archiveDocId;
	}
	public void setArchiveDocId(String archiveDocId) {
		this.archiveDocId = archiveDocId;
	}
	
	public String getStatus() {
		return status;
	}
	public ZonedDateTime getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(ZonedDateTime createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	public ZonedDateTime getLastUpdatedDateTime() {
		return lastUpdatedDateTime;
	}
	public void setLastUpdatedDateTime(ZonedDateTime lastUpdatedDateTime) {
		this.lastUpdatedDateTime = lastUpdatedDateTime;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getSystemId() {
			return systemId;
	}
		public void setSystemId(String systemId) {
			this.systemId = systemId;
		}
		public List<String> getComponents() {
			return components;
		}
		public void setComponents(List<String> components) {
			this.components = components;
		}
	
		public String getContentType() {
			return contentType;
		}
		public void setContentType(String contentType) {
			this.contentType = contentType;
		}
	
		public int getContentLength() {
			return contentLength;
		}
		public void setContentLength(int contentLength) {
			this.contentLength = contentLength;
		}
		
		 public String getResourceId() {
				return resourceId;
			}
			public void setResourceId(String resourceId) {
				this.resourceId = resourceId;
			}
			
			   public String getArchiveDocIdResourceId() {
					return archiveDocIdResourceId;
				}
				public void setArchiveDocIdResourceId(String archiveDocIdResourceId) {
					this.archiveDocIdResourceId = archiveDocIdResourceId;
				}
				
				public Document(String archiveId, String archiveDocId, String fileName, String fileType) {
					super();
					this.archiveId = archiveId;
					this.archiveDocId = archiveDocId;
					this.fileName = fileName;
					this.fileType = fileType;
				}
				
}
