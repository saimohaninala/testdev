package com.smartdocs.smartportal.domain;

public class OAuthConstants {

    public static final String ACCESS_TOKEN = "access_token";
    public static final String CLIENT_ID = "client_id";
    public static final String CLIENT_SECRET = "client_secret";
    public static final String CODE = "code";
    public static final String REDIRECT_URI = "redirect_uri";
    public static final String GRANT_TYPE = "grant_type";
    public static final String REFRESH_GRANT_TYPE="refresh_token";
    public static final String RESPONSE_TYPE = "response_type";
    public static final String STATE = "state";
    public static final String SCOPE = "scope";
    public static final String RESOURCE = "resource";
    public static final String REFRESH_TOKEN="refresh_token";
    public static String REDIRECT_URI_VALUE = "http://localhost:7777/api/google/redirect";


    public static final String GOOGLE_AUTHORIZATION_URL = "https://accounts.google.com/o/oauth2/auth";
    public static final String GOOGLE_ACCESS_TOKEN_URL = "https://www.googleapis.com/oauth2/v3/token";
    public static String SCOPE_VALUE = "https://www.googleapis.com/auth/plus.login%20https://www.googleapis.com/auth/calendar%20https://www.googleapis.com/auth/userinfo.email";
   // public static String SCOPE_VALUE = "https://www.googleapis.com/auth/drive";
    public static final String GOOGLE_API_INFO = "https://www.googleapis.com/plus/v1/people/me";
    
    
    public static final String ONEDRIVE_AUTHORIZATION_URL = "https://login.microsoftonline.com/common/oauth2/authorize";
    public static final String ONEDRIVE_ACCESS_TOKEN_URL = "https://login.microsoftonline.com/common/oauth2/token";
    public static final String ONEDRIVE_RESOURCE_URL = "https://graph.microsoft.com/";


  

    

}
