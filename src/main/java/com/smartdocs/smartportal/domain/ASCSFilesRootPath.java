package com.smartdocs.smartportal.domain;

import java.io.Serializable;

public class ASCSFilesRootPath implements Serializable {
	public String company;
	public String systemId;
	public String repositoryID;
	public String sequenceNumber;	 
	public String filesRootPath;
	
	public ASCSFilesRootPath(){}
	public ASCSFilesRootPath(String company,String systemId, String repositoryID,
			String sequenceNumber, String filesRootPath
			) {
		super();
		this.company = company;
		this.systemId=systemId;
		this.repositoryID = repositoryID;
		this.sequenceNumber = sequenceNumber;
		this.filesRootPath = filesRootPath;
		
	}
	
	public String getSystemId() {
		return systemId;
	}
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getRepositoryID() {
		return repositoryID;
	}
	public void setRepositoryID(String repositoryID) {
		this.repositoryID = repositoryID;
	}
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	public String getFilesRootPath() {
		return filesRootPath;
	}
	public void setFilesRootPath(String filesRootPath) {
		this.filesRootPath = filesRootPath;
	}

	
}
