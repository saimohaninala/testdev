package com.smartdocs.smartportal.domain;

public class BrandSetting {

	private String id;
	private String loginlogo;
	private String loginbackgroundcolor;
	private String loginbackgroundimage;
	private String applogo;
	private String appbackgroundimage;
	private String applabel;
	
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLoginlogo() {
		return loginlogo;
	}
	public void setLoginlogo(String loginlogo) {
		this.loginlogo = loginlogo;
	}
	public String getLoginbackgroundcolor() {
		return loginbackgroundcolor;
	}
	public void setLoginbackgroundcolor(String loginbackgroundcolor) {
		this.loginbackgroundcolor = loginbackgroundcolor;
	}
	public String getLoginbackgroundimage() {
		return loginbackgroundimage;
	}
	public void setLoginbackgroundimage(String loginbackgroundimage) {
		this.loginbackgroundimage = loginbackgroundimage;
	}
	public String getApplogo() {
		return applogo;
	}
	public void setApplogo(String applogo) {
		this.applogo = applogo;
	}
	public String getAppbackroundimage() {
		return appbackgroundimage;
	}
	public void setAppbackroundimage(String appbackgroundimage) {
		this.appbackgroundimage = appbackgroundimage;
	}
	public String getApplabel() {
		return applabel;
	}
	public void setApplabel(String applabel) {
		this.applabel = applabel;
	}
	
	@Override
	public String toString() {
		return "BrandSetting [id=" + id + ", loginlogo=" + loginlogo
				+ ", loginbackgroundcolor=" + loginbackgroundcolor
				+ ", loginbackgroundimage=" + loginbackgroundimage
				+ ", applogo=" + applogo + ", appbackgroundimage="
				+ appbackgroundimage + ", applabel=" + applabel + "]";
	}
   
	
	
}
