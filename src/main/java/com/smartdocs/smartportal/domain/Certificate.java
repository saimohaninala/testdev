/**
 ** @Author Gattu ChandraShaker
 ** @company ActiProcess
 ** @Project SmartStore
 */
package com.smartdocs.smartportal.domain;


public class Certificate {
	
	
	
	private String certid;
	
	
	
	
	
	private String systemId;

	private String validFrom;

	private String validTo;
	
	private String publicKey;

	private String algorithm;

	
	private String status;
	
	private String syncStatus;
	
	private String certificate;
	
	private Long syncDate;

	public Certificate(String certid, String companyId, String systemId,
			String validFrom, String validTo, String publicKey, String algorithm,String status,String certificate) {
		super();
		
		this.certid = certid;
		
		this.systemId = systemId;
		this.validFrom = validFrom;
		this.validTo = validTo;
	//	this.publicKey = new Text(publicKey);
		this.algorithm = algorithm;
		this.status=status;
		this.certificate=certificate;
	}
	
	public Certificate() {
		// TODO Auto-generated constructor stub
	}

	public String getCertificate() {
		return certificate;
	}

	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}

	public String getPublicKey() {
		return publicKey;
	}

	public void setPublicKey(String publicKey) {
		this.publicKey = publicKey;
	}

	public String getSyncStatus() {
		return syncStatus;
	}

	public void setSyncStatus(String syncStatus) {
		this.syncStatus = syncStatus;
	}

	public Long getSyncDate() {
		return syncDate;
	}

	public void setSyncDate(Long syncDate) {
		this.syncDate = syncDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCertid() {
		return certid;
	}
	public void setCertid(String certid) {
		this.certid = certid;
	}

	public String getSystemId() {
		return systemId;
	}
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}
	public String getValidFrom() {
		return validFrom;
	}
	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}
	public String getValidTo() {
		return validTo;
	}
	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}
/*	public Text getPublicKey() {
		return publicKey;
	}
	public void setPublicKey(String publicKey) {
		this.publicKey = new Text(publicKey);
	}*/
	public String getAlgorithm() {
		return algorithm;
	}
	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	

}
