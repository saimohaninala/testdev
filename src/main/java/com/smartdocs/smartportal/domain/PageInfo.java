package com.smartdocs.smartportal.domain;

public class PageInfo {

	private Integer page;
	private Integer size;
	
	
	public PageInfo() {
		super();
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}


	@Override
	public String toString() {
		return "PageInfo [page=" + page + ", size=" + size + "]";
	}
	
}
