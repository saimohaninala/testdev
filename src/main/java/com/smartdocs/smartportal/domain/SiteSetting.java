package com.smartdocs.smartportal.domain;

public class SiteSetting {
	
	private String sitedisplayname;
	private Integer maxnosite;
	private Boolean isWebDav;
	
	public String getSitedisplayname() {
		return sitedisplayname;
	}
	public void setSitedisplayname(String sitedisplayname) {
		this.sitedisplayname = sitedisplayname;
	}
	public Boolean getIsWebDav() {
		if(isWebDav!=null){
			return isWebDav;
		}else{
			return false;
		}
	}
	public void setIsWebDav(Boolean isWebDav) {
		this.isWebDav = isWebDav;
	}
	public Integer getMaxnosite() {
		return maxnosite;
	}
	public void setMaxnosite(Integer maxnosite) {
		this.maxnosite = maxnosite;
	}
	
	
	
}
