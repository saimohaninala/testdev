package com.smartdocs.smartportal.domain;




public class FileSystemRootPath  {
	

	private String companyId;
	
	 private String repositoryID;
	
	 private String systemId;
	
	 private String sequenceNumber;
	
	
	 private String filesRootPath;
	
	
	private String status;
	
	public FileSystemRootPath(String companyId,String systemId, String repositoryID,
			String sequenceNumber, String filesRootPath
			)
	{
		super();
		this.systemId=systemId;
		this.companyId = companyId;
		this.repositoryID = repositoryID;
		this.sequenceNumber = sequenceNumber;
		this.filesRootPath=filesRootPath;
		
		this.status="active";
	}
	
	public String getFilesRootPath() {
		return filesRootPath;
	}

	public void setFilesRootPath(String filesRootPath) {
		this.filesRootPath = filesRootPath;
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCompany() {
		return companyId;
	}
	public void setCompany(String companyId) {
		this.companyId = companyId;
	}
	public String getRepositoryID() {
		return repositoryID;
	}
	public void setRepositoryID(String repositoryID) {
		this.repositoryID = repositoryID;
	}
	public String getSequenceNumber() {
		return sequenceNumber;
	}
	public void setSequenceNumber(String sequenceNumber) {
		this.sequenceNumber = sequenceNumber;
	}
	
	
	
}
