package com.smartdocs.smartportal.domain;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "companys")
public class Company implements Serializable {
	
	 private static final long serialVersionUID = 1L;
	 
	 
		@Id
	    private String id;
		@Field("companyId")
		private String companyId;
	 	@Field("name")
	    private String name;
	 	@Field("description")
	    private String description;
	    @Field("address")
	    private String address;
	    @Field("contact")
	    private String contact;
	   
	    
	    
		
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getCompanyId() {
			return companyId;
		}
		public void setCompanyId(String companyId) {
			this.companyId = companyId;
		}
		public String getContact() {
			return contact;
		}
		public void setContact(String contact) {
			this.contact = contact;
		}
		
}
