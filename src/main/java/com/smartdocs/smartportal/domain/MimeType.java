package com.smartdocs.smartportal.domain;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "mimetype")

public class MimeType implements Serializable {
	 private static final long serialVersionUID = 1L;
	 
	 
		@Id
	    private String id;
		@Field("mimetype")
		private String mimeType;
		@Field("fileExtension")
	    private String fileExtension;
		
		
		@Field("companyId")
	    private String companyId;
		
		public String getCompanyId() {
			return companyId;
		}
		public void setCompanyId(String companyId) {
			this.companyId = companyId;
		}
		
	 	public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getMimeType() {
			return mimeType;
		}
		public void setMimeType(String mimeType) {
			this.mimeType = mimeType;
		}
		public String getFileExtension() {
			return fileExtension;
		}
		public void setFileExtension(String fileExtension) {
			this.fileExtension = fileExtension;
		}
		
}
