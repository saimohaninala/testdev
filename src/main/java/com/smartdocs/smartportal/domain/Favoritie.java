package com.smartdocs.smartportal.domain;

public class Favoritie {
	private String link_name;
	private String link_url;
	private String link_description;
	private String link_tag;
	
	public Favoritie() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Favoritie(String link_name, String link_url, String link_description, String link_tag) {
		super();
		this.link_name = link_name;
		this.link_url = link_url;
		this.link_description = link_description;
		this.link_tag = link_tag;
	}


	public String getLink_name() {
		return link_name;
	}

	public void setLink_name(String link_name) {
		this.link_name = link_name;
	}

	public String getLink_url() {
		return link_url;
	}

	public void setLink_url(String link_url) {
		this.link_url = link_url;
	}

	public String getLink_tag() {
		return link_tag;
	}

	public void setLink_tag(String link_tag) {
		this.link_tag = link_tag;
	}


	public String getLink_description() {
		return link_description;
	}


	public void setLink_description(String link_description) {
		this.link_description = link_description;
	}

}
