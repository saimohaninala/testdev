package com.smartdocs.smartportal.domain;

import java.util.HashMap;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;



public class SystemSettings {

	
    @Id
    private String id;

    @Field("system")
    private String system;

    @Field("menu")
    private List<String> menu;
    
    @Field("active_menu")
    private HashMap<String,Boolean> activeMenu;
	
    @Field("smartstore")
    private SmartStore smartstore;
    
    @Field("solr")
    private Solr  solr;
	

	@Field("site")
    private SiteSetting  site;

	@Field("password_policy")
    private String passwordPolicy;

    public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSystem() {
		return system;
	}

    public SystemSettings system(String system) {
        this.system = system;
        return this;
    }

    public void setSystem(String system) {
		this.system = system;
	}

	public List<String> getMenu() {
		return menu;
	}

    public SystemSettings menu(List<String> menu) {
        this.menu = menu;
        return this;
    }

	public void setMenu(List<String> menu) {
		this.menu = menu;
	}

	public HashMap<String, Boolean> getActiveMenu() {
		return activeMenu;
	}

    public SystemSettings activeMenu(HashMap<String, Boolean> activeMenu) {
        this.activeMenu = activeMenu;
        return this;
    }

	public void setActiveMenu(HashMap<String, Boolean> activeMenu) {
		this.activeMenu = activeMenu;
	}


	public SmartStore getSmartstore() {
		return smartstore;
	}

	public void setSmartstore(SmartStore smartstore) {
		this.smartstore = smartstore;
	}

	public Solr getSolr() {
		return solr;
	}

	public void setSolr(Solr solr) {
		this.solr = solr;
	}

	public String getPasswordPolicy() {
		return passwordPolicy;
	}

	public void setPasswordPolicy(String passwordPolicy) {
		this.passwordPolicy = passwordPolicy;
	}

	public SiteSetting getSite() {
		return site;
	}

	public void setSite(SiteSetting site) {
		this.site = site;
	}
}