package com.smartdocs.smartportal.domain;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Document(collection = "repository")
public class Repository implements Serializable{
	private static final long serialVersionUID = 1L;
	 
	 
	@Id
    private String id;
	@Field("repositoryId")
	private String repositoryId;
	@Field("repositoryName")
    private String name;
	@Field("description")
	private String description;
	@Field("pVersion")
    private String pVersion;
	@Field("storageType")
    private String storageType;
	@Field("clientId")
	private String clientId;
	@Field("clientSecret")
    private String clientSecret;
	@Field("rootPath")
    private String rootPath;
	@Field("url_expiration")
    private String url_expiration;
	@Field("systemId")
    private String systemId;
	
	public String getUrl_expiration() {
		return url_expiration;
	}
	public void setUrl_expiration(String url_expiration) {
		this.url_expiration = url_expiration;
	}
	public String getRootPath() {
		return rootPath;
	}
	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}
	public String getClientId() {
		return clientId;
	}
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	public String getClientSecret() {
		return clientSecret;
	}
	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRepositoryId() {
		return repositoryId;
	}
	public void setRepositoryId(String repositoryId) {
		this.repositoryId = repositoryId;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getpVersion() {
		return pVersion;
	}
	public void setpVersion(String pVersion) {
		this.pVersion = pVersion;
	}
	public String getStorageType() {
		return storageType;
	}
	public void setStorageType(String storageType) {
		this.storageType = storageType;
	}
	public String getSystemId() {
		return systemId;
	}
	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}
	
}
