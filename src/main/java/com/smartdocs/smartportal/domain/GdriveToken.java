package com.smartdocs.smartportal.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "gdrivetoken")

public class GdriveToken{
		@Id
		private String id;
		private String accessToken;

	  private Long expirationTimeMilliseconds;

	  private String refreshToken;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Long getExpirationTimeMilliseconds() {
		return expirationTimeMilliseconds;
	}

	public void setExpirationTimeMilliseconds(Long expirationTimeMilliseconds) {
		this.expirationTimeMilliseconds = expirationTimeMilliseconds;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}


}
