package com.smartdocs.smartportal.repository;

import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;


import com.smartdocs.smartportal.domain.MimeType;


public interface MimeRepository extends MongoRepository<MimeType,String>{
	 MimeType findOneByMimeType(String mimeType);
	 Optional<MimeType> findOneBymimeType(String mimeType);
	 List<MimeType> findMimeTypesByCompanyId(String companyId);
		MimeType findMimeTypeByCompanyId(String companyId);
		MimeType findOneByfileExtension(String fileExtension);
}
