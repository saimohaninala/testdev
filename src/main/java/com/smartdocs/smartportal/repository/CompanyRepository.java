package com.smartdocs.smartportal.repository;



import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;


import com.smartdocs.smartportal.domain.Company;


@SuppressWarnings("unused")
public interface CompanyRepository extends MongoRepository<Company,String> {
	 Optional<Company> findOneByName(String name);
	 Company findOneByCompanyId(String companyId);
}
