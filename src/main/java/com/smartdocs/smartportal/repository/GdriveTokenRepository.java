package com.smartdocs.smartportal.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartdocs.smartportal.domain.GdriveToken;



public interface GdriveTokenRepository extends MongoRepository<GdriveToken, String> {

}
