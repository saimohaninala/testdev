package com.smartdocs.smartportal.repository;

import com.smartdocs.smartportal.domain.User;

import java.time.ZonedDateTime;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the User entity.
 */
public interface UserRepository extends MongoRepository<User, String>  {

    Optional<User> findOneByActivationKey(String activationKey);

    List<User> findAllByActivatedIsFalseAndCreatedDateBefore(ZonedDateTime dateTime);

    Optional<User> findOneByResetKey(String resetKey);

    Optional<User> findOneByEmail(String email);

    Optional<User> findOneByLogin(String login);
    
    User findOneObjectByLogin(String login);
    
    List<User> findByLoginStartingWith(String regexp);
    List<User> findByFirstNameStartingWith(String regexp);
    List<User> findByLastNameStartingWith(String regexp);
    List<User> findByEmailStartingWith(String regexp);
    
    List<User> findByLoginLikeOrderByIdAsc(String regexp);
    List<User> findByFirstNameLikeOrderByIdAsc(String regexp);
    List<User> findByLastNameLikeOrderByIdAsc(String regexp);
    List<User> findByEmailLikeOrderByIdAsc(String regexp);
    List<User> findByLogin(String login);
    
    
    
    
    
    
}
