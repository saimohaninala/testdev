package com.smartdocs.smartportal.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartdocs.smartportal.domain.SystemSettings;

public interface SystemSettingsRepository extends MongoRepository<SystemSettings,String>  {

	
	Optional<SystemSettings> findOneBySystem(String system);

}