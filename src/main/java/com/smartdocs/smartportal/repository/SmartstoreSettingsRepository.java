package com.smartdocs.smartportal.repository;

import java.util.Optional;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.smartdocs.smartportal.domain.SmartstoreSettings;

public interface SmartstoreSettingsRepository extends MongoRepository<SmartstoreSettings, String> {
	 Optional<SmartstoreSettings> findOneByKey(String key);
}
