package com.smartdocs.smartportal.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartdocs.smartportal.domain.AzureBlobSettings;

public interface AzureBlobSettingsRepository extends MongoRepository<AzureBlobSettings,String>{

}
