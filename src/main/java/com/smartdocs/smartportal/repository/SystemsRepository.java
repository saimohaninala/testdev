package com.smartdocs.smartportal.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartdocs.smartportal.domain.Systems;



public interface SystemsRepository extends MongoRepository<Systems,String> {
	 Optional<Systems> findOneBySystemName(String systemName);
	 Systems  findOneByAuthId(String authId);
	 Systems findOneBySystemId(String systemId);
}

