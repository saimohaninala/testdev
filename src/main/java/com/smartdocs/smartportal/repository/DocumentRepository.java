package com.smartdocs.smartportal.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;


import com.smartdocs.smartportal.domain.Document;

public interface DocumentRepository extends MongoRepository<Document, String>{
	  Document findOneByArchiveDocId(String archiveDocId);
	  Document findOneByArchiveDocIdAndFileName(String archiveDocId, String fileName);
	 List<Document> findOneByArchiveDocIdAndArchiveId(String archiveDocId,String archiveId);
	 List<Document> findByArchiveDocIdAndFileName(String archiveDocId, String fileName);
	 List<Document> findOneBySystemIdAndArchiveIdAndArchiveDocId(String systemId, String archiveId,
				String archiveDocId);
}
