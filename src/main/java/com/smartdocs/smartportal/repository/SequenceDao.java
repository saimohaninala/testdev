package com.smartdocs.smartportal.repository;

public interface SequenceDao {
    long getNextSequenceId(String key) throws Exception;
   
}