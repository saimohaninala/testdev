package com.smartdocs.smartportal.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.smartdocs.smartportal.domain.BrandSetting;


@Repository
public interface BrandSettingRepository extends MongoRepository<BrandSetting,String>{

	
	
}
