package com.smartdocs.smartportal.repository;

import org.springframework.data.mongodb.repository.MongoRepository;


import com.smartdocs.smartportal.domain.Certificate;

public interface CertificateRepository extends MongoRepository<Certificate,String> {
	Certificate findCertificateBySystemId(String systemId);
	void deleteCertificateBySystemId(String systemId);
	
}
