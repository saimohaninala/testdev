package com.smartdocs.smartportal.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartdocs.smartportal.domain.FileSystemRootPath;



public interface FileSystemRootPathRepository  extends MongoRepository<FileSystemRootPath, String>  {

}
