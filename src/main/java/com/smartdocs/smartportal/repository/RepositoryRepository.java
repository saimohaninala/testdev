package com.smartdocs.smartportal.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.smartdocs.smartportal.domain.Repository;




public interface RepositoryRepository extends MongoRepository<Repository,String>{
	Optional<Repository> findOneByName(String name);
	 Repository findOneByrepositoryId(String repositoryId);
	 List<Repository> findOneByRepositoryId(String repositoryId);
	 List<Repository> findOneBySystemId(String systemId);

}
