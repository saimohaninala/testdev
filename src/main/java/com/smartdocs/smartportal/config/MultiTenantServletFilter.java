package com.smartdocs.smartportal.config;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.web.filter.GenericFilterBean;

import com.google.common.net.InternetDomainName;
import com.smartdocs.smartportal.util.TenantContextHolder;

public class MultiTenantServletFilter extends GenericFilterBean {
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		TenantContextHolder.setTenantId(extractSubdomain(request));

		chain.doFilter(request, response);
	}

	String extractSubdomain(ServletRequest req) {
		return InternetDomainName.from(req.getServerName()).parts().get(0);
	}

}
