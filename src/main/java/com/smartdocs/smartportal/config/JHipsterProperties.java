package com.smartdocs.smartportal.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.cors.CorsConfiguration;

import com.smartdocs.smartportal.util.TenantContextHolder;

/**
 * Properties specific to JHipster.
 *
 * <p>
 *     Properties are configured in the application.yml file.
 * </p>
 */
@ConfigurationProperties(prefix = "jhipster", ignoreUnknownFields = false)
public class JHipsterProperties {
	private String location = "F:\\upload-dir";

    private final Async async = new Async();

    private final Http http = new Http();

    private final Cache cache = new Cache();

    private final Mail mail = new Mail();

    private final Security security = new Security();

    private final Swagger swagger = new Swagger();

    private final Metrics metrics = new Metrics();

    private final CorsConfiguration cors = new CorsConfiguration();

    private final Social social = new Social();

    private final Ribbon ribbon = new Ribbon();
    
    private final Smartstore smartstore = new Smartstore();
    
    private final Gdrive gdrive=new Gdrive();
    
    private final OneDrive onedrive=new OneDrive();
    
    private final Azure azure=new Azure();
    
    public Azure getAzure() {
		return azure;
	}

public Gdrive getGdrive() {
		return gdrive;
	}

	public Async getAsync() {
        return async;
    }

    public Http getHttp() {
        return http;
    }

    public Cache getCache() {
        return cache;
    }

    public Mail getMail() {
        return mail;
    }

    public Security getSecurity() {
        return security;
    }

    public Swagger getSwagger() {
        return swagger;
    }

    public Metrics getMetrics() {
        return metrics;
    }

    public CorsConfiguration getCors() {
        return cors;
    }

    public Social getSocial() {
        return social;
    }

    public Ribbon getRibbon() {
        return ribbon;
    }

    public static class Async {

        private int corePoolSize = 2;

        private int maxPoolSize = 50;

        private int queueCapacity = 10000;

        public int getCorePoolSize() {
            return corePoolSize;
        }

        public void setCorePoolSize(int corePoolSize) {
            this.corePoolSize = corePoolSize;
        }

        public int getMaxPoolSize() {
            return maxPoolSize;
        }

        public void setMaxPoolSize(int maxPoolSize) {
            this.maxPoolSize = maxPoolSize;
        }

        public int getQueueCapacity() {
            return queueCapacity;
        }

        public void setQueueCapacity(int queueCapacity) {
            this.queueCapacity = queueCapacity;
        }
    }

    public static class Http {

        private final Cache cache = new Cache();

        public Cache getCache() {
            return cache;
        }

        public static class Cache {

            private int timeToLiveInDays = 1461;

            public int getTimeToLiveInDays() {
                return timeToLiveInDays;
            }

            public void setTimeToLiveInDays(int timeToLiveInDays) {
                this.timeToLiveInDays = timeToLiveInDays;
            }
        }
    }

    public static class Cache {
    }

    public static class Mail {

        private String from = "smartportal@localhost";

        private String baseUrl = "";

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getBaseUrl() {
            return baseUrl;
        }

        public void setBaseUrl(String baseUrl) {
            this.baseUrl = baseUrl;
        }
    }

    public static class Security {

        private final Authentication authentication = new Authentication();

        public Authentication getAuthentication() {
            return authentication;
        }
        public static class Authentication {

            private final Jwt jwt = new Jwt();

            public Jwt getJwt() {
                return jwt;
            }

            public static class Jwt {

                private String secret;

                private long tokenValidityInSeconds = 1800;

                private long tokenValidityInSecondsForRememberMe = 2592000;

                public String getSecret() {
                    return secret;
                }

                public void setSecret(String secret) {
                    this.secret = secret;
                }

                public long getTokenValidityInSeconds() {
                    return tokenValidityInSeconds;
                }

                public void setTokenValidityInSeconds(long tokenValidityInSeconds) {
                    this.tokenValidityInSeconds = tokenValidityInSeconds;
                }

                public long getTokenValidityInSecondsForRememberMe() {
                    return tokenValidityInSecondsForRememberMe;
                }

                public void setTokenValidityInSecondsForRememberMe(long tokenValidityInSecondsForRememberMe) {
                    this.tokenValidityInSecondsForRememberMe = tokenValidityInSecondsForRememberMe;
                }
            }
        }
    }

    public static class Swagger {

        private String title = "smartportal API";

        private String description = "smartportal API documentation";

        private String version = "0.0.1";

        private String termsOfServiceUrl;

        private String contactName;

        private String contactUrl;

        private String contactEmail;

        private String license;

        private String licenseUrl;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getVersion() {
            return version;
        }

        public void setVersion(String version) {
            this.version = version;
        }

        public String getTermsOfServiceUrl() {
            return termsOfServiceUrl;
        }

        public void setTermsOfServiceUrl(String termsOfServiceUrl) {
            this.termsOfServiceUrl = termsOfServiceUrl;
        }

        public String getContactName() {
            return contactName;
        }

        public void setContactName(String contactName) {
            this.contactName = contactName;
        }

        public String getContactUrl() {
            return contactUrl;
        }

        public void setContactUrl(String contactUrl) {
            this.contactUrl = contactUrl;
        }

        public String getContactEmail() {
            return contactEmail;
        }

        public void setContactEmail(String contactEmail) {
            this.contactEmail = contactEmail;
        }

        public String getLicense() {
            return license;
        }

        public void setLicense(String license) {
            this.license = license;
        }

        public String getLicenseUrl() {
            return licenseUrl;
        }

        public void setLicenseUrl(String licenseUrl) {
            this.licenseUrl = licenseUrl;
        }
    }

    public static class Metrics {

        private final Jmx jmx = new Jmx();

        private final Graphite graphite = new Graphite();

        private final Prometheus prometheus = new Prometheus();

        private final Logs logs = new Logs();

        public Jmx getJmx() {
            return jmx;
        }

        public Graphite getGraphite() {
            return graphite;
        }

        public Prometheus getPrometheus() {
            return prometheus;
        }

        public Logs getLogs() {
            return logs;
        }

        public static class Jmx {

            private boolean enabled = true;

            public boolean isEnabled() {
                return enabled;
            }

            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }
        }

        public static class Graphite {

            private boolean enabled = false;

            private String host = "localhost";

            private int port = 2003;

            private String prefix = "smartportal";

            public boolean isEnabled() {
                return enabled;
            }

            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }

            public String getHost() {
                return host;
            }

            public void setHost(String host) {
                this.host = host;
            }

            public int getPort() {
                return port;
            }

            public void setPort(int port) {
                this.port = port;
            }

            public String getPrefix() {
                return prefix;
            }

            public void setPrefix(String prefix) {
                this.prefix = prefix;
            }
        }

        public static class Prometheus {

            private boolean enabled = false;

            private String endpoint = "/prometheusMetrics";

            public boolean isEnabled() {
                return enabled;
            }

            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }

            public String getEndpoint() {
                return endpoint;
            }

            public void setEndpoint(String endpoint) {
                this.endpoint = endpoint;
            }
        }

        public static class Logs {

            private boolean enabled = false;

            private long reportFrequency = 60;

            public long getReportFrequency() {
                return reportFrequency;
            }

            public void setReportFrequency(int reportFrequency) {
                this.reportFrequency = reportFrequency;
            }

            public boolean isEnabled() {
                return enabled;
            }

            public void setEnabled(boolean enabled) {
                this.enabled = enabled;
            }
        }
    }

    private final Logging logging = new Logging();

    public Logging getLogging() { return logging; }

    public static class Logging {

        private final Logstash logstash = new Logstash();

        public Logstash getLogstash() { return logstash; }

        public static class Logstash {

            private boolean enabled = false;

            private String host = "localhost";

            private int port = 5000;

            private int queueSize = 512;

            public boolean isEnabled() { return enabled; }

            public void setEnabled(boolean enabled) { this.enabled = enabled; }

            public String getHost() { return host; }

            public void setHost(String host) { this.host = host; }

            public int getPort() { return port; }

            public void setPort(int port) { this.port = port; }

            public int getQueueSize() { return queueSize; }

            public void setQueueSize(int queueSize) { this.queueSize = queueSize; }
        }
    }

    public static class Social {

        private String redirectAfterSignIn = "/#/home";

        public String getRedirectAfterSignIn() {
            return redirectAfterSignIn;
        }

        public void setRedirectAfterSignIn(String redirectAfterSignIn) {
            this.redirectAfterSignIn = redirectAfterSignIn;
        }
    }

    public static class Ribbon {

        private String[] displayOnActiveProfiles;

        public String[] getDisplayOnActiveProfiles() {
            return displayOnActiveProfiles;
        }

        public void setDisplayOnActiveProfiles(String[] displayOnActiveProfiles) {
            this.displayOnActiveProfiles = displayOnActiveProfiles;
        }
    }
    
    public static class Smartstore {
    	private String url;
    	private String company;
    	private String systemId;
    	private String repository;
    	
    	
    	//private String createUrl="http://localhost:8080/smartstore/actiprocess/thriveni?create&pVersion=0046&contRep=A7&docId=00000000000000000000000000000000&compId=data&docProt=rud&accessMode=c&authId=CN=MSP&expiration=20170330082306&sp=true&secKey=MIH4BgkqhkiG9w0BBwKggeowgecCAQExCzAJBgUrDgMCGgUAMAsGCSqGSIb3DQEHATGBxzCBxAIBATAZMA4xDDAKBgNVBAMTA0lEMwIHIBUBGAlAEDAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTcwMzMwMDYyMzA2WjAjBgkqhkiG9w0BCQQxFgQUXp+j6yi3h79tsnyLrse1bGrSyYAwCQYHKoZIzjgEAwQvMC0CFQDXXQ741+hxLNhT1biNGa/EJpTXfwIUBHkVs7AvjG75b+PSdsoR9KedVo4=";
    		//	private String createUrl="http://localhost:8080/smartstore/actiprocess/thriveni?create&pVersion=0046&contRep=R5&docId=00000000000000000000000000000000&compId=data&docProt=rud&accessMode=c&authId=CN=smartportal&expiration=20170330082306&sp=true&secKey=MIH4BgkqhkiG9w0BBwKggeowgecCAQExCzAJBgUrDgMCGgUAMAsGCSqGSIb3DQEHATGBxzCBxAIBATAZMA4xDDAKBgNVBAMTA0lEMwIHIBUBGAlAEDAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTcwMzMwMDYyMzA2WjAjBgkqhkiG9w0BCQQxFgQUXp+j6yi3h79tsnyLrse1bGrSyYAwCQYHKoZIzjgEAwQvMC0CFQDXXQ741+hxLNhT1biNGa/EJpTXfwIUBHkVs7AvjG75b+PSdsoR9KedVo4=";
    		//	private String createUrl="http://smartportal-smartstore-r5.appspot.com/actiprocess/smartdocs?create&pVersion=0046&contRep=R5&docId=00000000000000000000000000000000&compId=data&docProt=rud&accessMode=c&authId=CN=ID3&expiration=20170330082306&sp=true&secKey=MIH4BgkqhkiG9w0BBwKggeowgecCAQExCzAJBgUrDgMCGgUAMAsGCSqGSIb3DQEHATGBxzCBxAIBATAZMA4xDDAKBgNVBAMTA0lEMwIHIBUBGAlAEDAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTcwMzMwMDYyMzA2WjAjBgkqhkiG9w0BCQQxFgQUXp+j6yi3h79tsnyLrse1bGrSyYAwCQYHKoZIzjgEAwQvMC0CFQDXXQ741+hxLNhT1biNGa/EJpTXfwIUBHkVs7AvjG75b+PSdsoR9KedVo4=";

		public String getCreateUrl() {
			String u=getUrl()+"/actiprocess/"+getCompany()+"?create&pVersion=0046&contRep="+getRepository()+"&docId=00000000000000000000000000000000&compId=data&docProt=rud&accessMode=c&authId=CN="+getSystemId()+"&expiration=20170330082306&sp=true&secKey=MIH4BgkqhkiG9w0BBwKggeowgecCAQExCzAJBgUrDgMCGgUAMAsGCSqGSIb3DQEHATGBxzCBxAIBATAZMA4xDDAKBgNVBAMTA0lEMwIHIBUBGAlAEDAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTcwMzMwMDYyMzA2WjAjBgkqhkiG9w0BCQQxFgQUXp+j6yi3h79tsnyLrse1bGrSyYAwCQYHKoZIzjgEAwQvMC0CFQDXXQ741+hxLNhT1biNGa/EJpTXfwIUBHkVs7AvjG75b+PSdsoR9KedVo4=";
			return u;
			//return createUrl;
		}

		/*public void setCreateUrl(String createUrl) {
			this.createUrl = createUrl;
		}*/
    	//http://smartportal-smartstore-r5.appspot.com/actiprocess/smartdocs
		//http://35.201.138.214/smartstore/actiprocess/actiprocess
		//http://localhost/smartstore/actiprocess/actiprocess
	//	private String getUrl="http://localhost:8080/smartstore/actiprocess/thriveni?get&pVersion=0046&contRep=A7&docId=00000000000000000000000000000000&compId=data&docProt=rud&accessMode=c&authId=CN=MSP&expiration=20170330082306&sp=true&secKey=MIH4BgkqhkiG9w0BBwKggeowgecCAQExCzAJBgUrDgMCGgUAMAsGCSqGSIb3DQEHATGBxzCBxAIBATAZMA4xDDAKBgNVBAMTA0lEMwIHIBUBGAlAEDAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTcwMzMwMDYyMzA2WjAjBgkqhkiG9w0BCQQxFgQUXp+j6yi3h79tsnyLrse1bGrSyYAwCQYHKoZIzjgEAwQvMC0CFQDXXQ741+hxLNhT1biNGa/EJpTXfwIUBHkVs7AvjG75b+PSdsoR9KedVo4=&sp=true";
		//	private String getUrl="http://localhost:8080/smartstore/actiprocess/thriveni?get&pVersion=0046&contRep=R5&docId=00000000000000000000000000000000&compId=data&docProt=rud&accessMode=c&authId=CN=smartportal&expiration=20170330082306&sp=true&secKey=MIH4BgkqhkiG9w0BBwKggeowgecCAQExCzAJBgUrDgMCGgUAMAsGCSqGSIb3DQEHATGBxzCBxAIBATAZMA4xDDAKBgNVBAMTA0lEMwIHIBUBGAlAEDAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTcwMzMwMDYyMzA2WjAjBgkqhkiG9w0BCQQxFgQUXp+j6yi3h79tsnyLrse1bGrSyYAwCQYHKoZIzjgEAwQvMC0CFQDXXQ741+hxLNhT1biNGa/EJpTXfwIUBHkVs7AvjG75b+PSdsoR9KedVo4=&sp=true";
		//private String getUrl="http://smartportal-smartstore-r5.appspot.com/actiprocess/smartdocs?get&pVersion=0046&contRep=R5&docId=00000000000000000000000000000000&compId=data&docProt=rud&accessMode=c&authId=CN=ID3&expiration=20170330082306&sp=true&secKey=MIH4BgkqhkiG9w0BBwKggeowgecCAQExCzAJBgUrDgMCGgUAMAsGCSqGSIb3DQEHATGBxzCBxAIBATAZMA4xDDAKBgNVBAMTA0lEMwIHIBUBGAlAEDAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTcwMzMwMDYyMzA2WjAjBgkqhkiG9w0BCQQxFgQUXp+j6yi3h79tsnyLrse1bGrSyYAwCQYHKoZIzjgEAwQvMC0CFQDXXQ741+hxLNhT1biNGa/EJpTXfwIUBHkVs7AvjG75b+PSdsoR9KedVo4=&sp=true";

		public String getGetUrl() {
			
			String u=getUrl()+"/actiprocess/"+getCompany()+"?get&pVersion=0046&contRep=R5&docId=00000000000000000000000000000000&compId=data&docProt=rud&accessMode=c&authId=CN="+getSystemId()+"&expiration=20170330082306&sp=true&secKey=MIH4BgkqhkiG9w0BBwKggeowgecCAQExCzAJBgUrDgMCGgUAMAsGCSqGSIb3DQEHATGBxzCBxAIBATAZMA4xDDAKBgNVBAMTA0lEMwIHIBUBGAlAEDAJBgUrDgMCGgUAoF0wGAYJKoZIhvcNAQkDMQsGCSqGSIb3DQEHATAcBgkqhkiG9w0BCQUxDxcNMTcwMzMwMDYyMzA2WjAjBgkqhkiG9w0BCQQxFgQUXp+j6yi3h79tsnyLrse1bGrSyYAwCQYHKoZIzjgEAwQvMC0CFQDXXQ741+hxLNhT1biNGa/EJpTXfwIUBHkVs7AvjG75b+PSdsoR9KedVo4=&sp=true";
			return u;
		}


		public String getCompany() {
			return company;
		}

		public void setCompany(String company) {
			this.company = company;
		}

		public String getSystemId() {
			return systemId;
		}

		public void setSystemId(String systemId) {
			this.systemId = systemId;
		}


		public String getRepository() {
			return repository;
		}

		public void setRepository(String repository) {
			this.repository = repository;
		}

		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}
    	
    }
    
    public Smartstore getSmartstore() {
		return smartstore;
	}

	public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    
    public static class Gdrive {
    	
    	private String clientId;
    	private String clientSecret;
    	private String redirectUri;
    	
    	public String getClientId() {
			return clientId;
		}
		public void setClientId(String clientId) {
			this.clientId = clientId;
		}
		public String getClientSecret() {
			return clientSecret;
		}
		public void setClientSecret(String clientSecret) {
			this.clientSecret = clientSecret;
		}
		public String getRedirectUri() {
			return redirectUri;
		}
		public void setRedirectUri(String redirectUri) {
			this.redirectUri = redirectUri;
		}
    	
    }
    
    public static class OneDrive {
    	private String clientId;
    	public String getClientId() {
			return clientId;
		}
		public void setClientId(String clientId) {
			this.clientId = clientId;
		}
		public String getClientSecret() {
			return clientSecret;
		}
		public void setClientSecret(String clientSecret) {
			this.clientSecret = clientSecret;
		}
		public String getRedirectUri() {
			return redirectUri;
		}
		public void setRedirectUri(String redirectUri) {
			this.redirectUri = redirectUri;
		}
		private String clientSecret;
    	private String redirectUri;
    	
    }
   
    public OneDrive getOnedrive() {
		return onedrive;
	}
    
 public static class Azure {
    	
    	private String containerName;
    	private String storageConnectionString;
		public String getContainerName() {
			//return containerName;
			return TenantContextHolder.getTenant();
		}
		public void setContainerName(String containerName) {
			this.containerName = containerName;
		}
		public String getStorageConnectionString() {
			return storageConnectionString;
		}
		public void setStorageConnectionString(String storageConnectionString) {
			this.storageConnectionString = storageConnectionString;
		}
    	
 }
 
 
    
}
