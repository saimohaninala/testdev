package com.smartdocs.smartportal.config.multitenant;

import java.net.UnknownHostException;

import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;

import com.mongodb.DB;
import com.mongodb.MongoClientURI;
import com.smartdocs.smartportal.util.TenantContextHolder;

public class MultiTenantMongoDbFactory extends SimpleMongoDbFactory {

	public String DEFAULT_DB = "smartportal";

	public MultiTenantMongoDbFactory(MongoClientURI uri) throws UnknownHostException {

		super(uri);
	}

	@Override
	public DB getDb() throws DataAccessException {
		

		String tenant = TenantContextHolder.getTenant();
		System.out.println("Tenant Name"+tenant);

		if (tenant != null)
		return getDb(tenant);
		return super.getDb(DEFAULT_DB);
	}

}
