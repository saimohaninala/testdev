package com.smartdocs.smartportal.config;

import com.smartdocs.smartportal.config.multitenant.MultiTenantMongoDbFactory;
import com.smartdocs.smartportal.domain.util.JSR310DateConverters.*;
import com.mongodb.Mongo;
import com.mongodb.MongoClientURI;
import com.github.mongobee.Mongobee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.Import;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.convert.CustomConversions;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.event.ValidatingMongoEventListener;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.inject.Inject;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

@Configuration
@Profile("!" + Constants.SPRING_PROFILE_CLOUD)
@EnableMongoRepositories("com.smartdocs.smartportal.repository")
@Import(value = MongoAutoConfiguration.class)
@EnableMongoAuditing(auditorAwareRef = "springSecurityAuditorAware")
public class DatabaseConfiguration {

    private final Logger log = LoggerFactory.getLogger(DatabaseConfiguration.class);

    @Bean
    public ValidatingMongoEventListener validatingMongoEventListener() {
        return new ValidatingMongoEventListener(validator());
    }

    @Bean
    public LocalValidatorFactoryBean validator() {
        return new LocalValidatorFactoryBean();
    }

    @Bean
    public CustomConversions customConversions() {
        List<Converter<?, ?>> converters = new ArrayList<>();
        converters.add(DateToZonedDateTimeConverter.INSTANCE);
        converters.add(ZonedDateTimeToDateConverter.INSTANCE);
        return new CustomConversions(converters);
    }

/*    @Bean
    public Mongobee mongobee(Mongo mongo, MongoProperties mongoProperties) {
        log.debug("Configuring Mongobee");
        Mongobee mongobee = new Mongobee(mongo);
        mongobee.setDbName(mongoProperties.getDatabase());
        // package to scan for migrations
        mongobee.setChangeLogsScanPackage("com.smartdocs.smartportal.config.dbmigrations");
        mongobee.setEnabled(true);
        return mongobee;
    }
*/    
    
	@Bean

	public MongoDbFactory mongoDbFactory(Mongo mongo, MongoProperties mongoProperties) throws UnknownHostException {

		MongoClientURI mongoClientURI = new MongoClientURI(mongoProperties.getUri());

		return new MultiTenantMongoDbFactory(mongoClientURI);
	}
    @Bean
    public GridFsTemplate gridFsTemplate(Mongo mongo, MongoProperties mongoProperties, MappingMongoConverter mappingMongoConverter) throws Exception {
        return new GridFsTemplate(mongoDbFactory(mongo,mongoProperties), mappingMongoConverter);
    }

    
}
