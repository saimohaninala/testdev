package com.smartdocs.smartportal.exception;

public class InvalidWorkflowException extends RuntimeException {

    public InvalidWorkflowException() {
    }

    public InvalidWorkflowException(String message) {
        super(message);
    }
}