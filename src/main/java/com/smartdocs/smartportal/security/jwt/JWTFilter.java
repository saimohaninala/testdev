package com.smartdocs.smartportal.security.jwt;

import java.io.IOException;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import com.google.common.net.InternetDomainName;
import com.smartdocs.smartportal.util.TenantContextHolder;

import io.jsonwebtoken.ExpiredJwtException;

/**
 * Filters incoming requests and installs a Spring Security principal if a header corresponding to a valid user is
 * found.
 */
public class JWTFilter extends GenericFilterBean {

    private final Logger log = LoggerFactory.getLogger(JWTFilter.class);

    private TokenProvider tokenProvider;

    public JWTFilter(TokenProvider tokenProvider) {
        this.tokenProvider = tokenProvider;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
        throws IOException, ServletException {
    	 System.out.println("remote ip address***********************"+servletRequest.getRemoteAddr());
        try {
            HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
            
            System.out.println("RequestUri"+httpServletRequest.getRequestURI()+"RequestUrl"+httpServletRequest.getRequestURL());
            
            
            String jwt = resolveToken(httpServletRequest);
            if (StringUtils.hasText(jwt)) {
                if (this.tokenProvider.validateToken(jwt)) {       
                    Authentication authentication = this.tokenProvider.getAuthentication(jwt);
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }else{
            	String tenantId=servletRequest.getParameter("tenant");
            	//String 
            	if(tenantId==null){
            		tenantId=extractSubdomain(servletRequest);
            		if(tenantId==null)
            		tenantId="smartdocs";
            	}
                TenantContextHolder.setTenantId(tenantId);
              
            }
            filterChain.doFilter(servletRequest, servletResponse);
        } catch (ExpiredJwtException eje) {
            log.info("Security exception for user {} - {}", eje.getClaims().getSubject(), eje.getMessage());
            ((HttpServletResponse) servletResponse).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    private String resolveToken(HttpServletRequest request){
        String bearerToken = request.getHeader(JWTConfigurer.AUTHORIZATION_HEADER);
        String jwtToken=request.getParameter("jwtToken");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            String jwt = bearerToken.substring(7, bearerToken.length());
            return jwt;
        }else if(jwtToken!=null){
        		return jwtToken;
        	}
        
        return null;
    }
    String extractSubdomain(ServletRequest req) {
        return InternetDomainName.from(req.getServerName()).parts().get(0);
      }

}
