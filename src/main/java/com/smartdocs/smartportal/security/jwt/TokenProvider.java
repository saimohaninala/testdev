package com.smartdocs.smartportal.security.jwt;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import com.smartdocs.smartportal.config.JHipsterProperties;
import com.smartdocs.smartportal.domain.Authority;
import com.smartdocs.smartportal.service.UserService;
import com.smartdocs.smartportal.util.TenantContextHolder;
import com.smartdocs.smartportal.web.rest.JWTTokenMobile;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;


@Component
public class TokenProvider {

    private final Logger log = LoggerFactory.getLogger(TokenProvider.class);

    private static final String AUTHORITIES_KEY = "auth";
    private static final String TENANT_ID = "tenant";

    private String secretKey;

    private long tokenValidityInMilliseconds;

    private long tokenValidityInMillisecondsForRememberMe;
    
    @Inject
	private UserService userService;

    @Inject
    private JHipsterProperties jHipsterProperties;

    @PostConstruct
    public void init() {
        this.secretKey =
            jHipsterProperties.getSecurity().getAuthentication().getJwt().getSecret();

        this.tokenValidityInMilliseconds =
            1000 * jHipsterProperties.getSecurity().getAuthentication().getJwt().getTokenValidityInSeconds();
        this.tokenValidityInMillisecondsForRememberMe =
            1000 * jHipsterProperties.getSecurity().getAuthentication().getJwt().getTokenValidityInSecondsForRememberMe();
    }

    public String createToken(Authentication authentication, Boolean rememberMe) {
        String authorities = authentication.getAuthorities().stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.joining(","));
        
        System.out.println("authorities in create Token"+authorities);

        long now = (new Date()).getTime();
        Date validity;
        if (rememberMe) {
            validity = new Date(now + this.tokenValidityInMillisecondsForRememberMe);
        } else {
            validity = new Date(now + this.tokenValidityInMilliseconds);
        }
        
        System.out.println("authentication.Subject.getName():"+authentication.getName());
        String tenantId=TenantContextHolder.getTenant();
        return Jwts.builder()
            .setSubject(authentication.getName())
            .claim(AUTHORITIES_KEY, authorities)
            .claim(TENANT_ID, tenantId)
            .signWith(SignatureAlgorithm.HS512, secretKey)
            .setExpiration(validity)
            .compact();
    }
    
    
    public JWTTokenMobile createTokenforMobile(String login, Boolean rememberMe) {
    	
    com.smartdocs.smartportal.domain.User user=  userService.findOneObjectByLogin(login);
    	
    	if(user ==null) {
    		return null;
    	}else {

    	
 	Set<Authority> authorities=	user.getAuthorities();
 	Set<String> auth=new HashSet<String>();
 	for (Authority authority : authorities) {
 		auth.add(authority.getName().trim());
	}
 	//String str_1 = StringUtils.join(auth, " ");
 	String[] array = auth.stream().toArray(String[]::new);
	String s=Arrays.toString(array);
	String	str = s.replaceAll("\\[", "").replaceAll("\\]","");
	long now = (new Date()).getTime();
        Date validity;
        if (rememberMe) {
            validity = new Date(now + this.tokenValidityInMillisecondsForRememberMe);
        } else {
            validity = new Date(now + this.tokenValidityInMilliseconds);
        }
        
    String jwttoken=    Jwts.builder()
        .setSubject(login)
        .claim(AUTHORITIES_KEY,str)
        .signWith(SignatureAlgorithm.HS512, secretKey)
        .setExpiration(validity)
        .compact();
    
    JWTTokenMobile mobileLogin=new JWTTokenMobile();
    mobileLogin.setIdToken(jwttoken);
    mobileLogin.setUserId(login);
    mobileLogin.setEmail(user.getEmail());
   
        return mobileLogin;
    }
    }
    
    
    
    

    public Authentication getAuthentication(String token) {
        Claims claims = Jwts.parser()
            .setSigningKey(secretKey)
            .parseClaimsJws(token)
            .getBody();

        Collection<? extends GrantedAuthority> authorities =
            Arrays.stream(claims.get(AUTHORITIES_KEY).toString().split(","))
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
        String tenantId = claims.get(TENANT_ID) != null ? claims.get(TENANT_ID).toString() : null;
        TenantContextHolder.setTenantId(tenantId);
        
        User principal = new User(claims.getSubject(), "", authorities);

        return new UsernamePasswordAuthenticationToken(principal, "", authorities);
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            log.info("Invalid JWT signature: " + e.getMessage());
            return false;
        }
    }
}
