package com.smartdocs.smartportal.service.impl;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.inject.Inject;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.google.api.client.util.Clock;
import com.smartdocs.smartportal.config.JHipsterProperties;
import com.smartdocs.smartportal.domain.GdriveToken;
import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.repository.GdriveTokenRepository;
import com.smartdocs.smartportal.service.OneDriveOauthTokenService;
import com.smartdocs.smartportal.smartstore.OneDriveOauthUtils;


@Service
public class OneDriveOauthTokenServiceImpl implements OneDriveOauthTokenService {
	
	
	@Inject
    private  GdriveTokenRepository tokenInfoRepository;
	@Inject
	private JHipsterProperties jHipsterProperties;
	
	
	@Override
	public String getActiveAccessToken(Repository repository,String id) {
		// TODO Auto-generated method stub
		 String accessToken=null;
		OneDriveOauthUtils oauthutil=new OneDriveOauthUtils();
		GdriveToken tokenInfo=tokenInfoRepository.findOne(id);
		accessToken=tokenInfo.getAccessToken();
		Clock clock = Clock.SYSTEM;
		 final Lock lock = new ReentrantLock();
	    lock.lock();
	    try {
	      // check if token will expire in a minute
	       Long expiresIn= (tokenInfo.getExpirationTimeMilliseconds() - clock.currentTimeMillis()) / 1000;
	      if (tokenInfo.getAccessToken() == null || expiresIn != null && expiresIn <= (60-10)) {
	    	  JSONObject jsonObject=  oauthutil.refreshToken(repository,tokenInfo.getRefreshToken(),jHipsterProperties.getOnedrive().getRedirectUri());
	    	  String accesstoken= (String) jsonObject.get("access_token");
	    	  System.out.println("accesstoken in onedriveoauthtokenService"+accesstoken);
	    	  
	        if (accesstoken== null) {
	          // nothing we can do without an access token
	        }else {
	        	
		    	tokenInfo.setAccessToken(accesstoken);
		    	tokenInfoRepository.save(tokenInfo);
		    	
		    	GdriveToken tokenInfo1=tokenInfoRepository.findOne(id);
		    	accessToken=	tokenInfo1.getAccessToken();
	        
	         return accessToken;
	        }
	      }else {
	        	return tokenInfo.getAccessToken();
	        }
	    } catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
	      lock.unlock();
	    }
		
	    return accessToken;
		
	}
	
	

	

}
