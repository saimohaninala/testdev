package com.smartdocs.smartportal.service.impl;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.smartdocs.smartportal.domain.Certificate;
import com.smartdocs.smartportal.repository.CertificateRepository;
import com.smartdocs.smartportal.service.CertificateService;

@Service
public class CertificateServiceImpl  implements CertificateService {
	@Inject
	CertificateRepository certificateRepository;

	@Override
	public Certificate findCertificateBySystemId(String systemId) {
		// TODO Auto-generated method stub
		return certificateRepository.findCertificateBySystemId(systemId);
	}

	@Override
	public void deleteCertificateBySystemId( String systemId) {
		// TODO Auto-generated method stub
		certificateRepository.deleteCertificateBySystemId( systemId);
	}

	@Override
	public Certificate save(Certificate certificate) {
		// TODO Auto-generated method stub
		return certificateRepository.save(certificate);
	}

	@Override
	public Page<Certificate> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Certificate findOne(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		
	}

}
