package com.smartdocs.smartportal.service.impl;

import java.util.Optional;
import javax.inject.Inject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.smartdocs.smartportal.domain.SmartstoreSettings;
import com.smartdocs.smartportal.repository.SmartstoreSettingsRepository;
import com.smartdocs.smartportal.service.SmartstoreSettingsService;
@Service
public class SmartstoreSettingsServiceImpl implements SmartstoreSettingsService {

	@Inject
	private SmartstoreSettingsRepository smartstoreSettingsRepository;
	@Override
	public SmartstoreSettings save(SmartstoreSettings smartstore) {
		SmartstoreSettings result=smartstoreSettingsRepository.save(smartstore);
		return result;
	}

	@Override
	public Page<SmartstoreSettings> findAll(Pageable pageable) {
		 Page<SmartstoreSettings> result=smartstoreSettingsRepository.findAll(pageable);
		return result;
	}

	@Override
	public SmartstoreSettings findOne(String id) {
		SmartstoreSettings result = smartstoreSettingsRepository.findOne(id);
        return result;
	}

	@Override
	public void delete(String id) {
		smartstoreSettingsRepository.delete(id);

	}

	@Override
	public Optional<SmartstoreSettings> findOneByKey(String key) {
		return smartstoreSettingsRepository.findOneByKey(key);
	}

}
