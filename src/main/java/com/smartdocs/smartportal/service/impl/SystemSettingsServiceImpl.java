package com.smartdocs.smartportal.service.impl;

import com.smartdocs.smartportal.service.SystemSettingsService;
import com.smartdocs.smartportal.domain.SystemSettings;
import com.smartdocs.smartportal.repository.SystemSettingsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing DocumentTemplate.
 */
@Service
public class SystemSettingsServiceImpl implements SystemSettingsService{

    private final Logger log = LoggerFactory.getLogger(SystemSettingsServiceImpl.class);
    
    private final SystemSettingsRepository systemSettingsRepository;

    public SystemSettingsServiceImpl(SystemSettingsRepository systemSettingsRepository) {
        this.systemSettingsRepository = systemSettingsRepository;
    }

    /**
     * Save a SystemSettings.
     *
     * @param SystemSettings the entity to save
     * @return the persisted entity
     */
    @Override
    public SystemSettings save(SystemSettings systemSettings) {
        log.debug("Request to save SystemSettings : {}", systemSettings);
        SystemSettings result = systemSettingsRepository.save(systemSettings);
        return result;
    }

    /**
     *  Get all the SystemSettings.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    public Page<SystemSettings> findAll(Pageable pageable) {
        log.debug("Request to get all DocumentTemplate");
        Page<SystemSettings> result = systemSettingsRepository.findAll(pageable);
        return result;
    }

    /**
     *  Get one SystemSettings by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    public SystemSettings findOne(String id) {
        log.debug("Request to get SystemSettings : {}", id);
        SystemSettings systemSettings = systemSettingsRepository.findOne(id);
        return systemSettings;
    }

    /**
     *  Delete the  SystemSettings by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete SystemSettings : {}", id);
        systemSettingsRepository.delete(id);
    }

	@Override
	public Optional<SystemSettings> findOneBySystem(String system) {
		// TODO Auto-generated method stub
		return systemSettingsRepository.findOneBySystem(system);
	}
}