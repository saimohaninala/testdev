package com.smartdocs.smartportal.service.impl;

import java.util.Optional;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.domain.Systems;
import com.smartdocs.smartportal.repository.SystemsRepository;
import com.smartdocs.smartportal.service.SystemsService;
@Service
public class SystemsServiceImpl implements SystemsService {

	@Inject
	private SystemsRepository systemsRepository;
	@Override
	public Systems save(Systems systems) {
		Systems result=systemsRepository.save(systems);
		return result;
	}

	@Override
	public Page<Systems> findAll(Pageable pageable) {
		Page<Systems> result=systemsRepository.findAll(pageable);
		return result;
	}

	@Override
	public Systems findOne(String id) {
		Systems result=systemsRepository.findOne(id);
		return result;
	}

	@Override
	public void delete(String id) {
		 systemsRepository.delete(id);
	}

	@Override
	public Optional<Systems> findOneBySystemName(String systemName) {

		return systemsRepository.findOneBySystemName(systemName);
	}

	@Override
	public Systems findOneBySystemAuthId(String authId) {
		// TODO Auto-generated method stub
		return systemsRepository.findOneByAuthId(authId);
	}

	@Override
	public Systems findOneBySystemId(String systemId) {
		// TODO Auto-generated method stub
		return systemsRepository.findOneBySystemId(systemId);
	}
	

}
