package com.smartdocs.smartportal.service.impl;

import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.smartdocs.smartportal.domain.Company;
import com.smartdocs.smartportal.domain.MimeType;
import com.smartdocs.smartportal.repository.MimeRepository;
import com.smartdocs.smartportal.service.MimeTypeService;
@Service
public class MimeTypeServiceImpl implements MimeTypeService {

	 @Inject
	 private  MimeRepository  mimeRepository;
	 
	@Override
	public MimeType save(MimeType mimeType) {
		MimeType result=mimeRepository.save(mimeType);
		return result;
	}

	@Override
	public Page<MimeType> findAll(Pageable pageable) {
		 Page<MimeType> result=mimeRepository.findAll(pageable);
			return result;
	}

	@Override
	public void delete(String id) {
		mimeRepository.delete(id);

	}

	@Override
	public MimeType findOneByMimeType(String mimeType) {
		MimeType result= mimeRepository.findOneByMimeType(mimeType);
		  return result; 
	}

	@Override
	public Optional<MimeType> findOneBymimeType(String mimeType) {
		// TODO Auto-generated method stub
		return mimeRepository.findOneBymimeType(mimeType);
	}

	@Override
	public List<MimeType> findMimeTypes() {
		// TODO Auto-generated method stub
	List<MimeType> result=	mimeRepository.findAll();
		return result;
	}

	@Override
	public MimeType findOneByfileExtension(String fileExtension) {
		MimeType result=mimeRepository.findOneByfileExtension(fileExtension);
		return result;
	}
	
	
	@Override
	public MimeType findMimeTypeByCompanyId(String companyId) {
		// TODO Auto-generated method stub
	MimeType result=	mimeRepository.findMimeTypeByCompanyId(companyId);
		return result;
	}

}
