package com.smartdocs.smartportal.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.google.api.client.http.ByteArrayContent;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpContent;
import com.google.api.client.http.HttpHeaders;
import com.google.api.client.http.HttpMediaType;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpResponse;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.MultipartContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.http.json.JsonHttpContent;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.JsonObjectParser;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.common.collect.Maps;
import com.google.common.net.MediaType;
import com.smartdocs.smartportal.domain.GdriveToken;
import com.smartdocs.smartportal.repository.GdriveTokenRepository;
import com.smartdocs.smartportal.service.OneDriveClientApi;
import com.smartdocs.smartportal.service.OneDriveOauthTokenService;
@Service
public class OneDriveClientApiImpl implements OneDriveClientApi {
	
	  private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();

	  /* Global instance of the JSON factory. */
	  private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	@Inject
	OneDriveOauthTokenService oauthtokenService;
	@Inject
	GdriveTokenRepository gdriveTokenRepository;
	
	private HttpRequestFactory requestFactory;
	
	@Inject
	OneDriveOauthTokenService onedriveoauthtokenservice;
	
	public OneDriveClientApiImpl() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OneDriveClientApiImpl(String accessToken) {
		super();
		 this.requestFactory = HTTP_TRANSPORT.createRequestFactory(new HttpRequestInitializer() {
			
			@Override
			public void initialize(HttpRequest request) throws IOException {
				
				 request.setParser(new JsonObjectParser(JSON_FACTORY));
				 
				 HttpHeaders header =request.getHeaders();
			
				 
				 System.out.println("accessToken>>>>>>>>>>>>>>"+accessToken);
				 
				 header.setAuthorization("Bearer "+accessToken);
				 header.setAccept("*/*");
			
                // request.setHeaders(header);
                 
                
               //  request.setHeaders(new HttpHeaders().setAccept("application/json"));

				
			}
		});
	
	}
	
	public HttpResponse  createDocument(String url, byte[] data, String conetentType){
		HttpResponse response=null;

/*		Map<String, String> parameters = Maps.newHashMap();
		parameters.put("contentType", conetentType);

*/		// Map print options into CJT structure

		// Add parameters
/*		MultipartContent content = new MultipartContent().setMediaType(
		        new HttpMediaType("multipart/form-data")
		                .setParameter("boundary", "__END_OF_PART__"));
		for (String name : parameters.keySet()) {
		    MultipartContent.Part part = new MultipartContent.Part(
		            new ByteArrayContent(null, parameters.get(name).getBytes()));
		    part.setHeaders(new HttpHeaders().set(
		            "Content-Disposition", String.format("form-data; name=\"%s\"", name)));
		    content.addPart(part);
		}
*/    String a="test content";
   
		// Add file
		ByteArrayContent byteArrayContent=new ByteArrayContent(conetentType, a.getBytes());
		//FileContent fileContent = new FileContent(
		  //      "text/plain", new File());
/*		MultipartContent.Part part = new MultipartContent.Part(byteArrayContent);
		part.setHeaders(new HttpHeaders().set(
		        "Content-Disposition", 
		        String.format("form-data; name=\"content\"; filename=\"%s\"", file.getName())));
		content.addPart(part);
*/        try {
			response= put(url, byteArrayContent);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			return response;
	}
	
	 @Override
	  public HttpResponse get(String url) throws IOException {
		    if (requestFactory == null)
		      throw new RuntimeException("API access must be authorized before using it");

		    HttpRequest request = requestFactory.buildGetRequest(new GenericUrl(url));
		    return request.execute();
		  }
	 	@Override
	  public HttpResponse post(String url, Object data) throws IOException {
		    if (requestFactory == null)
		      throw new RuntimeException("API access must be authorized before using it");

		    HttpContent content = data == null ? null : new JsonHttpContent(JSON_FACTORY, data);
		    HttpRequest request = requestFactory.buildPostRequest(new GenericUrl(url), content);
		    return request.execute();
		  }
	 	@Override
		  public HttpResponse put(String url, Object data) throws IOException {
		    if (requestFactory == null)
		      throw new RuntimeException("API access must be authorized before using it");

		    HttpContent content = data == null ? null : new JsonHttpContent(JSON_FACTORY, data);
		    
		    HttpRequest request = requestFactory.buildPutRequest(new GenericUrl(url), content);
		    
		    return request.execute();
		  }

	 	@Override
		  public HttpResponse delete(String url) throws IOException {
		    if (requestFactory == null)
		      throw new RuntimeException("API access must be authorized before using it");

		    HttpRequest request = requestFactory.buildDeleteRequest(new GenericUrl(url));
		    return request.execute();
		  }
		}



