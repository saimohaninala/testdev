package com.smartdocs.smartportal.service.impl;

import java.util.List;
import java.util.Optional;
import javax.inject.Inject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.repository.RepositoryRepository;
import com.smartdocs.smartportal.service.RepositoryService;

@Service
public class RepositoryServiceImpl implements RepositoryService {

	@Inject
	private RepositoryRepository repositoryRepository;
	@Override
	public Repository save(Repository repository) {
		Repository result=repositoryRepository.save(repository);
		return result;
	}

	@Override
	public Page<Repository> findAll(Pageable pageable) {
		Page<Repository> result=repositoryRepository.findAll(pageable);
		return result;
	}

	@Override
	public void delete(String repositoryId) {
		repositoryRepository.delete(repositoryId);

	}

	@Override
	public Optional<Repository> findOneByName(String name) {
	     return repositoryRepository.findOneByName(name); 
	}

	@Override
	public Repository findOneByrepositoryId(String repositoryId) {
		Repository result= repositoryRepository.findOneByrepositoryId(repositoryId);
	     return result; 
	}

	
	@Override
	public List<Repository> findOneByRepositoryId(String repositoryId) {
		List<Repository> result=repositoryRepository.findOneByRepositoryId(repositoryId);
		return result;
	}

	@Override
	public List<Repository> findAllList() {
		// TODO Auto-generated method stub
	List<Repository> result=repositoryRepository.findAll();
		return result;
	}

	@Override
	public List<Repository> findOneBySystemId(String systemId) {
		// TODO Auto-generated method stub
		return repositoryRepository.findOneBySystemId(systemId);
	}
	
	

	
}
