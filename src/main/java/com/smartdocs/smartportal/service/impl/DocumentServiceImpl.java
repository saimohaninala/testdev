package com.smartdocs.smartportal.service.impl;

import java.util.List;

import javax.inject.Inject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.smartdocs.smartportal.domain.Document;
import com.smartdocs.smartportal.repository.DocumentRepository;
import com.smartdocs.smartportal.service.DocumentService;

@Service
public class DocumentServiceImpl implements DocumentService {
	
	@Inject
	DocumentRepository documentRepository;

	@Override
	public Document save(Document document) {
		Document result=documentRepository.save(document);
		return result;
	}

	
	@Override
	public Page<Document> findAll(Pageable pageable) {
		Page<Document> result=documentRepository.findAll(pageable);
		return result;
	}

	@Override
	public Document findOne(String id) {
		Document result=documentRepository.findOne(id);
		return result;
	}

	@Override
	public void delete(String id) {
		documentRepository.delete(id);

	}


	@Override
	public Document findOneByDocId(String docId) {
		Document result=documentRepository.findOneByArchiveDocId(docId);
		return result;
	}


	@Override
	public List<Document> findOneByArchiveDocIdAndArchiveId(String archiveDocId, String archiveId) {
		// TODO Auto-generated method stub
		return documentRepository.findOneByArchiveDocIdAndArchiveId(archiveDocId, archiveId);
	}


	@Override
	public List<Document> findByDocIdAndFileName(String archiveDocId, String fileName) {
		// TODO Auto-generated method stub
		return documentRepository.findByArchiveDocIdAndFileName(archiveDocId, fileName);
	}


	@Override
	public Document findOneByArchiveDocIdAndFileName(String archiveDocId, String fileName) {
		// TODO Auto-generated method stub
		return documentRepository.findOneByArchiveDocIdAndFileName(archiveDocId, fileName);
	}


	@Override
	public List<Document> findOneBySystemIdAndArchiveIdAndArchiveDocId(String systemId, String archiveId,
			String archiveDocId) {
		// TODO Auto-generated method stub
		return documentRepository.findOneBySystemIdAndArchiveIdAndArchiveDocId(systemId,archiveId,archiveDocId);
	}

}
