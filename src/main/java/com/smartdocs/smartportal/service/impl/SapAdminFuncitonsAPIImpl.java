package com.smartdocs.smartportal.service.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;

import com.google.api.client.util.Strings;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.domain.Systems;
import com.smartdocs.smartportal.service.AdminFuncitonsAPI;
import com.smartdocs.smartportal.service.RepositoryService;
import com.smartdocs.smartportal.service.SystemsService;
import com.smartdocs.smartportal.service.util.Base64;
import com.smartdocs.smartportal.web.rest.util.SSConstants;
@Service
public class SapAdminFuncitonsAPIImpl implements AdminFuncitonsAPI {
	public Logger log = Logger.getLogger("" + SapAdminFuncitonsAPIImpl.class);
	@Inject
	SystemsService systemsService;
	@Inject
	RepositoryService repositoryService;
	@Inject
	GridFsTemplate gridFsTemplate;

	@Override
	public HttpServletResponse setPutCert(HttpServletRequest request, HttpServletResponse response)
			throws IOException, Exception {
		DBObject metaData = new BasicDBObject();

		String authId = request.getParameter(SSConstants.authId);
		String systemId = null;
		if (Strings.isNullOrEmpty(authId)) {
			response.setStatus(500, "System with id '" + authId.substring(authId.indexOf("=") + 1));
			response.setHeader(SSConstants.xErrorDescription,
					"System with id '" + authId.substring(authId.indexOf("=") + 1));
			return response;
		} else {
			String[] authSplit = authId.split(",", 0);
			System.out.println(authSplit[0]);
			String systemName = authSplit[0];
			systemId = systemName.substring(authId.indexOf("=") + 1);
		

		
			gridFsTemplate.store(request.getInputStream(), systemId, metaData).getId().toString();
		

			return response;
		}

	}

	@Override
	public HttpServletResponse serverInfo(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String pVersion = request.getParameter(SSConstants.pVersion);
		String contRep = request.getParameter(SSConstants.contRep);
		String resultAs = request.getParameter(SSConstants.resultAs);
		log.info("contRep=" + contRep + ",pVersion=" + pVersion + ",resultAs=" + resultAs);

		Repository repository = null;
		Systems systemsInfo = null;

		if (pVersion == null || pVersion.equalsIgnoreCase("")) {
			response.setStatus(400, "Unknown function or unknown parameter");
			response.setHeader(SSConstants.xErrorDescription, "Unknown function or unknown parameter");
			return response;
		}

		repository = repositoryService.findOneByrepositoryId(contRep);

		if (null == repository) {
			log.warn("Repository '" + contRep + "' is not Exist on Copmany Name ");
			response.setStatus(500, "Repository '" + contRep + "' is not Exist on Copmany Name '");
			response.setHeader(SSConstants.xErrorDescription,
					"Repository '" + contRep + "' is not Exist on Copmany Name '");
			return response;
		} else {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'at'HH:mm:ss");
			Date d = new Date(System.currentTimeMillis());
			response.setContentType("text/html");
			String temp[] = dateFormat.format(d).split("at");
			if (resultAs == null || resultAs.equalsIgnoreCase("") || resultAs.equalsIgnoreCase("ascii")) {
				response.getWriter().println(
						"serverStatus=\"running\";serverVendorId=\"actiprocess\";serverVersion=\"4500\";serverBuild=\"1.0\";serverTime=\""
								+ temp[1].trim() + "\";serverDate=\"" + temp[0].trim()
								+ "\";serverStatusDescription=\"actiproces\";pVersion=" + repository.getpVersion()
								+ ";CRLF");
				if (contRep != null && !contRep.equalsIgnoreCase("")) {
					response.getWriter()
							.println("contRep=\"" + repository.getName() + "\";contRepDescription=\""
									+ repository.getDescription() + "\";contRepStatus=\"" + "running" + "\";pVersion=\""
									+ repository.getpVersion() + "\";CRLF");
				} else {
					List<Repository> list = repositoryService.findOneByRepositoryId(repository.getRepositoryId());
					Repository rep = null;
					for (int i = 0; i < list.size(); i++) {
						rep = (Repository) list.get(i);
						response.getWriter()
								.println("contRep=\"" + rep.getName() + "\";contRepDescription=\""
										+ rep.getDescription() + "\";contRepStatus=\"" + "running" + "\";pVersion=\""
										+ rep.getpVersion() + "\";CRLF");
					}
				}
			} else if (resultAs.equalsIgnoreCase("html")) {
				response.getWriter().println("<html><body><table><tr><td>serverStatus=\"running\";</td></tr>"
						+ "<td><tr>serverVendorId=\"actiprocess\";</td></tr>"
						+ "<td><tr>serverVersion=\"4500\";</td></tr>" + "<td><tr>serverBuild=\"1.0\";</td></tr>"
						+ "<td><tr>serverTime=\"" + temp[1].trim() + "\";</td></tr>" + "<td><tr>serverDate=\""
						+ temp[0].trim() + "\";</td></tr>" + "<td><tr>serverStatusDescription=\"actiproces\";</td></tr>"
						+ "<td><tr>pVersion=" + repository.getpVersion() + ";</td></tr>CRLF");
			}
			response.setStatus(200, "OK");
		}

		return response;
	}

}
