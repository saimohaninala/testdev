package com.smartdocs.smartportal.service.impl;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import javax.inject.Inject;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.MultipartStream;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.ctc.wstx.util.StringUtil;
import com.google.api.client.util.Strings;
import com.mongodb.gridfs.GridFSDBFile;
import com.smartdocs.smartportal.config.JHipsterProperties;
import com.smartdocs.smartportal.constant.SmartstoreConstant;
import com.smartdocs.smartportal.domain.AzureBlobSettings;
import com.smartdocs.smartportal.domain.Document;
import com.smartdocs.smartportal.domain.GdriveToken;
import com.smartdocs.smartportal.domain.MimeType;
import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.domain.Systems;
import com.smartdocs.smartportal.repository.AzureBlobSettingsRepository;
import com.smartdocs.smartportal.repository.GdriveTokenRepository;
import com.smartdocs.smartportal.repository.RepositoryRepository;
import com.smartdocs.smartportal.service.AcceessFuncitonsAPI;
import com.smartdocs.smartportal.service.DocumentService;
import com.smartdocs.smartportal.service.MimeTypeService;
import com.smartdocs.smartportal.service.RepositoryService;
import com.smartdocs.smartportal.service.SystemsService;
import com.smartdocs.smartportal.service.util.MetaDataXMLParser;
import com.smartdocs.smartportal.web.rest.AdminAccessFunctions;
import com.smartdocs.smartportal.web.rest.util.SSConstants;
import com.smartdocs.smartportal.web.rest.util.SSUtils;
@Service
public class SapAcceessFuncitonsAPIImple implements AcceessFuncitonsAPI {
	private final Logger log = LoggerFactory.getLogger(SapAcceessFuncitonsAPIImple.class);
	@Inject
	MimeTypeService mimetypeService;
	@Inject
	RepositoryRepository repositoryRepository;
	@Inject
	DocumentService documentService;
	@Inject
	SystemsService systemsService;
	@Inject
	GdriveTokenRepository gdriveTokenRepository;
	@Inject
	JHipsterProperties jHipsterProperties;
	@Inject
	GridFsTemplate gridFsTemplate;
	@Inject
	RepositoryService repositoryService;
	@Inject
	AzureBlobSettingsRepository azureBlobSettingsRepository;
	

	@Override
	public HttpServletResponse Info(HttpServletRequest request, HttpServletResponse response) {

		System.out.println("INFO METHOD CALLING");

		// TODO Auto-generated method stub
		String pVersion = request.getParameter(SSConstants.pVersion);
		String contRep = request.getParameter(SSConstants.contRep);
		String docId = request.getParameter(SSConstants.docId);
		String compId = request.getParameter(SSConstants.compId);
		String resultAs = request.getParameter(SSConstants.resultAs);
		String accessMode = request.getParameter(SSConstants.accessMode);
		String docProt = request.getParameter("docProt");
		String authId = request.getParameter(SSConstants.authId);
		String expiration = request.getParameter(SSConstants.expiration);
		String secKey = request.getParameter(SSConstants.secKey);
		String sp = request.getParameter("sp");
		String appendedtext = null;
		String systemId = null;

		if (authId != null) {
			String[] authSplit = authId.split(",", 0);
			System.out.println(authSplit[0]);
			String systemName = authSplit[0];
			systemId = systemName.substring(authId.indexOf("=") + 1);
		}
		String serverName = "Apache Tomcat";
		if (accessMode == null)
			accessMode = "r";

		log.info("pVersion='" + pVersion + "' contRep='" + contRep + "' docId='" + docId + "' resultAs='" + resultAs
				+ "' accessMode='" + accessMode + "' authId='" + authId + "' expiration='" + expiration + "' seckey='"
				+ secKey + "'");
		ServletOutputStream out = null;
		try {

			// Checking for Mandatory files
			if (contRep == null || pVersion == null || docId == null || accessMode == null || authId == null
					|| expiration == null || secKey == null) {
				log.info("Mandatory fields missiing");
				response.setStatus(401, "Breach of security");// "Mandatory
																// fields
																// missiing");
				response.setHeader(SSConstants.xErrorDescription, contRep + "$" + pVersion + "$" + docId + "$"
						+ accessMode + "$" + authId + "$" + expiration + "$" + secKey);
				return response;
			}

			Repository repository = repositoryRepository.findOne(systemId+contRep);

			Systems systems = systemsService.findOneBySystemId(systemId);
		    String id=	systems.getSystemId();
			
			
			
			String PRE_CONDICTION_STATUS = null;
			
			if (sp == null) {
				GridFSDBFile file = gridFsTemplate.findOne(new Query(Criteria.where("filename").is(id)));
				InputStream certificatein = file.getInputStream();

				// CHECKING PRE-CONDITIONS
				PRE_CONDICTION_STATUS = SSUtils.checkPre_Conditions(authId, secKey,
						contRep + docId + accessMode + authId + expiration, expiration, repository.getRepositoryId(),
						repository, systems,certificatein);
			}
			if (PRE_CONDICTION_STATUS != null) {
				response.setStatus(401, PRE_CONDICTION_STATUS);
				response.setHeader(SSConstants.xErrorDescription, PRE_CONDICTION_STATUS);
				return response;
			}

			String boundary = System.currentTimeMillis() + "";

			Document document = documentService.findOneByDocId(docId);

			if (document != null) {

				// Creation Date
				ZonedDateTime zoneDateTime = document.getCreatedDateTime();
				LocalDate d1 = zoneDateTime.toLocalDate();
				String XdateC = String.valueOf(d1);
				LocalTime lt = zoneDateTime.toLocalTime();
				String minutes = String.valueOf(lt);
				String[] split = minutes.split("\\.");
				String XtimeC = split[0];

				// Modification Date

				ZonedDateTime zoneDateTimeM = document.getLastUpdatedDateTime();
				LocalDate d2 = zoneDateTime.toLocalDate();
				String XdateM = String.valueOf(d2);
				LocalTime lt2 = zoneDateTime.toLocalTime();
				String minutes1 = String.valueOf(lt);
				String[] split1 = minutes.split("\\.");
				String XtimeM = split[0];

				response.setHeader("Content-Type", "multipart/form-data; boundary=" + boundary);
				response.setHeader("boundary", boundary + "");
				if (serverName.startsWith("Apache Tomcat")) {
					// response.setHeader("Content-Length: ",
					// documentInfo.getContentLength());
				} else
					response.setHeader("Content-Length", Integer.toString(document.getContentLength()));
				response.setHeader("X-dateC", XdateC);
				response.setHeader("X-timeC", XtimeC);
				response.setHeader("X-dateM", XdateM);
				response.setHeader("X-timeM", XtimeM);
				response.setHeader("X-numberComps", "1");
				response.setHeader("X-contentRep", contRep);
				response.setHeader("X-docId", document.getArchiveDocId());
				response.setHeader("X-docStatus", "online");
				response.setHeader("X-pVersion", pVersion);

				System.out.println("Header Part Completed");

				List<Document> documentList = null;
				if (compId == null || compId.equals("")) {

					documentList = documentService.findOneByArchiveDocIdAndArchiveId(docId, contRep);
					log.info("document List size='" + documentList.size() + "' : docId='" + docId + "'");
				} else {
					documentList = documentService.findOneByArchiveDocIdAndArchiveId(docId, contRep);
					System.out.println("document found.........");
					// response.setHeader("X-numberComps",
					// documentList.size()+"");
					log.info("document List size='" + documentList.size() + "'  : docId='" + docId + "' :compId='"
							+ document.getFileName() + "'");
				}
				out = response.getOutputStream();
				if (resultAs == null || resultAs.equals("") || resultAs.equalsIgnoreCase("ascii"))
					out.println("\n--" + boundary);
				for (int j = 0; j < documentList.size(); j++) {
					document = (Document) documentList.get(j);
					String repString = null;
					if (resultAs == null || resultAs.equals("") || resultAs.equalsIgnoreCase("ascii")) {
						repString = "";
						out.println("");
						out.println(("\nContent-Type: "
								+ (document.getContentType() != null && document.getContentType().trim().length() > 0
										&& !document.getContentType().equalsIgnoreCase("null")
												? document.getContentType() : " ")));
						out.println(("\nX-Content-Length: " + ((document.getContentLength()))));
						out.println(("\nX-compId: " + document.getFileName()));
						out.println(("\nX-compDateC: " + XdateC));
						out.println(("\nX-compTimeC: " + XtimeC));
						out.println(("\nX-compDateM: " + XdateM));
						out.println(("\nX-compTimeM: " + XtimeM));
						out.println(("\nX-contentRep: " + document.getArchiveId()));
						out.println(("\nX-numberComps: " + documentList.size()));
						out.println(("\nX-docId: " + document.getArchiveDocId()));
						out.println(("\nX-compStatus:" + " online"));
						out.println(("\nX-pVersion: " + pVersion + "\n"));
						out.print(("--" + boundary));

						System.out.println("running in for loop");

					} else {
						repString = "<tr><td>boundary</td><td>" + boundary + "</td><td>Content-Type</td><td>"
								+ document.getContentType() + "<tr><td>charset</td><td>" + ""
								+ "<tr><td>version</td><td>" + "" + "<tr><td>Content-Length</td><td>"
								+ document.getContentLength() + "<tr><td>X-compId</td><td>" + document.getFileName()
								+ "<tr><td>X-compDateC</td><td>" + XdateC + "<tr><td>X-compTimeC</td><td>" + XtimeC
								+ "<tr><td>X-compDateM</td><td>" + XdateM + "<tr><td>X-compTimeM</td><td>" + XtimeM
								+ "<tr><td>X-compStatus</td><td>" + "online" + "<tr><td>X-pVersion</td><td>" + pVersion
								+ "</td></tr>";
						out.println(repString);
						/* response.getWriter().println( repString ); */
					}

				} // for Loop

				if (resultAs == null || resultAs.equals("") || resultAs.equalsIgnoreCase("ascii"))
					out.print("--");
				response.setStatus(200, "OK");

				System.out.println("INFO METHOD ENDING");

			} else {
				response.setStatus(404, "OK");
				response.setHeader(SSConstants.xErrorDescription, "Document with id=" + docId + " is not found");
			}
		} catch (Exception exp) {
			exp.printStackTrace();
			log.warn(exp.getMessage());
			response.setStatus(500, exp.getMessage());
			response.setHeader(SSConstants.xErrorDescription, exp.getMessage());
		} finally {
			if (null != out) {
				try {
					out.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return response;

	}

	@Override
	public HttpServletResponse Get(HttpServletRequest request, HttpServletResponse response) throws IOException {

		// TODO Auto-generated method stub
	
		
		System.out.println("<<<<<<<<<<<<<<<<<<<<<<GET METHOD CALLING>>>>>>>>>>>>>>>>>>>>>>>");
		
		String pVersion= request.getParameter(SSConstants.pVersion);
		String contRep= request.getParameter(SSConstants.contRep);
		String docId= request.getParameter(SSConstants.docId);
		String compId= request.getParameter(SSConstants.compId);
		String resultAs= request.getParameter(SSConstants.resultAs);
		String fromOffset= request.getParameter(SSConstants.fromOffset);
		String toOffset= request.getParameter(SSConstants.toOffset);
		String accessMode= request.getParameter(SSConstants.accessMode);		
		String authId= request.getParameter(SSConstants.authId);
		
		System.out.println("authId "+authId);
		String expiration= request.getParameter(SSConstants.expiration);
		String secKey= request.getParameter(SSConstants.secKey);
		String sp= request.getParameter("sp");
		String contentType = request.getContentType();		
		String serverName ="Apache Tomcat";
		MimeType info=null;
		String systemId=null;
		if(authId !=null) {
				String[] authSplit=		authId.split(",",0);
				String systemName=	authSplit[0];
				systemId=	systemName.substring(authId.indexOf("=")+1 );
			}
		
		List<MimeType> mimetypesList=	mimetypeService.findMimeTypes();
		Map<String,String> mimeTypeMap=new HashMap<String,String>();
		for(MimeType mimetypes:mimetypesList) {
			mimeTypeMap.put(mimetypes.getMimeType(), mimetypes.getFileExtension());
		}
		
		OutputStream out=null;
		
		try{
			//Checking for mandatory fields
		if(contRep==null || docId==null || accessMode==null||authId==null||expiration==null||secKey==null){
			log.info("Mandatory fields are missing");
			response.setStatus(401, "Breach of security");// "Mandatory fields are missing");
			response.setHeader(SSConstants.xErrorDescription, contRep+"$"+docId+"$"+accessMode+"$"+authId+"$"+expiration+"$"+secKey);
			return response;
		}else if(contRep.equalsIgnoreCase("null") || docId.equalsIgnoreCase("null") || accessMode.equalsIgnoreCase("null")||authId.equalsIgnoreCase("null")||expiration.equalsIgnoreCase("null")||secKey.equalsIgnoreCase("null")){
			log.info("Mandatory fields are missing");
			response.setStatus(401,  "Breach of security");//"Mandatory fields are missing");
			response.setHeader(SSConstants.xErrorDescription, contRep+"$"+docId+"$"+accessMode+"$"+authId+"$"+expiration+"$"+secKey);
			return response;
		}
		
		
			//Checking the content repository exist or not
			Repository	repository=repositoryRepository.findOne((systemId+contRep).toUpperCase());
			Systems systems=	systemsService.findOneBySystemId(systemId);
			
			if(null==repository){
				log.info("Repository not found with name="+contRep);
				response.setStatus(404, "Repository not found with name="+contRep);
				response.setHeader(SSConstants.xErrorDescription, "Repository not found with name="+contRep);
				return response;
			}
			
			if(null == systems) {
				log.info("Systems not found with name="+systemId);
				response.setStatus(404, "Repository not found with name="+systemId);
				response.setHeader(SSConstants.xErrorDescription, "Repository not found with name="+systemId);
				return response;
			}
			
		String id=	systems.getSystemId();
			
			String PRE_CONDICTION_STATUS=null;
		    if( sp ==null) {
				GridFSDBFile file = gridFsTemplate.findOne(new Query(Criteria.where("filename").is(id)));
				InputStream certificatein = file.getInputStream();

			PRE_CONDICTION_STATUS=SSUtils.checkPre_Conditions( authId, secKey, contRep+docId+accessMode+authId+expiration, expiration, repository.getRepositoryId(), repository,systems,certificatein);
		    
		     if(PRE_CONDICTION_STATUS!=null){					
				
		    	response.setStatus(401,PRE_CONDICTION_STATUS);
				response.setHeader(SSConstants.xErrorDescription,PRE_CONDICTION_STATUS);
				return response;
		     }
		    }

		Document doc=	documentService.findOneByDocId(docId);
		
		String boundary=System.currentTimeMillis()+"";
		if(doc==null){
			
			log.info("Dcoument not found Docid="+docId);
			response.setStatus(404, "Dcoument not found");
			response.setHeader(SSConstants.xErrorDescription, "Dcoument not found Docid="+docId);
			return response;
		}else{
			response.setDateHeader("Date", System.currentTimeMillis());
			response.setLocale(new Locale("English"));
			response.setContentType("multipart/form-data; boundary="+boundary);//, always multipart/form-data
			String resString="boundary="+boundary+"Content-type=multipart/form-data&boundary=&Content-Length="+doc.getContentLength()+"&X-dateTimeC="+doc.getCreatedDateTime()+"&X-dateTimeM="+doc.getLastUpdatedDateTime()+
					"&X-contRep="+contRep+"&X-docId="+doc.getDocId()+"&X-docStatus="+doc.getStatus()+"&X-pVersion="+pVersion;
					
					log.info(resString);
		}
		
			//Retrieving the components from DB	
			Document document=documentService.findOneByDocId(docId);
			log.info(document.getDocId()+" "+document.getFileName()+" "+document.getContentType()+" "+document.getContentLength()+document.getLastUpdatedDateTime()+" "+document.getCreatedDateTime());
			String fileExtension="";
			try{
				
				System.out.println("document.getContentType()"+document.getContentType());
				info=mimetypeService.findOneByMimeType(document.getContentType());
				
				//System.out.println("info.getFileExtension()"+info.getFileExtension());
				 if(document.getFileName().indexOf(".")==-1 && null!=info)
					fileExtension="."+info.getFileExtension();
			
			
			
			}catch(Exception exp){
				exp.printStackTrace();
			}
			
			if(isNullOrEmpty(repository.getStorageType())){
				AzureBlobSettings azureBlobSettings=azureBlobSettingsRepository.findOne(SmartstoreConstant.SS_AZURE_BLOBSTORAGE);
				repository.setStorageType(SmartstoreConstant.SS_AZURE_BLOBSTORAGE);
				repository.setClientId(azureBlobSettings.getAccountName());
				repository.setClientSecret(azureBlobSettings.getAccountKey());
			}
			
			
			AdminAccessFunctions accessFunctions=AdminAccessFunctions.getInstance(repository, gdriveTokenRepository,jHipsterProperties);
			byte[] in=null;
			if(null!=document.getResourceId()){
				
				in=accessFunctions.downFile(document.getResourceId());
			
				String resString="boundary="+boundary+"Content-type=multipart/form-data&boundary=&Content-Length="+document.getContentLength()+"&X-dateTimeC="+document.getCreatedDateTime()+"&X-dateTimeM="+document.getLastUpdatedDateTime()+
						"&X-compId="+document.getFileName()+"&X-contRep="+contRep+"&X-docId="+document.getArchiveDocId()+"&X-docStatus="+document.getStatus()+"&X-pVersion="+pVersion;
						
						log.info(resString);
				int to=-1,from=0;
				if(toOffset!=null && fromOffset!=null ){
				try{ to=Integer.parseInt(toOffset);}catch (Exception e) {}
				try{ from=Integer.parseInt(fromOffset);}catch (Exception e) {}
				}
				
				if(to==-1 && from==0){
					response.setContentLength(in.length);
					log.info(document.getDocId()+":"+" Content Length='"+in.length+"'");
					 if(serverName.startsWith("Apache Tomcat")){
						// response.setHeader("Content-Length: ", in.length+"");
					 }else
					response.setHeader("content-length", in.length+"");
					response.setHeader("x-content-length", in.length+"");
				//	response.setContentType((info.getMimeType().trim().length()==0?";":info.getMimeType()));
					
					
					if(info==null ) {
						if(document.getContentType()==null ) {
							
							System.out.println("document.getContentType( if block)????????????????????????????????"+document.getContentType());
							
							response.setContentType("application/octet-stream");
						
						}else {
							
							System.out.println("document.getContentType( else block)????????????????????????????????"+document.getContentType());
							response.setContentType(document.getContentType().trim());
						}
					}
					else {
					response.setContentType(info.getMimeType().trim());
					}
					
					
					//response.setContentType((info.getMimeType().trim().length()==0?";":info.getMimeType()));
					 out=response.getOutputStream();
					 for (int j = 0; j < in.length; j++) {
						 out.write(in[j]);	
					}
					out.flush();
					log.info("***************************************send the link");
				
				}else{
					
					log.info("*********************"+in.length+" from="+from+" to="+to);
					
					if(to>in.length){
						to=in.length-1;
						log.info("**************to changed "+to);
					}
					if(from>0 && from<to){
						to=to+1;
					}
					

					//response.setContentType((0==info.getMimeType().trim().length()?";":info.getMimeType()));
					
					if(info==null ) {
						if(document.getContentType()==null ) {
							response.setContentType("application/octet-stream");
						
						}else {
							response.setContentType(document.getContentType().trim());
						}
					}
					else {
					response.setContentType(info.getMimeType().trim());
					}
					
					
					
					 out=response.getOutputStream();
					
					for (int j = from; j <=to; j++){
						try{out.write(in[j]);}catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					
					response.setContentLength(to-from);	
					log.info(document.getDocId()+":"+" Content Length='"+(to-from)+"'");
					log.info(document.getDocId()+":"+" Content Length='"+in.length+"'");
					 if(serverName.startsWith("Apache Tomcat")){
						 //response.setHeader("Content-Length: ", to-from+"");
					 }else
					response.setHeader("content-length", to-from+"");
					response.setHeader("x-content-length",to-from+"");
					
				}
			
			response.setStatus(200,"OK");
			
			}else{
				response.setStatus(404,"Not Found");
				response.setHeader(SSConstants.xErrorDescription, "document not found Docid="+docId+" compid="+compId);
			}
	}
	catch(Exception exp){
			log.info(exp.getMessage());
			exp.printStackTrace();
			response.setStatus(500,exp.getMessage()); 
			response.setHeader(SSConstants.xErrorDescription, exp.getMessage());
		}finally{
			if(null!=out)
			out.close();
		}
		return response;
		
	
	}

	@Override
	public HttpServletResponse DocGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HttpServletResponse Append(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HttpServletResponse Update(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HttpServletResponse Delete(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HttpServletResponse Search(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HttpServletResponse AttrSearch(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HttpServletResponse Mcreate(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HttpServletResponse createDoc(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub

		String pVersion = request.getParameter(SSConstants.pVersion);
		String contentRepository = request.getParameter(SSConstants.contRep);
		String archiveDocId = request.getParameter(SSConstants.docId);
		String compId = request.getParameter(SSConstants.compId);
		String docProt = request.getParameter("docProt");
		String accessMode = request.getParameter(SSConstants.accessMode);
		String authId = request.getParameter(SSConstants.authId); // CN=SD2 Like
																	// That

		String expiration = request.getParameter(SSConstants.expiration);
		String secKey = request.getParameter(SSConstants.secKey);
		String contentType = request.getContentType();
		int contentLength = request.getContentLength();
		URL destUrl = null;
		String sp = request.getParameter("sp");
		String cContentType = null;

		String appendedtext = null;
		Repository repository = null;
		int cContentLength = 0;
		String fileName = null;
		Document document = null;
		String systemId = null;
		if (authId != null) {
			String[] authSplit = authId.split(",", 0);
			System.out.println(authSplit[0]);
			String systemName = authSplit[0];

			systemId = systemName.substring(authId.indexOf("=") + 1);

		}

		Map<String, String> mimeTypes = getMimeTypes();
		
		String id=	systemId;
		
		if (request.getMethod().equalsIgnoreCase("PUT")) {

			System.out.println("<<<<<<<<<<<<<<<<<<<PUT METHOD CALLING>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

			int status = 201;

			if (Strings.isNullOrEmpty(contentRepository) || Strings.isNullOrEmpty(archiveDocId)
					|| Strings.isNullOrEmpty(compId) || contentLength == 0 || Strings.isNullOrEmpty(accessMode)
					|| Strings.isNullOrEmpty(authId) || Strings.isNullOrEmpty(expiration)
					|| Strings.isNullOrEmpty(pVersion) || Strings.isNullOrEmpty(secKey)) {
				response.setStatus(401, "Breach of security");// "Unknown
																// function or
																// unknown
																// parameter");
				response.setHeader(SSConstants.xErrorDescription,
						contentRepository + "$" + pVersion + "$" + archiveDocId + "$" + compId + "$" + accessMode + "$"
								+ authId + "$" + expiration + "$" + secKey);
				return response;
			}

			if (Strings.isNullOrEmpty(docProt)) {
				appendedtext = contentRepository + archiveDocId + compId + accessMode + authId + expiration;
			} else {
				appendedtext = contentRepository + archiveDocId + compId + docProt + accessMode + authId + expiration;
			}
			
			repository = repositoryRepository.findOne(systemId+contentRepository);
			Systems systems = systemsService.findOneBySystemId(systemId.toUpperCase());
			
			String PRE_CONDICTION_STATUS = null;
			if (sp == null) {
				GridFSDBFile file = gridFsTemplate.findOne(new Query(Criteria.where("filename").is(id)));
				InputStream certificatein = file.getInputStream();


				PRE_CONDICTION_STATUS = SSUtils.checkPre_Conditions(authId, secKey, appendedtext, expiration,
						repository.getRepositoryId(), repository, systems,certificatein);

				if (PRE_CONDICTION_STATUS != null) {

					response.setStatus(401, PRE_CONDICTION_STATUS);
					response.setHeader(SSConstants.xErrorDescription, PRE_CONDICTION_STATUS);
					return response;
				}
			}

			List<Document> docs = documentService.findOneBySystemIdAndArchiveIdAndArchiveDocId(systemId,
					contentRepository, archiveDocId);
			if (docs.size() > 0) {

				response.setStatus(403, "Document already exists ");
				response.setHeader(SSConstants.xErrorDescription, "Document already exists");
				return response;
			} else {

				if (!Strings.isNullOrEmpty(compId)) {

					fileName = compId;
					if (fileName.indexOf(".") == -1) {
						System.out.println("filename+++++++++++++");
						if (!Strings.isNullOrEmpty(contentType)) {
							System.out.println("contentType-----"+contentType);
							String extension = mimeTypes.get(contentType.toLowerCase());
							if(!Strings.isNullOrEmpty(extension))
								System.out.println("extension-----"+extension);
							fileName = fileName + "." + extension;
							System.out.println("filename-----"+fileName);
						}

					}

				}
				
				System.out.println("contentType-----"+contentType);	
				Document doc = new Document();

				List<String> components = new ArrayList<String>();
				components.add(compId);

				doc.setId(archiveDocId);
				doc.setArchiveId(contentRepository);
				doc.setArchiveDocId(archiveDocId);
				doc.setCreatedDateTime(ZonedDateTime.now());
				doc.setSystemId(systemId);
				doc.setLastUpdatedDateTime(ZonedDateTime.now());
				doc.setFileName(compId);
				doc.setFileType(contentType);
				doc.setComponents(components);
				doc.setContentType(contentType);
				doc.setContentLength(contentLength);
				System.out.println("fileName in doc============"+fileName);
				
				if(isNullOrEmpty(repository.getStorageType())){
					AzureBlobSettings azureBlobSettings=azureBlobSettingsRepository.findOne(SmartstoreConstant.SS_AZURE_BLOBSTORAGE);
					repository.setStorageType(SmartstoreConstant.SS_AZURE_BLOBSTORAGE);
					repository.setClientId(azureBlobSettings.getAccountName());
					repository.setClientSecret(azureBlobSettings.getAccountKey());
				}
				
				AdminAccessFunctions accessFunctions=AdminAccessFunctions.getInstance(repository, gdriveTokenRepository,jHipsterProperties);				
				String resourceId = accessFunctions.uploadFile(SSConstants.SS_SMARTSTORE_NAME, systemId,
						contentRepository, archiveDocId, fileName, contentType,
						IOUtils.toByteArray(request.getInputStream()));
				if (!Strings.isNullOrEmpty(resourceId)) {
					doc.setResourceId(resourceId);
					status = 201;
					documentService.save(doc);
					response.setStatus(status, "Document Created");

				} else {
					status = 500;
					response.setStatus(status, "Document Not Created");

				}

			}

			return response;

		} else if (request.getMethod().equalsIgnoreCase("POST")) {
			int status = 201;

			System.out.println("<<<<<<<<<<<<<<<<<<<POST METHOD CALLING>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

			// if any manadatory parameters are missing then return error code
			if (contentRepository == null || archiveDocId == null || contentLength == 0 || accessMode == null
					|| authId == null || expiration == null || pVersion == null || secKey == null) {
				response.setStatus(401, "Breach of security");// "Unknown
																// function or
																// unknown
																// parameter");
				response.setHeader(SSConstants.xErrorDescription, contentRepository + "$" + pVersion + "$"
						+ archiveDocId + "$" + accessMode + "$" + authId + "$" + expiration + "$" + secKey);
				return response;
			}
			if (docProt == null) {
				appendedtext = contentRepository + archiveDocId + accessMode + authId + expiration;
			} else {
				appendedtext = contentRepository + archiveDocId + docProt + accessMode + authId + expiration;
			}

			repository =repositoryRepository.findOne(systemId+contentRepository);

			Systems systems = systemsService.findOneBySystemId(systemId);

			String PRE_CONDICTION_STATUS = null;
			if (sp == null) {
				GridFSDBFile file = gridFsTemplate.findOne(new Query(Criteria.where("filename").is(id)));
				InputStream certificatein = file.getInputStream();



				System.out.println(authId + "........." + secKey + "......." + appendedtext + "......." + expiration
						+ "........" + repository.getRepositoryId() + "......" + systems.getSystemId());
				PRE_CONDICTION_STATUS = SSUtils.checkPre_Conditions(authId, secKey, appendedtext, expiration,
						repository.getRepositoryId(), repository, systems,certificatein);

				if (PRE_CONDICTION_STATUS != null) {

					response.setStatus(401, PRE_CONDICTION_STATUS);
					response.setHeader(SSConstants.xErrorDescription, PRE_CONDICTION_STATUS);
					return response;
				}
			}

			repository = repositoryRepository.findOne(systemId+contentRepository);

			List<Document> docs = documentService.findOneBySystemIdAndArchiveIdAndArchiveDocId(systemId,
					contentRepository, archiveDocId);
			if (docs.size() > 0) {

				response.setStatus(403, "Document already exists ");
				response.setHeader(SSConstants.xErrorDescription, "Document already exists");
				return response;
			} else {

				document = new Document();

				
				  List<String> components=new ArrayList<String>();
				  components.add(compId);
				 
				document.setId(archiveDocId);
				document.setArchiveId(contentRepository);
				document.setArchiveDocId(archiveDocId);
				document.setCreatedDateTime(ZonedDateTime.now());
				document.setSystemId(systemId);
				document.setLastUpdatedDateTime(ZonedDateTime.now());
				document.setFileName(compId);
				document.setFileType(contentType);
				 document.setComponents(components);

				document.setContentLength(contentLength);

			}

			// parsing body
			int boundaryIndex = contentType.indexOf("boundary=");
			// doc.setContentType(contentType.substring(0, boundaryIndex));

			byte[] boundary = (contentType.substring(boundaryIndex + 9)).getBytes();

			System.out.println("boundary:" + new String(boundary));

			MultipartStream mps = new MultipartStream(request.getInputStream(), boundary);

			boolean nextPart = mps.skipPreamble();

			System.out.println("nextPart:" + nextPart);

			int count = 0;

			/* starting main while loop */
			while (nextPart) {
				try {
					String headers = mps.readHeaders();
					log.info("Headers: " + headers);
					StringTokenizer st = new StringTokenizer(headers, "\n");

					while (st.hasMoreTokens()) {
						StringTokenizer subst = new StringTokenizer(st.nextToken(), ":");
						String s1 = subst.nextToken();
						if (s1.equalsIgnoreCase("X-compId")) {
							compId = subst.nextToken().trim();
							document.setFileName(compId);
							List<String> components = new ArrayList<String>();
							components.add(compId);
							document.setComponents(components);

							log.info("X-compId is  " + compId);
						}

						else if (s1.equalsIgnoreCase("Content-Type")) {
							cContentType = subst.nextToken().trim();
							document.setContentType(cContentType);

						}

						else if (s1.equalsIgnoreCase("Content-Length")) {
							cContentLength = Integer.parseInt(subst.nextToken().trim());
							log.info("Content-Length is " + cContentLength);
						} else if (s1.equalsIgnoreCase("X-docId")) {

							log.info("X-docId " + subst.nextToken().trim());
						}
					}

					ByteArrayOutputStream data = new ByteArrayOutputStream();
					mps.readBodyData(data);

					if (compId != null) {
						if (compId.indexOf(".") != -1) {
							log.info(compId);
							fileName = compId;
						} else {
							if (cContentType != null) {
								fileName = compId;
								try {

									if (compId.indexOf(".") == -1 && mimeTypes.containsKey(cContentType.toLowerCase()))
										fileName = compId + "." + mimeTypes.get(cContentType.toLowerCase());
									log.info("new fileName===" + fileName + "\n"
											+ mimeTypes.get(cContentType.toLowerCase()));

								}

								catch (Exception e) {
									e.printStackTrace();
								}
							} else {
								fileName = compId;
							}

						}
						InputStream inputstream = new ByteArrayInputStream(data.toByteArray());

						if(isNullOrEmpty(repository.getStorageType())){
							AzureBlobSettings azureBlobSettings=azureBlobSettingsRepository.findOne(SmartstoreConstant.SS_AZURE_BLOBSTORAGE);
							repository.setStorageType(SmartstoreConstant.SS_AZURE_BLOBSTORAGE);
							repository.setClientId(azureBlobSettings.getAccountName());
							repository.setClientSecret(azureBlobSettings.getAccountKey());
						}
						
						
						AdminAccessFunctions accessFunctions=AdminAccessFunctions.getInstance(repository, gdriveTokenRepository,jHipsterProperties);						
						String resourceId = accessFunctions.uploadFile(SSConstants.SS_SMARTSTORE_NAME, systemId,
								contentRepository, archiveDocId, fileName, cContentType,
								IOUtils.toByteArray(inputstream));
						if (!Strings.isNullOrEmpty(resourceId)) {
							document.setResourceId(resourceId);
							status = 201;

							documentService.save(document);
							response.setStatus(status, "Document Created");

						} else {
							status = 500;
							response.setStatus(status, "Document Not Created");

						}
					}

					nextPart = mps.readBoundary();
				} catch (Exception e) {
					nextPart = false;
				}

			} /* ending main while loop */

			return response;

		} else {
			log.warn("Unknown function or unknown parameter");
			response.setStatus(400, "Unknown function or unknown parameter");
			response.setHeader(SSConstants.xErrorDescription, "Unknown function or unknown parameter");
			return response;
		}
		// return response;
	}

	public Map<String, String> getMimeTypes() {
		List<MimeType> mimetypesList = mimetypeService.findMimeTypes();

		Map<String, String> mimeTypeMap = new HashMap<String, String>();

		for (MimeType mimetypes : mimetypesList) {
			mimeTypeMap.put(mimetypes.getMimeType(), mimetypes.getFileExtension());
		}
		return mimeTypeMap;
	}

	public HttpServletResponse Restore(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String compId = null;		
		String authId = request.getParameter(SSConstants.authId); // CN=SD2 Like				
		Repository repository=null;
		MimeType info=null;
		String systemId = null;
		int contentLength=0;
		String docId=null;
		String archiveId=null;
		String fileName=null;
		String fileType=null;
		File resouceId=null;
		if (authId != null) {
			String[] authSplit = authId.split(",", 0);
			System.out.println(authSplit[0]);
			String systemName = authSplit[0];
			systemId = systemName.substring(authId.indexOf("=") + 1);
		}
			
		InputStream in = request.getInputStream();
		int i = -1;	
		StringBuilder builder = new StringBuilder();
		i = in.read();
		while (i != -1) {
			builder.append(((char) i + "").trim());		
			i = in.read();
		}		
		in.close();
			
		MetaDataXMLParser parser=new MetaDataXMLParser();
		List<Document> docdatalist = parser.getParseXMLData(builder.toString());	
		for (Document documentInfo : docdatalist) {
			log.info("docId----"+documentInfo.getArchiveDocId());
			docId=	documentInfo.getArchiveDocId();
			archiveId=documentInfo.getArchiveId();
			fileName="data";
			fileType=documentInfo.getFileType();
			log.info("fileType----"+fileType+"---archiveId----"+archiveId);
		
		List<Document> documentList = null;
		
		repository=repositoryService.findOneByrepositoryId(archiveId);
		if(null==repository){
			log.info("Repository not found with name="+repository);
			response.setStatus(404, "Repository not found with name="+repository);
			response.setHeader(SSConstants.xErrorDescription, "Repository not found with name="+repository);
			return response;
		}
			
		List<MimeType> mimetypesList=	mimetypeService.findMimeTypes();	
		Map<String,String> mimeTypeMap=new HashMap<String,String>();	
		for(MimeType mimetypes:mimetypesList) {
			mimeTypeMap.put(mimetypes.getMimeType(), mimetypes.getFileExtension());
		}
		String fileExtension="";		
		if(mimeTypeMap.get(fileType)==null) {
			info=mimetypeService.findOneByfileExtension(fileType);	
			 if(fileName.indexOf(".")==-1 && null!=info)
				fileExtension="."+info.getFileExtension();
		}else {	
			info=mimetypeService.findOneByMimeType(fileType);
			if(fileName.indexOf(".")==-1 && null!=info)
				fileExtension="."+info.getFileExtension();
		}
		if(isNullOrEmpty(repository.getStorageType())){
			AzureBlobSettings azureBlobSettings=azureBlobSettingsRepository.findOne(SmartstoreConstant.SS_AZURE_BLOBSTORAGE);
			repository.setStorageType(SmartstoreConstant.SS_AZURE_BLOBSTORAGE);
			repository.setClientId(azureBlobSettings.getAccountName());
			repository.setClientSecret(azureBlobSettings.getAccountKey());
		}
		AdminAccessFunctions accessFunctions=AdminAccessFunctions.getInstance(repository, gdriveTokenRepository, jHipsterProperties);
		long ins=0;
		File file=accessFunctions.getDocPath(archiveId,systemId, docId, fileName, repository.getRootPath());		
		if(file.exists()) {
				ins=accessFunctions.getDocContentLength(archiveId,systemId,docId,fileName,repository.getRootPath());	
				compId=fileName;
				resouceId=accessFunctions.getDocResourcePath(archiveId,systemId, docId, fileName, repository.getRootPath());
		}else {			
			 file=accessFunctions.getDocPath(archiveId,systemId, docId, fileName+"."+fileType, repository.getRootPath());
			 compId=fileName+"."+fileType;
			 ins=accessFunctions.getDocContentLength(archiveId,systemId,docId,fileName+"."+fileType,repository.getRootPath());	
			 resouceId=accessFunctions.getDocResourcePath(archiveId,systemId, docId, fileName+"."+fileType, repository.getRootPath());
		}
			
		if(0!=ins){							
			contentLength=(int) ins;			
		}
		 
		
		Document document=	documentService.findOneByDocId(docId);
		if(document==null){
			document=new Document();
			System.out.println("creating document-----");
			List<String> components=new ArrayList<String>();
			components.add(compId);		
			document.setId(docId);
			document.setArchiveId(archiveId);
			document.setArchiveDocId(docId);
			document.setCreatedDateTime(ZonedDateTime.now());
			document.setSystemId(systemId);
			document.setLastUpdatedDateTime(ZonedDateTime.now());
			document.setFileName(compId);
			document.setFileType(info.getFileExtension());
			document.setComponents(components);
			if(fileType==null) {
				document.setContentType("application/octet-stream");
			}
			else {
				document.setContentType(info.getMimeType());
			}	
			document.setContentLength(contentLength);
			document.setResourceId(resouceId.toString());		
			log.info("Dcoument created Docid="+docId);
			documentService.save(document);
			
		}else{
			documentList = documentService.findOneByArchiveDocIdAndArchiveId(docId, archiveId);		
			log.info("document exists and document List size='" + documentList.size() + "'  : docId='" + docId + "' :compId='"
					+ document.getFileName() + "'");
		}			
		
	}
		response.setStatus(201, "Dcoument created");			
	return response;
	}
    public  boolean isNullOrEmpty(String str) {
        if(str != null && !str.isEmpty())
            return false;
        return true;
    }

}
