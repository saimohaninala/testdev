package com.smartdocs.smartportal.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.smartdocs.smartportal.domain.BrandSetting;
import com.smartdocs.smartportal.repository.BrandSettingRepository;
import com.smartdocs.smartportal.service.BrandService;

@Service
public class BrandServiceImpl implements BrandService{
  
	@Inject
	BrandSettingRepository brandrepo;
	@Override
	public BrandSetting save(BrandSetting brand) {
		return brandrepo.save(brand);
	}
	@Override
	public List<BrandSetting> get() {
		return brandrepo.findAll();
	}

}
