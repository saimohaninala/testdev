package com.smartdocs.smartportal.service.impl;


import java.util.Optional;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.smartdocs.smartportal.domain.Company;
import com.smartdocs.smartportal.repository.CompanyRepository;

import com.smartdocs.smartportal.service.CompanyService;

@Service
public class CompanyServiceImpl implements CompanyService {
	
	
	 @Inject
	 private  CompanyRepository  companyRepository;
	

	@Override
	public Company save(Company company) {
		
		System.out.println(company.getName());
		
		// TODO Auto-generated method stub
	Company result=	companyRepository.save(company);
		return result;
	}

	@Override
	public Page<Company> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		 Page<Company> result=companyRepository.findAll(pageable);
		return result;
	}

	/*@Override
	public Company findOne(String id) {
		// TODO Auto-generated method stub
		Company result = companyRepository.findOne(id);
	        return result;
	}*/

	@Override
	public void delete(String id) {
		// TODO Auto-generated method stub
		companyRepository.delete(id);
	}

	@Override
	public Optional<Company> findOneByName(String name) {
		// TODO Auto-generated method stub
		return companyRepository.findOneByName(name);
	}

	@Override
	public Company findOneByCompanyId(String companyId) {
		Company result= companyRepository.findOneByCompanyId(companyId);
     return result; 
	}

	@Override
	public Company findOne(String id) {
		// TODO Auto-generated method stub
	Company company=	companyRepository.findOne(id);
		return company;
	}

}
