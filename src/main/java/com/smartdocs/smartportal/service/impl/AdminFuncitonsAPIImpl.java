/*package com.smartdocs.smartportal.service.impl;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.smartdocs.smartportal.domain.Certificate;


import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.domain.Systems;
import com.smartdocs.smartportal.service.AdminFuncitonsAPI;
import com.smartdocs.smartportal.service.CertificateService;
import com.smartdocs.smartportal.service.CompanyService;
import com.smartdocs.smartportal.service.RepositoryService;
import com.smartdocs.smartportal.service.SystemsService;
import com.smartdocs.smartportal.service.util.Base64;
import com.smartdocs.smartportal.smartstore.FileSystemAPI;
import com.smartdocs.smartportal.web.rest.AdminAccessFunctions;
import com.smartdocs.smartportal.web.rest.util.SSConstants;
@Service
public class AdminFuncitonsAPIImpl implements AdminFuncitonsAPI  {
	public Logger log = Logger.getLogger("" + AdminFuncitonsAPIImpl.class);
	
	@Inject
	RepositoryService repositoryService;
	@Inject
	SystemsService systemsService;
	@Inject
	CompanyService companyService;
	@Inject
	CertificateService certificateService;


	
	
	
	
	
	@Override
	public HttpServletResponse setPutCert(HttpServletRequest request, HttpServletResponse response) throws IOException, Exception{
		
		Repository repository=null;
		Certificate cert =new Certificate();
		String contRep = request.getParameter(SSConstants.contRep); //means A7 or R5
		String pVersion = request.getParameter(SSConstants.pVersion);//means 0046
		String authId = request.getParameter(SSConstants.authId);//means CN=ID3 (ID3 is System Name)
		
		String systemId=null;
		String systemName=null;
		if(authId !=null) {
			String[] authSplit=	authId.split(",",0);
			System.out.println(authSplit[0]);
			 systemName=	authSplit[0];
			systemId=systemName.substring(authId.indexOf("=")+1 );
		}
		authId=systemName;
		
		if (contRep == null || pVersion == null || authId == null) {				
			response.setStatus(401, "Unknown function or unknown parameter");
			response.setHeader(SSConstants.xErrorDescription, contRep+"$"+pVersion+"$"+authId);
			return response;
		}
		if (contRep != null && !contRep.equalsIgnoreCase("")) {
			repository=repositoryService.findOneByrepositoryId(contRep);
		}
		if(repository ==null ){
			 	response.setStatus(404, "Repository not found with name="+contRep);
				response.setHeader(SSConstants.xErrorDescription, "Repository not found with name="+contRep);
				return response;
		}
		
		Systems system=	systemsService.findOneBySystemId(systemId);
		 
		if(system==null){
			response.setStatus(500, "System with id '" + authId.substring(authId.indexOf("=")+1));
				response.setHeader(SSConstants.xErrorDescription, "System with id '" + authId.substring(authId.indexOf("=")+1));
				return response;
		}
		
	//	if(system.getAuthId().equalsIgnoreCase(authId))
		
		if(system.getAuthId().equalsIgnoreCase(systemName)) {
			
			System.out.println("%%%%%%%%%%%%%%%%%%%%"+system.getSystemId());
			
			cert.setSystemId(system.getSystemId());
			
			
			InputStream inStream = request.getInputStream();
			
			CertificateFactory factory = CertificateFactory.getInstance("X.509");
			Collection collect = factory.generateCertificates(inStream);
			X509Certificate certificate = null;
			
			for (Iterator i = collect.iterator(); i.hasNext();) {
				certificate = (X509Certificate) i.next();
			}
			
			try {
				certificate.checkValidity();
				if (certificate != null) {
					cert.setValidFrom(certificate.getNotBefore().toString());
					cert.setValidTo(certificate.getNotAfter().toString());
					cert.setPublicKey(Base64.encode(certificate.getEncoded()));					
					cert.setAlgorithm(certificate.getSigAlgName());						
				}
				
			} catch (Exception ex) {
				log.warn("Exception occured in SSAdminApiAction while getting certificate "+ex.getMessage());
				ex.printStackTrace();
				response.setStatus(406, ex.getMessage());
				response.setHeader(SSConstants.xErrorDescription, ex.getMessage());
				return response;
			}
			System.out.println(":::::::::::::::::::::::::"+Base64.encode(certificate.getEncoded()));
			InputStream is=new ByteArrayInputStream(certificate.getEncoded());
			AdminAccessFunctions adminFunctions=AdminAccessFunctions.getInstance( authId, contRep, repository.getStorageType());
			if(repository.getStorageType().equalsIgnoreCase("gdrive")) {
			String gdriveResourceId=adminFunctions.putCert(repository.getRepositoryId(),authId, is,null);
			
			if(gdriveResourceId.contentEquals("Certificate already uploaded to the Gdrive")) {
				
			}else {
				
			}
			
			System.out.println("gdriveResourceId:"+gdriveResourceId);
			
			
			}else {
	    	adminFunctions.putCert(repository.getRepositoryId(),authId, is,repository.getRootPath());
			}
			system.setCertificate(Base64.encode(certificate.getEncoded()));
			systemsService.save(system);
			
		
		//	certificateService.save(cert);
		
		}
		
		
		return response;
	}
	
	
	
	
	
	
	
	
	
	
	
	

	@Override
	public HttpServletResponse serverInfo(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String pVersion = request.getParameter(SSConstants.pVersion);
		String contRep = request.getParameter(SSConstants.contRep);
		String resultAs = request.getParameter(SSConstants.resultAs);
		log.info("contRep="+contRep+",pVersion="+pVersion+",resultAs="+resultAs);
		
		Repository repository = null;
		Systems systemsInfo=null;
		
		if (pVersion == null || pVersion.equalsIgnoreCase("")) {
			response.setStatus(400, "Unknown function or unknown parameter");
			response.setHeader(SSConstants.xErrorDescription,"Unknown function or unknown parameter");
			return response;
		} 
       
		
		repository = repositoryService.findOneByrepositoryId(contRep);
		
		if (null == repository) {
			log.warn("Repository '" + contRep+ "' is not Exist on Copmany Name ");
			response.setStatus(500, "Repository '" + contRep+ "' is not Exist on Copmany Name '");
			response.setHeader(SSConstants.xErrorDescription,"Repository '" + contRep+ "' is not Exist on Copmany Name '");
			return response;
		} else {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'at'HH:mm:ss");
			Date d = new Date(System.currentTimeMillis());
			response.setContentType("text/html");
			String temp[] = dateFormat.format(d).split("at");
			if (resultAs == null || resultAs.equalsIgnoreCase("")
					|| resultAs.equalsIgnoreCase("ascii")) {
				response
						.getWriter()
						.println(
								"serverStatus=\"running\";serverVendorId=\"actiprocess\";serverVersion=\"4500\";serverBuild=\"1.0\";serverTime=\""
										+ temp[1].trim()
										+ "\";serverDate=\""
										+ temp[0].trim()
										+ "\";serverStatusDescription=\"actiproces\";pVersion="
										+ repository.getpVersion()
										+ ";CRLF");
				if (contRep != null && !contRep.equalsIgnoreCase("")) {
					response.getWriter().println(
							"contRep=\"" + repository.getName()
									+ "\";contRepDescription=\""
									+ repository.getDescription()
									+ "\";contRepStatus=\"" + "running"
									+ "\";pVersion=\""
									+ repository.getpVersion() + "\";CRLF");
				} else {
					List<Repository> list =  repositoryService.findOneByRepositoryId(
							repository.getRepositoryId());
					Repository rep = null;
					for (int i = 0; i < list.size(); i++) {
						rep = (Repository) list.get(i);
						response.getWriter().println(
								"contRep=\"" + rep.getName()
										+ "\";contRepDescription=\""
										+ rep.getDescription()
										+ "\";contRepStatus=\"" + "running"
										+ "\";pVersion=\"" + rep.getpVersion()
										+ "\";CRLF");
					}
				}
			} else if (resultAs.equalsIgnoreCase("html")) {
				response
						.getWriter()
						.println(
								"<html><body><table><tr><td>serverStatus=\"running\";</td></tr>"
										+ "<td><tr>serverVendorId=\"actiprocess\";</td></tr>"
										+ "<td><tr>serverVersion=\"4500\";</td></tr>"+ "<td><tr>serverBuild=\"1.0\";</td></tr>"
										+ "<td><tr>serverTime=\""
										+ temp[1].trim()
										+ "\";</td></tr>"
										+ "<td><tr>serverDate=\""
										+ temp[0].trim()
										+ "\";</td></tr>"
										+ "<td><tr>serverStatusDescription=\"actiproces\";</td></tr>"
										+ "<td><tr>pVersion="
										+ repository.getpVersion()
										+ ";</td></tr>CRLF");
			}
			response.setStatus(200, "OK");
		}

		return response;
	}
	}



									
	*/