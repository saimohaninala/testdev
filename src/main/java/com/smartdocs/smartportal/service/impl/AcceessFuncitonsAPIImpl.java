/*package com.smartdocs.smartportal.service.impl;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;

import javax.inject.Inject;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.MultipartStream;
import org.apache.commons.fileupload.MultipartStream.MalformedStreamException;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import com.smartdocs.smartportal.domain.Company;
import com.smartdocs.smartportal.domain.Document;
import com.smartdocs.smartportal.domain.MimeType;
import com.smartdocs.smartportal.domain.MimeType;
import com.smartdocs.smartportal.domain.Repository;
import com.smartdocs.smartportal.domain.Systems;
import com.smartdocs.smartportal.service.AcceessFuncitonsAPI;
import com.smartdocs.smartportal.service.DocumentService;
import com.smartdocs.smartportal.service.MimeTypeService;
import com.smartdocs.smartportal.service.RepositoryService;
import com.smartdocs.smartportal.service.SystemsService;
import com.smartdocs.smartportal.web.rest.AdminAccessFunctions;
import com.smartdocs.smartportal.web.rest.util.SSConstants;
import com.smartdocs.smartportal.web.rest.util.SSUtils;

import java.io.ByteArrayOutputStream;

@Service
public class AcceessFuncitonsAPIImpl implements AcceessFuncitonsAPI {
	@Inject
	MimeTypeService mimetypeService;
	@Inject
	RepositoryService repositoryService;
	@Inject
	DocumentService documentService;
	@Inject
	SystemsService systemsService;
	
	private final Logger log = LoggerFactory.getLogger(AcceessFuncitonsAPIImpl.class);

	@Override
	public HttpServletResponse Info(HttpServletRequest request, HttpServletResponse response) {
	     System.out.println("INFO METHOD CALLING");
		
		// TODO Auto-generated method stub
		String pVersion= request.getParameter(SSConstants.pVersion);
		String contRep= request.getParameter(SSConstants.contRep);
		String docId= request.getParameter(SSConstants.docId);
		String compId= request.getParameter(SSConstants.compId);
		String resultAs= request.getParameter(SSConstants.resultAs);
		String accessMode= request.getParameter(SSConstants.accessMode);
		String docProt = request.getParameter("docProt");
		String authId= request.getParameter(SSConstants.authId);
		String expiration= request.getParameter(SSConstants.expiration);
		String secKey= request.getParameter(SSConstants.secKey);
		String sp= request.getParameter("sp");
		String appendedtext=null;
		String systemId=null;
		
			
		
			if(authId !=null) {
				String[] authSplit=		authId.split(",",0);
				System.out.println(authSplit[0]);
			String systemName=	authSplit[0];
			
			systemId=	systemName.substring(authId.indexOf("=")+1 );
				
			}
		String serverName ="Apache Tomcat";
		if(accessMode==null)
			accessMode="r";
		
		log.info("pVersion='"+pVersion+"' contRep='"+contRep+"' docId='"+docId+"' resultAs='"+resultAs+"' accessMode='"+accessMode+"' authId='"+authId+"' expiration='"+expiration+"' seckey='"+secKey+"'");
		ServletOutputStream out=null;
		try{
			
			//Checking for Mandatory files
		if(contRep==null || pVersion==null ||	docId==null || accessMode==null||authId==null||expiration==null||secKey==null){
			log.info("Mandatory fields missiing");
			response.setStatus(401, "Breach of security");//"Mandatory fields missiing");			
			response.setHeader(SSConstants.xErrorDescription, contRep+"$"+pVersion+"$"+docId+"$"+accessMode+"$"+authId+"$"+expiration+"$"+secKey);
			return response;
		}
		
		
		Repository	repository=repositoryService.findOneByrepositoryId(contRep);
	
		Systems systems=	systemsService.findOneBySystemId(systemId);
		String PRE_CONDICTION_STATUS=null;
		System.out.println("sp============="+sp);
		if( sp ==null) {
		//CHECKING PRE-CONDITIONS
		 PRE_CONDICTION_STATUS=SSUtils.checkPre_Conditions( authId, secKey,contRep+docId+accessMode+authId+expiration,expiration,repository.getRepositoryId(),repository,systems);
		 }
		if(PRE_CONDICTION_STATUS!=null){					
			response.setStatus(401,PRE_CONDICTION_STATUS);
			response.setHeader(SSConstants.xErrorDescription, PRE_CONDICTION_STATUS);
			return response;
		}
		
		String boundary=System.currentTimeMillis()+"";

		Document document=	documentService.findOneByDocId(docId);
		
		if(document!=null){
			
			//Creation Date
			ZonedDateTime zoneDateTime = document.getCreatedDateTime();
			 LocalDate d1 = zoneDateTime.toLocalDate();
			 String XdateC=String.valueOf(d1);
			 LocalTime lt=    zoneDateTime.toLocalTime();
			 String minutes= String.valueOf(lt);
			 String[] split= minutes.split("\\.");
			 String XtimeC= split[0];
			 
			 //Modification Date
			 
			 ZonedDateTime zoneDateTimeM = document.getLastUpdatedDateTime();
			 LocalDate d2 = zoneDateTime.toLocalDate();
			 String XdateM=String.valueOf(d2);
			 LocalTime lt2=    zoneDateTime.toLocalTime();
			 String minutes1= String.valueOf(lt);
			 String[] split1= minutes.split("\\.");
			 String XtimeM= split[0];
			
			 response.setHeader("Content-Type","multipart/form-data; boundary="+boundary);
			 response.setHeader("boundary", boundary+"");
			 if(serverName.startsWith("Apache Tomcat")){
				 //response.setHeader("Content-Length: ", documentInfo.getContentLength());
			 }else
			 response.setHeader("Content-Length", Integer.toString(document.getContentLength()));
			 response.setHeader("X-dateC", XdateC);
			 response.setHeader("X-timeC", XtimeC);
			 response.setHeader("X-dateM", XdateM);
			 response.setHeader("X-timeM", XtimeM);
			 response.setHeader("X-numberComps", "1");
			 response.setHeader("X-contentRep", contRep);
			 response.setHeader("X-docId", document.getArchiveDocId());
			 response.setHeader("X-docStatus","online");
			 response.setHeader("X-pVersion", pVersion);
			
			System.out.println("Header Part Completed");
			
			 List<Document> documentList=null;
			 if(compId==null || compId.equals("")){
					
					documentList= documentService.findOneByArchiveDocIdAndArchiveId(docId, contRep) ;
					 log.info("document List size='"+documentList.size()+"' : docId='"+docId+"'");
				}else{
					documentList=  documentService.findOneByArchiveDocIdAndArchiveId(docId, contRep) ;
					System.out.println("document found.........");
				//	response.setHeader("X-numberComps", documentList.size()+"");
					log.info("document List size='"+documentList.size()+"'  : docId='"+docId+"' :compId='"+document.getFileName()+"'");
				}
			 out=response.getOutputStream();
			 if(resultAs==null || resultAs.equals("") || resultAs.equalsIgnoreCase("ascii"))
			out.println("\n--"+boundary);
			 for(int j=0;j<documentList.size();j++){
				 document=(Document) documentList.get(j);
				 String repString=null; 
				 if(resultAs==null || resultAs.equals("") || resultAs.equalsIgnoreCase("ascii")){
						repString="";
						out.println("");
						out.println(("\nContent-Type: "+(document.getContentType()!=null &&document.getContentType().trim().length()>0 && !document.getContentType().equalsIgnoreCase("null")?document.getContentType():" "))); 
						out.println(("\nX-Content-Length: "+((document.getContentLength()))));
						out.println(("\nX-compId: "+document.getFileName()));
						out.println(("\nX-compDateC: "+XdateC));
						out.println(("\nX-compTimeC: "+XtimeC));
						out.println(("\nX-compDateM: "+XdateM));
						out.println(("\nX-compTimeM: "+XtimeM));
						out.println(("\nX-contentRep: "+document.getArchiveId()));
						out.println(("\nX-numberComps: "+documentList.size()));
						out.println(("\nX-docId: "+document.getArchiveDocId()));
						out.println(("\nX-compStatus:"+" online"));
						out.println(("\nX-pVersion: "+pVersion+"\n"));
						out.print(("--"+boundary));
					
						System.out.println("running in for loop");
						
						
				 }else {
					 repString="<tr><td>boundary</td><td>"+boundary+"</td><td>Content-Type</td><td>"+document.getContentType()+"<tr><td>charset</td><td>"+""+"<tr><td>version</td><td>"+""+"<tr><td>Content-Length</td><td>"+ 
							 document.getContentLength()+"<tr><td>X-compId</td><td>"+document.getFileName()+"<tr><td>X-compDateC</td><td>"+
							 XdateC+"<tr><td>X-compTimeC</td><td>"+XtimeC+"<tr><td>X-compDateM</td><td>"+XdateM+
					 		"<tr><td>X-compTimeM</td><td>"+XtimeM+"<tr><td>X-compStatus</td><td>"+"online"+"<tr><td>X-pVersion</td><td>"+pVersion+"</td></tr>";
							 out.println(repString);
							 response.getWriter().println( repString );
				 }
			 
			 }//for Loop
			
			 if(resultAs==null || resultAs.equals("") || resultAs.equalsIgnoreCase("ascii"))
			 out.print("--");
			 response.setStatus(200, "OK");
			 
			 System.out.println("INFO METHOD ENDING");
			
		}
		else{
			response.setStatus(404, "OK");
			response.setHeader(SSConstants.xErrorDescription, "Document with id="+docId+" is not found");
		}
		}catch(Exception exp){
			exp.printStackTrace();
			log.warn(exp.getMessage());
			response.setStatus(500,exp.getMessage());
			response.setHeader(SSConstants.xErrorDescription, exp.getMessage());
		}finally{
			if(null!=out){
				try{
					out.close();
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
		} 
		return response;
	}

	@Override
	public HttpServletResponse Get(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
	
		
		System.out.println("<<<<<<<<<<<<<<<<<<<<<<GET METHOD CALLING>>>>>>>>>>>>>>>>>>>>>>>");
		
		String pVersion= request.getParameter(SSConstants.pVersion);
		String contRep= request.getParameter(SSConstants.contRep);
		String docId= request.getParameter(SSConstants.docId);
		String compId= request.getParameter(SSConstants.compId);
		String resultAs= request.getParameter(SSConstants.resultAs);
		String fromOffset= request.getParameter(SSConstants.fromOffset);
		String toOffset= request.getParameter(SSConstants.toOffset);
		String accessMode= request.getParameter(SSConstants.accessMode);		
		String authId= request.getParameter(SSConstants.authId);
		
		System.out.println("authId "+authId);
		String expiration= request.getParameter(SSConstants.expiration);
		String secKey= request.getParameter(SSConstants.secKey);
		String sp= request.getParameter("sp");
		String contentType = request.getContentType();		
		String serverName ="Apache Tomcat";
		MimeType info=null;
		String systemId=null;
		if(authId !=null) {
				String[] authSplit=		authId.split(",",0);
				String systemName=	authSplit[0];
				systemId=	systemName.substring(authId.indexOf("=")+1 );
			}
		
		List<MimeType> mimetypesList=	mimetypeService.findMimeTypes();
		Map<String,String> mimeTypeMap=new HashMap<String,String>();
		for(MimeType mimetypes:mimetypesList) {
			mimeTypeMap.put(mimetypes.getMimeType(), mimetypes.getFileExtension());
		}
		
		OutputStream out=null;
		
		try{
			
		
			//Checking for mandatory fields
		if(contRep==null || docId==null || accessMode==null||authId==null||expiration==null||secKey==null){
			log.info("Mandatory fields are missing");
			response.setStatus(401, "Breach of security");// "Mandatory fields are missing");
			response.setHeader(SSConstants.xErrorDescription, contRep+"$"+docId+"$"+accessMode+"$"+authId+"$"+expiration+"$"+secKey);
			return response;
		}else if(contRep.equalsIgnoreCase("null") || docId.equalsIgnoreCase("null") || accessMode.equalsIgnoreCase("null")||authId.equalsIgnoreCase("null")||expiration.equalsIgnoreCase("null")||secKey.equalsIgnoreCase("null")){
			log.info("Mandatory fields are missing");
			response.setStatus(401,  "Breach of security");//"Mandatory fields are missing");
			response.setHeader(SSConstants.xErrorDescription, contRep+"$"+docId+"$"+accessMode+"$"+authId+"$"+expiration+"$"+secKey);
			return response;
		}
		
		
			//Checking the content repository exist or not
			Repository	repository=repositoryService.findOneByrepositoryId(contRep);
			Systems systems=	systemsService.findOneBySystemId(systemId);
			
			if(null==repository){
				log.info("Repository not found with name="+contRep);
				response.setStatus(404, "Repository not found with name="+contRep);
				response.setHeader(SSConstants.xErrorDescription, "Repository not found with name="+contRep);
				return response;
			}
			
			if(null == systems) {
				log.info("Systems not found with name="+systemId);
				response.setStatus(404, "Repository not found with name="+systemId);
				response.setHeader(SSConstants.xErrorDescription, "Repository not found with name="+systemId);
				return response;
			}
			
			String PRE_CONDICTION_STATUS=null;
		    if( sp ==null) {
			PRE_CONDICTION_STATUS=SSUtils.checkPre_Conditions( authId, secKey, contRep+docId+accessMode+authId+expiration, expiration, repository.getRepositoryId(), repository,systems);
		    
		     if(PRE_CONDICTION_STATUS!=null){					
				
		    	response.setStatus(401,PRE_CONDICTION_STATUS);
				response.setHeader(SSConstants.xErrorDescription,PRE_CONDICTION_STATUS);
				return response;
		     }
		    }

		Document doc=	documentService.findOneByDocId(docId);
		
		String boundary=System.currentTimeMillis()+"";
		if(doc==null){
			
			log.info("Dcoument not found Docid="+docId);
			response.setStatus(404, "Dcoument not found");
			response.setHeader(SSConstants.xErrorDescription, "Dcoument not found Docid="+docId);
			return response;
		}else{
			response.setDateHeader("Date", System.currentTimeMillis());
			response.setLocale(new Locale("English"));
			response.setContentType("multipart/form-data; boundary="+boundary);//, always multipart/form-data
			String resString="boundary="+boundary+"Content-type=multipart/form-data&boundary=&Content-Length="+doc.getContentLength()+"&X-dateTimeC="+doc.getCreatedDateTime()+"&X-dateTimeM="+doc.getLastUpdatedDateTime()+
					"&X-contRep="+contRep+"&X-docId="+doc.getDocId()+"&X-docStatus="+doc.getStatus()+"&X-pVersion="+pVersion;
					
					log.info(resString);
		}
		
			//Retrieving the components from DB	
			Document document=documentService.findOneByDocId(docId);
			log.info(document.getDocId()+" "+document.getFileName()+" "+document.getContentType()+" "+document.getContentLength()+document.getLastUpdatedDateTime()+" "+document.getCreatedDateTime());
			String fileExtension="";
			try{
				
				System.out.println("document.getContentType()"+document.getContentType());
				info=mimetypeService.findOneByMimeType(document.getContentType());
				
				//System.out.println("info.getFileExtension()"+info.getFileExtension());
				 if(document.getFileName().indexOf(".")==-1 && null!=info)
					fileExtension="."+info.getFileExtension();
			
			
			
			}catch(Exception exp){
				exp.printStackTrace();
			}
			
			
			AdminAccessFunctions accessFunctions=AdminAccessFunctions.getInstance(authId, contRep, repository.getStorageType());
			byte[] in=null;
			if(null!=document.getResourceId()){
				String resourceid=document.getResourceId().substring(document.getResourceId().indexOf(":")+1);

				in=accessFunctions.getComponentInputStreamUsingResourceIdExt(resourceid,fileExtension,docId,document.getFileName());
				System.out.println("byte[]>>>>>>>>>>>>>>>>"+in);
				
				
			}else {
			
			
			in=accessFunctions.getDocInputStreamURL(contRep,docId,document.getFileName()+fileExtension,repository.getRootPath());
			
			if(null == in){
				in=accessFunctions.getDocInputStreamURL(contRep,docId,document.getFileName(),repository.getRootPath());	
			}
			}
			if(null!=in){			
				String resString="boundary="+boundary+"Content-type=multipart/form-data&boundary=&Content-Length="+document.getContentLength()+"&X-dateTimeC="+document.getCreatedDateTime()+"&X-dateTimeM="+document.getLastUpdatedDateTime()+
						"&X-compId="+document.getFileName()+"&X-contRep="+contRep+"&X-docId="+document.getDocId()+"&X-docStatus="+document.getStatus()+"&X-pVersion="+pVersion;
						
						log.info(resString);
				int to=-1,from=0;
				if(toOffset!=null && fromOffset!=null ){
				try{ to=Integer.parseInt(toOffset);}catch (Exception e) {}
				try{ from=Integer.parseInt(fromOffset);}catch (Exception e) {}
				}
				
				if(to==-1 && from==0){
					response.setContentLength(in.length);
					log.info(document.getDocId()+":"+" Content Length='"+in.length+"'");
					 if(serverName.startsWith("Apache Tomcat")){
						// response.setHeader("Content-Length: ", in.length+"");
					 }else
						 response.setHeader("content-length", in.length+"");
					response.setHeader("x-content-length", in.length+"");
				//	response.setContentType((info.getMimeType().trim().length()==0?";":info.getMimeType()));
					
					
					if(info==null ) {
						if(document.getContentType()==null ) {
							response.setContentType("application/octet-stream");
						
						}else {
							response.setContentType(document.getContentType().trim());
						}
					}
					else {
					response.setContentType(info.getMimeType().trim());
					}
					
					
					//response.setContentType((info.getMimeType().trim().length()==0?";":info.getMimeType()));
					 out=response.getOutputStream();
					 for (int j = 0; j < in.length; j++) {
						 out.write(in[j]);	
					}
					out.flush();
					log.info("***************************************send the link");
				
				}else{
					
					log.info("*********************"+in.length+" from="+from+" to="+to);
					
					if(to>in.length){
						to=in.length-1;
						log.info("**************to changed "+to);
					}
					if(from>0 && from<to){
						to=to+1;
					}
					

				//	response.setContentType((0==info.getMimeType().trim().length()?";":info.getMimeType()));
					
					if(info==null ) {
						if(document.getContentType()==null ) {
							response.setContentType("application/octet-stream");
						
						}else {
							response.setContentType(document.getContentType().trim());
						}
					}
					else {
					response.setContentType(info.getMimeType().trim());
					}
					
					
					
					 out=response.getOutputStream();
					
					for (int j = from; j <=to; j++){
						try{out.write(in[j]);}catch (Exception e) {
							e.printStackTrace();
						}
					}
					
					
					response.setContentLength(to-from);	
					log.info(document.getDocId()+":"+" Content Length='"+(to-from)+"'");
					log.info(document.getDocId()+":"+" Content Length='"+in.length+"'");
					 if(serverName.startsWith("Apache Tomcat")){
						 //response.setHeader("Content-Length: ", to-from+"");
					 }else
					response.setHeader("content-length", to-from+"");
					response.setHeader("x-content-length",to-from+"");
					
				}
			
			response.setStatus(200,"OK");
			
			}else{
				response.setStatus(404,"Not Found");
				response.setHeader(SSConstants.xErrorDescription, "document not found Docid="+docId+" compid="+compId);
			}
	}
	catch(Exception exp){
			log.info(exp.getMessage());
			exp.printStackTrace();
			response.setStatus(500,exp.getMessage()); 
			response.setHeader(SSConstants.xErrorDescription, exp.getMessage());
		}finally{
			if(null!=out)
			out.close();
		}
		return response;
		
	}

	@Override
	public HttpServletResponse DocGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		
		log.info("******************Request for DOCGET********************");
		//Reading the parameters from Request
		String pVersion= request.getParameter(SSConstants.pVersion);
		String compId= request.getParameter(SSConstants.compId);
		String contRep= request.getParameter(SSConstants.contRep);
		String docId= request.getParameter(SSConstants.docId);
		String resultAs= request.getParameter(SSConstants.resultAs);
		String accessMode= request.getParameter(SSConstants.accessMode);		
		String authId= request.getParameter(SSConstants.authId);
		String expiration= request.getParameter(SSConstants.expiration);
		String secKey= request.getParameter(SSConstants.secKey);
		String sp= request.getParameter("sp");
		response.setContentType("binary/octet-stream");
		ServletOutputStream out=null;
		String systemId=null;
			if(authId !=null) {
					String[] authSplit=		authId.split(",",0);
					System.out.println(authSplit[0]);
					String systemName=	authSplit[0];
					systemId=	systemName.substring(authId.indexOf("=")+1 );
			}
		
		log.info("pVersion='"+pVersion+"' contRep='"+contRep+"' docId='"+docId+"' resultAs='"+resultAs+"' accessMode='"+accessMode+"' authId='"+authId+"' expiration='"+expiration+"' seckey='"+secKey+"'");
		try{		
			//Checking for mandatory fields
		if(contRep==null && docId==null || accessMode==null||authId==null||expiration==null||secKey==null){
			response.setStatus(401, "Breach of security");// "mandatory fields missiing");
			response.setHeader(SSConstants.xErrorDescription, contRep+"$"+docId+"$"+accessMode+"$"+authId+"$"+expiration+"$"+secKey);
			return response;
		}
		//Checking the content repository exist or not
		Repository repository=repositoryService.findOneByrepositoryId(contRep);
		Systems systems=systemsService.findOneBySystemId(systemId);
		
		if(null==repository){
			log.warn("Repository not found"+contRep);
			response.setStatus(404, "Repository not found");
			response.setHeader(SSConstants.xErrorDescription, "Repository not found"+contRep);
			return response;
		}
		System.out.println("sp============="+sp);
		if(null!=sp && sp.equalsIgnoreCase("true")){
			
		}else{
	//CHECKING PRE-CONDITIONS
	String PRE_CONDICTION_STATUS=SSUtils.checkPre_Conditions( authId, secKey, contRep+docId+accessMode+authId+expiration,expiration,contRep,repository,systems);
	log.info("PRE_CONDICTION_STATUS=="+PRE_CONDICTION_STATUS);
	if(PRE_CONDICTION_STATUS!=null){					
		response.setStatus(401,PRE_CONDICTION_STATUS);
		response.setHeader(SSConstants.xErrorDescription, PRE_CONDICTION_STATUS);
		return response;
	}
		}
		
		
		AdminAccessFunctions accessFunctions=AdminAccessFunctions.getInstance( authId, contRep, repository.getStorageType());
		MimeType info=null;
		List<MimeType> mimetypesList=	mimetypeService.findMimeTypes();
		Map<String,String> mimeTypeMap=new HashMap<String,String>();
		for(MimeType mimetypes:mimetypesList) {
			mimeTypeMap.put(mimetypes.getMimeType(), mimetypes.getFileExtension());
		}
		
		
		//Checking the documen is exist or not
		
		Document document =documentService.findOneByDocId(docId);
		 System.out.println(document.getArchiveDocId());
		String boundary=System.currentTimeMillis()+"";
		if(document==null){
			log.info("Dcoument not found docId="+docId);
			response.setStatus(404, "Dcoument not found docId="+docId);
			response.setHeader(SSConstants.xErrorDescription, contRep+"$"+docId+"$"+accessMode+"$"+authId+"$"+expiration+"$"+secKey);
			return response;
		}else{
			response.setDateHeader("Date", System.currentTimeMillis());
			response.setLocale(new Locale("English"));
			
		
			//response.setHeader("X-dateC",document.getCreatedDateTime());// YYYY-MM-DD Creation date (UTC)			
			//response.setHeader("X-dateM",document.getLastUpdatedDateTime());// YYYY-MM-DD Last changed on [date] (UTC)
		
			
			response.setHeader("X-contRep",contRep);// String Content repository
			response.setHeader("X-docId",document.getArchiveDocId());// String Document ID
			response.setHeader("X-docStatus","online");// String Status
			response.setHeader("X-pVersion",pVersion);// String Version
			
			String resString="boundary="+boundary+"&Content-type=multipart/form-data&boundary=&Content-Length="+document.getContentLength()+"&X-dateC="+document.getCreatedDateTime()+"&X-dateM="+document.getLastUpdatedDateTime()+
			"&X-contRep="+contRep+"&X-docId="+document.getArchiveDocId()+"&X-docStatus="+document.getStatus()+"&X-pVersion="+pVersion;
			
			log.info("resString="+resString);
		}
		//Retrieving the components from DB	
		 List<Document> documentList=null;
		documentList= documentService.findOneByArchiveDocIdAndArchiveId(docId, contRep) ;
		log.info("component size ='"+documentList.size()+"' docId='"+docId+"'compId='"+document.getFileName()+"'");
		
		out=response.getOutputStream();
		out.println(("--"+boundary));
		
		if(documentList.size()==0){
			
			out.print(("--"+boundary));
		}
			
		
		for(int i=0;i<documentList.size();i++){		
			 document=(Document) documentList.get(i);
			log.info(document.getArchiveDocId()+" "+document.getFileName()+" "+document.getArchiveId()+" "+document.getContentType()+" "+document.getContentLength()+" "+document.getCreatedDateTime()+" "+document.getLastUpdatedDateTime());
		   System.out.println(document.getArchiveDocId());
			
		   String fileExtension="";
			try{
				info=mimetypeService.findOneByMimeType(document.getContentType());
				if(document.getFileName().indexOf(".")==-1 && null!=info)
					fileExtension="."+info.getFileExtension();
			}catch(Exception exp){
				exp.printStackTrace();
			}
			
			
			byte[] in=null;
			in=accessFunctions.getDocInputStreamURL(contRep,docId,document.getFileName()+fileExtension,repository.getRootPath());
			log.info("document from  with file extension "+in);
			
			if(null == in){
				in=accessFunctions.getDocInputStreamURL(contRep,docId,document.getFileName(),repository.getRootPath());	
				log.info("document from  without file extension "+in);
			}
			
				
			out.println(("\nContent-Type: "+(document.getContentType()!=null &&document.getContentType().trim().length()>0 && !document.getContentType().equalsIgnoreCase("null")?document.getContentType():" "))); 
			
			out.println(("\nX-Content-Length: "+document.getContentLength()));
			out.println(("\nX-compId: "+document.getFileName()));
			out.println(("\nX-DateC: "+document.getCreatedDateTime()));					
			out.println(("\nX-DateM: "+document.getLastUpdatedDateTime()));					
			out.println(("\nX-contentRep: "+document.getArchiveId()));
			out.println(("\nX-numberComps: "+documentList.size()));
			out.println(("\nX-docId: "+document.getArchiveDocId()));
			out.println(("\nX-documentStatus: online"));
			out.println(("\nX-pVersion: "+pVersion+"\n"));
			
			
				out.println("");
				out.print("...");
				if(null!=in){
				out.write(in);
				}else{
					
					
				}
				out.println("...");
			//adding the document to the repsonse body
			out.print("--"+boundary);
			
			response.setStatus(200,"OK");
	}
		out.print("--");
		}catch(Exception exp){
			log.warn(exp.getMessage());
			exp.printStackTrace();
			response.setStatus(500,exp.getMessage()); 
			response.setHeader(SSConstants.xErrorDescription, exp.getMessage());
		}finally{
			if(null!=out)
				out.close();
		}
		return response;
	}

	@Override
	public HttpServletResponse Append(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		
		String pVersion= request.getParameter(SSConstants.pVersion);
		String contRep= request.getParameter(SSConstants.contRep);
		String docId= request.getParameter(SSConstants.docId);
		String compId= request.getParameter(SSConstants.compId);
		String accessMode= request.getParameter("acessMode");
		String authId= request.getParameter(SSConstants.authId);
		String expiration= request.getParameter(SSConstants.expiration);
		String secKey= request.getParameter(SSConstants.secKey);
		
		String sp= request.getParameter("sp");			
		try{
		if(accessMode==null)
			accessMode="u";
		
		if((contRep == null || docId ==null || compId == null || accessMode==null ||authId==null||expiration==null|| pVersion==null ||secKey==null )){
			response.setStatus(401, "Breach of security");// "Unknown function or unknown parameter");
			response.setHeader(SSConstants.xErrorDescription, contRep+"$"+pVersion+"$"+docId+"$"+accessMode+"$"+authId+"$"+expiration+"$"+secKey);
			return response;
		}
		
		
		Repository	repository=repositoryService.findOneByrepositoryId(contRep);
		Systems systems=systemsService.findOneBySystemAuthId(authId);
		
		
		
		if(null!=sp && sp.equalsIgnoreCase("true")){
			
		}else{
		//CHECKING PRE-CONDITIONS
		String PRE_CONDICTION_STATUS=SSUtils.checkPre_Conditions( authId, secKey, contRep+docId+accessMode+authId+expiration,expiration,contRep,repository,systems);
		log.info("PRE_CONDICTION_STATUS=="+PRE_CONDICTION_STATUS);
		if(PRE_CONDICTION_STATUS!=null){					
			response.setStatus(401,PRE_CONDICTION_STATUS);
			response.setHeader(SSConstants.xErrorDescription, PRE_CONDICTION_STATUS);
			return response;
		}
		}
		AdminAccessFunctions accessFunctions=AdminAccessFunctions.getInstance(authId, contRep, repository.getStorageType());
		
		Document doc=documentService.findOneByDocId(docId);					
		if(null==doc) {
			response.setStatus(404, "Document not found");	
			response.setHeader(SSConstants.xErrorDescription, "Document not found");
			return response;
		}else {
			
			doc.setLastUpdatedDateTime(ZonedDateTime.now());
			
			List<MimeType> mimetypesList=	mimetypeService.findMimeTypes();
			Map<String,String> mimeTypeMap=new HashMap<String,String>();
			for(MimeType mimetypes:mimetypesList) {
				mimeTypeMap.put(mimetypes.getMimeType(), mimetypes.getFileExtension());
			}
			
			String mimeType="binary/octet-stream";
			if(mimeTypeMap.containsKey(doc.getFileType().toLowerCase())){
				 mimeType="text/plain";
			}
			boolean appended = accessFunctions.appendDocFile(request.getInputStream(),contRep,docId,compId,(null!=mimeTypeMap.get(mimeType)?mimeTypeMap.get(mimeType):""));
			 if(appended){
				 	documentService.save(doc);
				    response.setStatus(200, "OK, data appended");
			 } else{
			    	response.setStatus(500,"Internal Server Error");
			    	response.setHeader(SSConstants.xErrorDescription, "Internal Server Error");
			    }
		}
		}catch(Exception e){
			response.setStatus(500,"Internal Server Error");
			response.setHeader(SSConstants.xErrorDescription, "Internal Server Error");
		}
		
		
		return response;
		
	}

	@Override
	public HttpServletResponse Update(HttpServletRequest request, HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HttpServletResponse Delete(HttpServletRequest request, HttpServletResponse response) throws IOException {
	
		String appendedText=null;
		String pVersion= request.getParameter(SSConstants.pVersion);
		String contRep= request.getParameter(SSConstants.contRep);
		String docId= request.getParameter(SSConstants.docId);
		String compId= request.getParameter(SSConstants.compId);
		//String resultAs= request.getParameter(SSConstants.resultAs);
		String accessMode= request.getParameter("acessMode");
		String authId= request.getParameter(SSConstants.authId);
		String expiration= request.getParameter(SSConstants.expiration);
		String secKey= request.getParameter(SSConstants.secKey);
		String sp=request.getParameter("sp");
		MimeType info=null;
		String systemId=null;
		if(authId !=null) {
			String[] authSplit=		authId.split(",",0);
			System.out.println(authSplit[0]);
		String systemName=	authSplit[0];
		systemId=	systemName.substring(authId.indexOf("=")+1 );
		}
		try{
			if(accessMode==null)
				accessMode="d";
			if(contRep==null || docId==null || pVersion==null || accessMode==null || authId==null  || expiration==null || secKey==null){
				response.setStatus(401, "Breach of security");// "mandatory fields missiing");
				response.setHeader(SSConstants.xErrorDescription, contRep+"$"+pVersion+"$"+docId+"$"+accessMode+"$"+authId+"$"+expiration+"$"+secKey);
				return response;
			}
			Repository	repository=repositoryService.findOneByrepositoryId(contRep);
			Systems systems=	systemsService.findOneBySystemId(systemId);		
			if(null==repository){
				response.setStatus(404, "Repository not found");
				response.setHeader(SSConstants.xErrorDescription, "Repository not found");
				return response;
			}
			
			if(compId==null){
				appendedText=contRep+docId+accessMode+authId+expiration;
			}
			else{
				appendedText=contRep+docId+compId+accessMode+authId+expiration;
			}
			
		
			String PRE_CONDICTION_STATUS=null;
		    if( sp ==null) {
			PRE_CONDICTION_STATUS=SSUtils.checkPre_Conditions( authId, secKey, appendedText, expiration, repository.getRepositoryId(), repository,systems);
		     if(PRE_CONDICTION_STATUS!=null){					
			response.setStatus(401,PRE_CONDICTION_STATUS);
				response.setHeader(SSConstants.xErrorDescription,PRE_CONDICTION_STATUS);
				return response;
		     }
		    }
			log.info("PreConditions successfully passed");
			AdminAccessFunctions accessFunctions=AdminAccessFunctions.getInstance(authId, contRep, repository.getStorageType());
			
			Document document=documentService.findOneByDocId(docId);					
			if(null==document) {
				response.setStatus(404, "Document not found");	
				response.setHeader(SSConstants.xErrorDescription, "Document not found");
				return response;
			} else {
				
				compId="data";
				
			
					boolean documentDeleted=false;
					
				String archiveDocIdResourceId=	document.getArchiveDocIdResourceId();
					
					if(repository.getStorageType().equalsIgnoreCase("gdrive")&&archiveDocIdResourceId !=null) {
					  documentDeleted=accessFunctions.deleteDocumentOrComponent(archiveDocIdResourceId);
					}else {
						documentDeleted=accessFunctions.DeleteDoc(repository.getName(),docId,compId,repository.getRootPath());
						
						System.out.println("documentDeleted"+documentDeleted);
					}
				if(!documentDeleted){
						//Getting the mime types
						String fileExtension="";
						if(null!=document){
						 info=mimetypeService.findOneByMimeType(request.getContentType());
						 if(null!=info) {
									fileExtension="."+info.getFileExtension();					
							documentDeleted=accessFunctions.DeleteDoc(repository.getName(),docId,compId+fileExtension,repository.getRootPath());
							 }
						}
										
					}
						if( documentDeleted){
							
							documentService.delete(docId);
							log.info("document with id '"+docId+"' is delete");
							response.setStatus(200,"document with id '"+docId+"' is delete");
						}
						else{
							log.info("document with id '"+docId+"' is doesnot Exist");
							response.setStatus(404,"document with id '"+docId+"' is doesnot Exist");
							response.setHeader(SSConstants.xErrorDescription, "document with id '"+docId+"' is doesnot Exist");
						}
					
			}
		}catch(Exception exp){exp.printStackTrace();
			response.setStatus(500,exp.getMessage());
			response.setHeader(SSConstants.xErrorDescription, exp.getMessage());
		}
		return response;
	
	}

	@Override
	public HttpServletResponse Search(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		
		
		return response;
}

	@Override
	public HttpServletResponse AttrSearch(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HttpServletResponse Mcreate(HttpServletRequest request, HttpServletResponse response) throws IOException {
		// TODO Auto-generated method stub
		
		return null;
	}

	

	
	
	@Override
	public HttpServletResponse createDoc(HttpServletRequest request, HttpServletResponse response) throws Exception  {
		// TODO Auto-generated method stub
		
		String pVersion= request.getParameter(SSConstants.pVersion);  
		String contentRepository= request.getParameter(SSConstants.contRep);
		String archiveDocId= request.getParameter(SSConstants.docId);
		String compId= request.getParameter(SSConstants.compId); 
		String docProt = request.getParameter("docProt");
		String accessMode = request.getParameter(SSConstants.accessMode);
		String authId = request.getParameter(SSConstants.authId); //CN=SD2 Like That
		
	   
		String expiration = request.getParameter(SSConstants.expiration);
		String secKey = request.getParameter(SSConstants.secKey);
		String contentType = request.getContentType();
		int contentLength = request.getContentLength();
		URL destUrl =null;
		String sp= request.getParameter("sp");
		String cContentType=null;
	
		String appendedtext=null;
		Repository repository=null;
		int cContentLength=0;
		String fileName=null;
		Document document=null;
		 String systemId=null;
		if(authId !=null) {
				String[] authSplit=		authId.split(",",0);
				System.out.println(authSplit[0]);
			String systemName=	authSplit[0];
			
			systemId=	systemName.substring(authId.indexOf("=")+1 );
				
			}
		
		if(request.getMethod().equalsIgnoreCase("PUT")) {
			
			System.out.println("<<<<<<<<<<<<<<<<<<<PUT METHOD CALLING>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			
			int status=201;
			List<MimeType> mimetypesList=	mimetypeService.findMimeTypes();
			
			Map<String,String> mimeTypeMap=new HashMap<String,String>();
			
			for(MimeType mimetypes:mimetypesList) {
				mimeTypeMap.put(mimetypes.getMimeType(), mimetypes.getFileExtension());
			}
		
			if(contentRepository == null || archiveDocId ==null || compId == null || contentLength ==0 ||accessMode==null ||authId==null||expiration==null|| pVersion==null ||secKey==null ){
				response.setStatus(401,  "Breach of security");//"Unknown function or unknown parameter");
				response.setHeader(SSConstants.xErrorDescription, contentRepository+"$"+pVersion+"$"+archiveDocId+"$"+compId+"$"+accessMode+"$"+authId+"$"+expiration+"$"+secKey);
				return response;
			}
		
		
			if(docProt==null){
				appendedtext=contentRepository+archiveDocId+compId+accessMode+authId+expiration;
			}
			else{
				appendedtext=contentRepository+archiveDocId+compId+docProt+accessMode+authId+expiration;
			}
		
			repository=repositoryService.findOneByrepositoryId(contentRepository);
			
			
			Systems systems =	systemsService.findOneBySystemId(systemId);
			String PRE_CONDICTION_STATUS=null;
		    if( sp ==null) {
			PRE_CONDICTION_STATUS=SSUtils.checkPre_Conditions( authId, secKey, appendedtext, expiration, repository.getRepositoryId(), repository,systems);
		    
		     if(PRE_CONDICTION_STATUS!=null){					
				
		    	response.setStatus(401,PRE_CONDICTION_STATUS);
				response.setHeader(SSConstants.xErrorDescription,PRE_CONDICTION_STATUS);
				return response;
		     }
		    }
				
				Document doc=documentService.findOneByDocId(archiveDocId);
				
				if(doc !=null) {
					response.setStatus(403, "Document already exists ");
					response.setHeader(SSConstants.xErrorDescription, "Document already exists");
					return response;
				}else {
					
					doc=new Document();
					
					List<String> components=new ArrayList<String>();
					components.add(compId);
					
					doc.setId(archiveDocId);
					doc.setArchiveId(contentRepository);
					doc.setArchiveDocId(archiveDocId);
					doc.setCreatedDateTime(ZonedDateTime.now());
					doc.setSystemId(systemId);
					doc.setLastUpdatedDateTime(ZonedDateTime.now());
					doc.setFileName(compId);
					doc.setFileType(contentType);
					doc.setComponents(components);
					doc.setContentType(contentType);
					doc.setContentLength(contentLength);
					
				}
				AdminAccessFunctions accessFunctions=AdminAccessFunctions.getInstance( authId, contentRepository, repository.getStorageType());
				String documentResourceId=null;
				if(repository.getStorageType().equalsIgnoreCase("gdrive")){
				String resourceId=	accessFunctions.getRepositoryResourceIdUnderSmartStore("Smartdocs", contentRepository);
				 documentResourceId=	accessFunctions.getDocumentResourceIdUnderRepository(resourceId, archiveDocId);
				 doc.setArchiveDocIdResourceId(documentResourceId);
				 System.out.println(documentResourceId);
				}
				
				
				
				if(compId != null) {
					
					fileName=compId;
					 if(compId.indexOf(".")!=-1){
							
							fileName=compId;
					}else {
						
						if(contentType!=null){
							fileName=compId;
							
						//	System.out.println("FileName:"+fileName);
							
							if(compId.indexOf(".")==-1 && mimeTypeMap.containsKey(contentType.toLowerCase()))
								fileName=compId+"."+mimeTypeMap.get(contentType.toLowerCase());
							
							
							log.info("new fileName==="+fileName+"\n"+mimeTypeMap.get(contentType.toLowerCase()));
							
							
						}
						else{
							doc.setFileName(compId);
							fileName=compId;
						}
					
					
					}
					
				}
				
				 if(repository.getStorageType().equalsIgnoreCase("gdrive")){
					 destUrl = accessFunctions.createcomponent(contentType,documentResourceId, fileName, request.getInputStream());
				 }else 
				 destUrl = accessFunctions.createcomponent(contentType, contentRepository, archiveDocId, fileName, request.getInputStream(),repository.getRootPath());
				 
				 System.out.println("destUrl -----------"+destUrl);
				 
				 	try{
						if(null!=destUrl && destUrl.toString().contains(";act;")){
							String temp9[]=destUrl.toString().split(";act;");
						   System.out.println("temp9[1]"+temp9[1]);
							doc.setResourceId(temp9[1]);
						}
						}catch (Exception e) {
							// TODO: handle exception
						}
				 
				 
				
				 try {		
				 if(destUrl !=null) {
							status=201;
							
							documentService.save(doc);
							response.setStatus(status, "Document Created");
						}else {
							status=500;
						   response.setStatus(status,"Document Not Created");
						}
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
				 
			//preconditions else
		
				 return response;
		
		
		}else if(request.getMethod().equalsIgnoreCase("POST")){
			int status=201;
			
			
			System.out.println("<<<<<<<<<<<<<<<<<<<POST METHOD CALLING>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			
		// if any manadatory parameters are missing then return error code
			if(contentRepository == null || archiveDocId ==null || contentLength ==0 ||accessMode==null ||authId==null||expiration==null|| pVersion==null ||secKey==null ){
				response.setStatus(401, "Breach of security");// "Unknown function or unknown parameter");
				response.setHeader(SSConstants.xErrorDescription, contentRepository+"$"+pVersion+"$"+archiveDocId+"$"+accessMode+"$"+authId+"$"+expiration+"$"+secKey);
				return response;
			}
			if(docProt==null){
				appendedtext=contentRepository+archiveDocId+accessMode+authId+expiration;
			}
			else{
				appendedtext=contentRepository+archiveDocId+docProt+accessMode+authId+expiration;
			}
			
			repository=repositoryService.findOneByrepositoryId(contentRepository);

			Systems systems =	systemsService.findOneBySystemId(systemId);
			
			String PRE_CONDICTION_STATUS=null;
		    if( sp ==null) {
		    
		    	
		    	System.out.println(authId+"........."+secKey+"......."+appendedtext+"......."+expiration+"........"+repository.getRepositoryId()+"......"+systems.getSystemId());
			PRE_CONDICTION_STATUS=SSUtils.checkPre_Conditions( authId, secKey, appendedtext, expiration, repository.getRepositoryId(), repository,systems);
		    
		     if(PRE_CONDICTION_STATUS!=null){					
				
		    	response.setStatus(401,PRE_CONDICTION_STATUS);
				response.setHeader(SSConstants.xErrorDescription,PRE_CONDICTION_STATUS);
				return response;
		     }
		    }
		

			repository=repositoryService.findOneByrepositoryId(contentRepository);
		
		
			
			List<MimeType> mimetypesList=	mimetypeService.findMimeTypes();
			
			Map<String,String> mimeTypeMap=new HashMap<String,String>();
			
			for(MimeType mimetypes:mimetypesList) {
				mimeTypeMap.put(mimetypes.getMimeType(), mimetypes.getFileExtension());
			}
		
		
			Document doc=documentService.findOneByDocId(archiveDocId);
			
		
			if(doc !=null) {
				response.setStatus(403, "Document already exists");
				response.setHeader(SSConstants.xErrorDescription, "Document already exists");
				return response;
			}else {
				
				
				document=new Document();
				
				List<String> components=new ArrayList<String>();
				components.add(compId);
				
				document.setId(archiveDocId);
				document.setArchiveId(contentRepository);
				document.setArchiveDocId(archiveDocId);
				document.setCreatedDateTime(ZonedDateTime.now());
				document.setSystemId(systemId);
				document.setLastUpdatedDateTime(ZonedDateTime.now());
				document.setFileName(compId);
				document.setFileType(contentType);
				//document.setComponents(components);
				
				document.setContentLength(contentLength);
				
			}
		
		
			AdminAccessFunctions accessFunctions=AdminAccessFunctions.getInstance( authId, contentRepository, repository.getStorageType());
			String documentResourceId=null;
			if(repository.getStorageType().equalsIgnoreCase("gdrive")){
				String resourceId=	accessFunctions.getRepositoryResourceIdUnderSmartStore("Smartdocs", contentRepository);
				 documentResourceId=	accessFunctions.getDocumentResourceIdUnderRepository(resourceId, archiveDocId);
				 if(documentResourceId !=null) {
					 document.setArchiveDocIdResourceId(documentResourceId);
				 }
				
				}
			
			
			
			
		
			//parsing body
			int boundaryIndex = contentType.indexOf("boundary=");
			//doc.setContentType(contentType.substring(0, boundaryIndex));
			
			byte[] boundary = (contentType.substring(boundaryIndex + 9)).getBytes();
			
			
			System.out.println("boundary:"+new String(boundary));
		
			MultipartStream mps=new MultipartStream(request.getInputStream(),boundary);
			
			boolean nextPart = mps.skipPreamble();
			
			System.out.println("nextPart:"+nextPart);
			
			
			int count=0;
			
			starting main while loop
			while(nextPart) {
				try {
				  String headers = mps.readHeaders();
				  log.info("Headers: " + headers);
				  StringTokenizer st = new StringTokenizer(headers,"\n");
				  
				  while (st.hasMoreTokens()) {
				    	 StringTokenizer subst = new StringTokenizer(st.nextToken(),":");
				    	 String s1=subst.nextToken();
				    	  if(s1.equalsIgnoreCase("X-compId")){
				    		  compId=subst.nextToken().trim();
				    		  document.setFileName(compId);
				    		  List<String> components=new ArrayList<String>();
				    		  components.add(compId);
				    		  document.setComponents(components);
				    		  
				    	   log.info("X-compId is  "+compId);
				    	  }
				    	 
				    	  else if(s1.equalsIgnoreCase("Content-Type")){
				    		  cContentType=subst.nextToken().trim();
				    		  document.setContentType(cContentType);
							
				    	  }
				    	 
				    	  else if(s1.equalsIgnoreCase("Content-Length")){
				    		  cContentLength=Integer.parseInt(subst.nextToken().trim());
				    		 log.info("Content-Length is "+cContentLength);
				    	  }
				    	  else if(s1.equalsIgnoreCase("X-docId")){
				    		 
				    		 log.info("X-docId "+subst.nextToken().trim());
				    	  }
				       }
				  
				  
				  ByteArrayOutputStream data = new ByteArrayOutputStream();
				  mps.readBodyData(data);
				  
				  if ( compId != null){
					  if(compId.indexOf(".")!=-1){
							log.info(compId);
							fileName=compId;
						}else{
							if(cContentType!=null){
								fileName=compId;
								try{
							
								
									if(compId.indexOf(".")==-1 && mimeTypeMap.containsKey(cContentType.toLowerCase()))
										fileName=compId+"."+mimeTypeMap.get(cContentType.toLowerCase());
							log.info("new fileName==="+fileName+"\n"+mimeTypeMap.get(cContentType.toLowerCase()));
							
								}
						
							catch(Exception e){
									e.printStackTrace();
								}
							}
							else{
								fileName=compId;
							}
							
						}
					  InputStream inputstream = new ByteArrayInputStream(data.toByteArray());
						
						 if(repository.getStorageType().equalsIgnoreCase("gdrive")){
							 destUrl = accessFunctions.createcomponent(cContentType,documentResourceId, fileName, inputstream);
						 }else 
							 destUrl=	accessFunctions.createcomponent(cContentType, contentRepository, archiveDocId,fileName,inputstream,repository.getRootPath());
						 try{
								if(null!=destUrl && destUrl.toString().contains(";act;")){
									String temp9[]=destUrl.toString().split(";act;");
								   System.out.println("temp9[1]"+temp9[1]);
									document.setResourceId(temp9[1]);
								}
								}catch (Exception e) {
									// TODO: handle exception
								}
						
						 if(null!=destUrl){
							 status=201;
							 documentService.save(document);
							 response.setStatus(status, "OK, document(s) created");
							// documentService.save(doc);
								count++;
						
						 }else {
							 status=500;
							 response.setStatus(status, "Document Not Created");
						 }
						
				 }
				
				  nextPart = mps.readBoundary();
				}catch(Exception e) {
					nextPart=false;
				}
				
			}ending main while loop
			
			
			
			
		
		
			
			return response;
		
		
		}else {
			log.warn("Unknown function or unknown parameter");
			response.setStatus(400, "Unknown function or unknown parameter");
			response.setHeader(SSConstants.xErrorDescription,"Unknown function or unknown parameter");
			return response;
		}
	//return response;
	}
	
	
	

}
*/