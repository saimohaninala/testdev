package com.smartdocs.smartportal.service;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.smartdocs.smartportal.domain.SmartstoreSettings;

public interface SmartstoreSettingsService {

	SmartstoreSettings save(SmartstoreSettings smartstore);
	Page<SmartstoreSettings> findAll(Pageable pageable);
	SmartstoreSettings findOne(String id);
	void delete(String id);
	 Optional<SmartstoreSettings> findOneByKey(String key);
	 //SmartstoreSettings findOneByCompanyId(String key);
}
