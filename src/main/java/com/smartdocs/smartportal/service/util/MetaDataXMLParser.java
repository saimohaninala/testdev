package com.smartdocs.smartportal.service.util;


import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;


import com.smartdocs.smartportal.domain.Document;

public class MetaDataXMLParser {
public  List<Document> getParseXMLData(String xmldata){
	//System.out.println("entered into metedataparser------");
	Document documentData=null;
	xmldata=replaceUnsupportedXMLChar(xmldata);
	List<Document> docdatalist=new ArrayList<Document>();
	try{
		
	BufferedReader rd = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(xmldata.getBytes())));
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	DocumentBuilder documentBuilder = factory.newDocumentBuilder();
	org.w3c.dom.Document doc = documentBuilder.parse(new InputSource(rd));
	doc.getDocumentElement().normalize();
	NodeList rootList = doc.getChildNodes();
	//System.out.println("rootList="+rootList.getLength());
	for (int j = 0; j < rootList.getLength(); j++) {
		Node rootNode = rootList.item(j);
		
		NodeList childList = rootNode.getChildNodes();
		for (int k = 0; k < childList.getLength(); k++) {	
			Node workItemNode = childList.item(k);
			
			//System.out.println("workItemNode======"+workItemNode.getNodeName());
			NodeList workItemList = workItemNode.getChildNodes();
			if (workItemNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) workItemNode;
				//System.out.println(childNode.getNodeName());
				 documentData=new Document(getTagValue("ARC_ID", eElement),getTagValue("ARC_DOCID", eElement),getTagValue("FILE_NAME", eElement),getTagValue("FILE_TYPE", eElement));
				//docdatalist.add(new Document(getTagValue("ARC_ID", eElement),getTagValue("ARC_DOCID", eElement),getTagValue("FILE_NAME", eElement),getTagValue("FILE_TYPE", eElement)));
				}			
			docdatalist.add(documentData);
		}
	}
	}catch (Exception e) {
		e.printStackTrace();
	}
	return docdatalist;
}
private static String getTagValue(String sTag, Element element) {
	try {
		NodeList nlList = element.getElementsByTagName(sTag).item(0)
				.getChildNodes();
		Node nValue = (Node) nlList.item(0);
		if (nValue == null)
			return "";
		else
			return nValue.getNodeValue();
	} catch (Exception e) {
		//e.printStackTrace();
	}
	return null;
}
public org.w3c.dom.Document getDocument(String xmldata){
	try{
	BufferedReader rd = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(xmldata.getBytes())));
	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	DocumentBuilder documentBuilder = factory.newDocumentBuilder();
	return documentBuilder.parse(new InputSource(rd));
	}catch (Exception e) {
		e.printStackTrace();
	}
	return null;
	
}
/*public String replaceUnsupportedXMLChar(String xmlData){
	xmlData=xmlData.replaceAll("&", "&amp;");
	xmlData=xmlData.replaceAll("%20", " ");
	return xmlData;
}*/
public String replaceUnsupportedXMLChar(String xmlData){
	try{
		if(null!=xmlData){
	//xmlData=xmlData.replaceAll("&", "&amp;");
	//xmlData=xmlData.replaceAll("\"", "&quot;");
	//xmlData=xmlData.replaceAll("'", "&apos;");
	//xmlData=xmlData.replaceAll("<", "&lt;");
	//xmlData=xmlData.replaceAll(">", "&gt;");
	xmlData=xmlData.replaceAll("%20", " ");
		}
	}catch (Exception e) {
		e.printStackTrace();
	}
	return xmlData;
}
public synchronized static String replaceUnsupportedInResponseBody(String xmlData){
	try{
		if(null!=xmlData){
			xmlData=xmlData.replaceAll("&", "&amp;");
		xmlData=xmlData.replaceAll("\"", "&quot;");
		
		xmlData=xmlData.replaceAll("'", "&apos;");
		xmlData=xmlData.replaceAll("<", "&lt;");
		xmlData=xmlData.replaceAll(">", "&gt;");
		}
	}catch (Exception e) {
		e.printStackTrace();
	}
	return xmlData;
}


}
