package com.smartdocs.smartportal.service;

import com.smartdocs.smartportal.domain.SystemSettings;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing SystemSettings.
 */
public interface SystemSettingsService {

	/**
	 * Save a SystemSettings.
	 *
	 * @param SystemSettings
	 *            the entity to save
	 * @return the persisted entity
	 */
	SystemSettings save(SystemSettings systemSettings);

	/**
	 * Get all the SystemSettings.
	 * 
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	Page<SystemSettings> findAll(Pageable pageable);

	/**
	 * Get the "id" SystemSettings.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	SystemSettings findOne(String id);

	/**
	 * Delete the "id" SystemSettings.
	 *
	 * @param id
	 *            the id of the entity
	 */
	void delete(String id);

	Optional<SystemSettings> findOneBySystem(String system);
}