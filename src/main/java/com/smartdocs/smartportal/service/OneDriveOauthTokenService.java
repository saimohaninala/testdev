package com.smartdocs.smartportal.service;

import com.smartdocs.smartportal.domain.Repository;
public interface OneDriveOauthTokenService {
	
	public String getActiveAccessToken(Repository repository,String id);
	
}
