package com.smartdocs.smartportal.service;

import com.smartdocs.smartportal.domain.Authority;
import com.smartdocs.smartportal.domain.User;

import com.smartdocs.smartportal.repository.AuthorityRepository;
import com.smartdocs.smartportal.repository.SequenceDao;
import com.smartdocs.smartportal.repository.UserRepository;
import com.smartdocs.smartportal.security.AuthoritiesConstants;
import com.smartdocs.smartportal.security.SecurityUtils;
import com.smartdocs.smartportal.service.util.RandomUtil;

import com.smartdocs.smartportal.web.rest.vm.ManagedUserVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;
import javax.inject.Inject;
import java.util.*;

/**
 * Service class for managing users.
 */
@Service
public class UserService {

    private final Logger log = LoggerFactory.getLogger(UserService.class);
    
	private static final String USER_SEQ_KEY = "User";
	
	@Inject
	private SequenceDao sequenceDao;

  

    @Inject
    private PasswordEncoder passwordEncoder;

    @Inject
    private UserRepository userRepository;

    @Inject
    private AuthorityRepository authorityRepository;
    
  

    public Optional<User> activateRegistration(String key) {
        log.debug("Activating user for activation key {}", key);
        return userRepository.findOneByActivationKey(key)
            .map(user -> {
                // activate given user for the registration key.
                user.setActivated(true);
                user.setActivationKey(null);
                userRepository.save(user);
                log.debug("Activated user: {}", user);
                return user;
            });
    }

    public Optional<User> completePasswordReset(String newPassword, String key) {
       log.debug("Reset user password for reset key {}", key);

       return userRepository.findOneByResetKey(key)
            .filter(user -> {
                ZonedDateTime oneDayAgo = ZonedDateTime.now().minusHours(24);
                return user.getResetDate().isAfter(oneDayAgo);
           })
           .map(user -> {
                user.setPassword(passwordEncoder.encode(newPassword));
                user.setResetKey(null);
                user.setResetDate(null);
                userRepository.save(user);
                return user;
           });
    }

    public Optional<User> requestPasswordReset(String mail) {
        return userRepository.findOneByEmail(mail)
            .filter(User::getActivated)
            .map(user -> {
                user.setResetKey(RandomUtil.generateResetKey());
                user.setResetDate(ZonedDateTime.now());
                userRepository.save(user);
                return user;
            });
    }

    public User createUser(String login, String password, String firstName, String lastName, String email,
        String langKey)  {

        User newUser = new User();
        try {
			newUser.setId(sequenceDao.getNextSequenceId(USER_SEQ_KEY)+"");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        Authority authority = authorityRepository.findOne(AuthoritiesConstants.USER);
        Set<Authority> authorities = new HashSet<>();
        String encryptedPassword = passwordEncoder.encode(password);
        newUser.setLogin(login);
        // new user gets initially a generated password
        newUser.setPassword(encryptedPassword);
        newUser.setFirstName(firstName);
        newUser.setLastName(lastName);
        newUser.setEmail(email);
        newUser.setLangKey(langKey);
        // new user is not active
        newUser.setActivated(false);
        // new user gets registration key
        newUser.setActivationKey(RandomUtil.generateActivationKey());
        authorities.add(authority);
        newUser.setAuthorities(authorities);
        userRepository.save(newUser);
        log.debug("Created Information for User: {}", newUser);
        return newUser;
    }
    
    public User createUser(String login, String password, String firstName, String lastName, String email,
            String langKey,boolean actvated) {

            User newUser = new User();
			try {
				newUser.setId(sequenceDao.getNextSequenceId(USER_SEQ_KEY)+"");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

            Authority authority = authorityRepository.findOne(AuthoritiesConstants.USER);
            Set<Authority> authorities = new HashSet<>();
            String encryptedPassword = passwordEncoder.encode(password);
            newUser.setLogin(login);
            // new user gets initially a generated password
            newUser.setPassword(encryptedPassword);
            newUser.setFirstName(firstName);
            newUser.setLastName(lastName);
            newUser.setEmail(email);
            newUser.setLangKey(langKey);
            // new user is not active
            newUser.setActivated(actvated);
            // new user gets registration key
            if(!actvated)
            newUser.setActivationKey(RandomUtil.generateActivationKey());
            authorities.add(authority);
            newUser.setAuthorities(authorities);
            userRepository.save(newUser);
            log.debug("Created Information for User: {}", newUser);
            return newUser;
        }

    public User createUser(ManagedUserVM managedUserVM) {
        User user = new User();
		try {
			user.setId(sequenceDao.getNextSequenceId(USER_SEQ_KEY)+"");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        user.setLogin(managedUserVM.getLogin());
        user.setFirstName(managedUserVM.getFirstName());
        user.setLastName(managedUserVM.getLastName());
        user.setEmail(managedUserVM.getEmail());
        user.setDateFormat(managedUserVM.getDateFormat());
        user.setCurrency(managedUserVM.getCurrency());
        user.setDecimalSeparator(managedUserVM.getDecimalSeparator());
        user.setLanguage(managedUserVM.getLanguage());
        user.setTimeZone(managedUserVM.getTimeZone());
        user.setLastModifiedBy(managedUserVM.getLastModifiedBy());
        user.setLastModifiedDate(managedUserVM.getLastModifiedDate());
        
        
        if (managedUserVM.getLangKey() == null) {
            user.setLangKey("en"); // default language
        } else {
            user.setLangKey(managedUserVM.getLangKey());
        }
        if (managedUserVM.getAuthorities() != null) {
            Set<Authority> authorities = new HashSet<>();
            managedUserVM.getAuthorities().forEach(
                authority -> authorities.add(authorityRepository.findOne(authority))
            );
            user.setAuthorities(authorities);
        }
       // String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
        
        String encryptedPassword = null;
        if(managedUserVM.getPassword()== null) {
         encryptedPassword = passwordEncoder.encode("Smartdocs@123");
        }
        else {
             encryptedPassword = passwordEncoder.encode(managedUserVM.getPassword());
        }
        
        
        
        
       // String encryptedPassword = passwordEncoder.encode("Smartdocs@123");
        user.setPassword(encryptedPassword);
        user.setResetKey(RandomUtil.generateResetKey());
        user.setResetDate(ZonedDateTime.now());
        user.setActivated(managedUserVM.isActivated());
        userRepository.save(user);
        log.debug("Created Information for User: {}", user);
        return user;
    }

    public void updateUser(String firstName, String lastName, String email, String langKey) {
        userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmail(email);
            user.setLangKey(langKey);
            userRepository.save(user);
            log.debug("Changed Information for User: {}", user);
        });
    }
    
   


    public void updateUser(String id, String login, String firstName, String lastName, String email,
        boolean activated, String langKey, Set<String> authorities,String currency,String decimalSeparator,String language,String dateFormat,String timeZone) {

        Optional.of(userRepository
            .findOne(id))
            .ifPresent(user -> {
                user.setLogin(login);
                user.setFirstName(firstName);
                user.setLastName(lastName);
                user.setEmail(email);
                user.setActivated(activated);
                user.setLangKey(langKey);
                Set<Authority> managedAuthorities = user.getAuthorities();
                managedAuthorities.clear();
                authorities.forEach(
                    authority -> managedAuthorities.add(authorityRepository.findOne(authority))
                );
                user.setCurrency(currency);
                user.setDecimalSeparator(decimalSeparator);
                user.setLanguage(language);
                user.setCurrency(currency);
                user.setDateFormat(dateFormat);
                user.setTimeZone(timeZone);
                
                userRepository.save(user);
                log.debug("Changed Information for User: {}", user);
            });
    }

    public void deleteUser(String login) {
        userRepository.findOneByLogin(login).ifPresent(user -> {
          //  socialService.deleteUserSocialConnection(user.getLogin());
            userRepository.delete(user);
          //  groupService.deleteMemberFromAllGroups(login);
            
            log.debug("Deleted User: {}", user);
        });
    }

    public void changePassword(String password) {
        userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin()).ifPresent(user -> {
            String encryptedPassword = passwordEncoder.encode(password);
            user.setPassword(encryptedPassword);
            userRepository.save(user);
            log.debug("Changed password for User: {}", user);
        });
    }

    public Optional<User> getUserWithAuthoritiesByLogin(String login) {
        return userRepository.findOneByLogin(login);
    }
    
   

    public User getUserWithAuthorities(String id) {
        User user = userRepository.findOne(id);
        return user;
    }

    public User getUserWithAuthorities() {
        Optional<User> optionalUser = userRepository.findOneByLogin(SecurityUtils.getCurrentUserLogin());
        User user = null;
        if (optionalUser.isPresent()) {
          user = optionalUser.get();
         }
         return user;
    }
    public List<User> createuploadUser(List<ManagedUserVM> managedUserVM) {
        
	     
	     List<User> userList=new ArrayList<User>();
	     
	    for(ManagedUserVM managedUserVM1:managedUserVM) {
	    User user = new User();
	  try {
	   user.setId(sequenceDao.getNextSequenceId(USER_SEQ_KEY)+"");
	  } catch (Exception e) {
	   // TODO Auto-generated catch block
	   e.printStackTrace();
	  }
	  user.setLogin(managedUserVM1.getLogin());
	        user.setFirstName(managedUserVM1.getFirstName());
	        user.setLastName(managedUserVM1.getLastName());
	        user.setEmail(managedUserVM1.getEmail());
	        user.setDateFormat(managedUserVM1.getDateFormat());
	        user.setCurrency(managedUserVM1.getCurrency());
	        user.setDecimalSeparator(managedUserVM1.getDecimalSeparator());
	        user.setLanguage(managedUserVM1.getLanguage());
	        user.setTimeZone(managedUserVM1.getTimeZone());
	        user.setLastModifiedBy(managedUserVM1.getLastModifiedBy());
	        user.setLastModifiedDate(managedUserVM1.getLastModifiedDate());
	        
	        
	        if (managedUserVM1.getLangKey() == null) {
	            user.setLangKey("en"); // default language
	        } else {
	            user.setLangKey(managedUserVM1.getLangKey());
	        }
	        if (managedUserVM1.getAuthorities() != null) {
	            Set<Authority> authorities = new HashSet<>();
	            managedUserVM1.getAuthorities().forEach(
	                authority -> authorities.add(authorityRepository.findOne(authority))
	            );
	            user.setAuthorities(authorities);
	        }
	       // String encryptedPassword = passwordEncoder.encode(RandomUtil.generatePassword());
	        
	        String encryptedPassword = null;
	        if(managedUserVM1.getPassword()== null) {
	         encryptedPassword = passwordEncoder.encode("Tril@123");
	        }
	        else {
	             encryptedPassword = passwordEncoder.encode(managedUserVM1.getPassword());
	        }
	         // String encryptedPassword = passwordEncoder.encode("Smartdocs@123");
	        user.setPassword(encryptedPassword);
	        user.setResetKey(RandomUtil.generateResetKey());
	        user.setResetDate(ZonedDateTime.now());
	        user.setActivated(managedUserVM1.isActivated());
	        
	        userList.add(user);
	    } 
	    
	     userRepository.save(userList);
	        log.debug("Created Information for User: {}", userList);
	        return userList;
	   
	    
	    }
    
    public User findOneObjectByLogin(String login) {
		// TODO Auto-generated method stub
		return userRepository.findOneObjectByLogin(login);
	}
	

    /**
     * Not activated users should be automatically deleted after 3 days.
     * <p>
     * This is scheduled to get fired everyday, at 01:00 (am).
     * </p>
     */
  //  @Scheduled(cron = "0 0 1 * * ?")
    public void removeNotActivatedUsers() {
        ZonedDateTime now = ZonedDateTime.now();
        List<User> users = userRepository.findAllByActivatedIsFalseAndCreatedDateBefore(now.minusDays(3));
        for (User user : users) {
            log.debug("Deleting not activated user {}", user.getLogin());
            userRepository.delete(user);
        }
    }

	
}
