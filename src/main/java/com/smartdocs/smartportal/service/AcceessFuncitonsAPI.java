package com.smartdocs.smartportal.service;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



public interface AcceessFuncitonsAPI {
	public HttpServletResponse Info(HttpServletRequest request, HttpServletResponse response);
	public HttpServletResponse Get(HttpServletRequest request, HttpServletResponse response) throws IOException;
	public HttpServletResponse DocGet(HttpServletRequest request, HttpServletResponse response) throws IOException;
	public HttpServletResponse Append(HttpServletRequest request, HttpServletResponse response) throws IOException;
	public HttpServletResponse Update(HttpServletRequest request, HttpServletResponse response) throws Exception;
	public HttpServletResponse Delete(HttpServletRequest request, HttpServletResponse response) throws IOException;
	public HttpServletResponse Search(HttpServletRequest request, HttpServletResponse response) throws IOException;
	public HttpServletResponse AttrSearch(HttpServletRequest request, HttpServletResponse response) throws IOException;
	public HttpServletResponse Mcreate(HttpServletRequest request, HttpServletResponse response) throws IOException;
	public HttpServletResponse createDoc(HttpServletRequest request, HttpServletResponse response) throws Exception;
	public HttpServletResponse Restore(HttpServletRequest request, HttpServletResponse response) throws Exception;


	
}
