package com.smartdocs.smartportal.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.smartdocs.smartportal.domain.Document;

public interface DocumentService {
	 /**
     * Save a documents.
     *
     * @param documents the entity to save
     * @return the persisted entity
     */
	 Document save( Document  document);

    /**
     *  Get all the documents.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page< Document> findAll(Pageable pageable);

    /**
     *  Get the "id" documents.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Document findOne(String id);

    /**
     *  Delete the "id" documents.
     *
     *  @param id the id of the entity
     */
    void delete(String id);
	
    Document findOneByDocId(String docId);
    
   List<Document> findOneByArchiveDocIdAndArchiveId(String archiveDocId,String archiveId);
   
   List<Document> findOneBySystemIdAndArchiveIdAndArchiveDocId(String systemId,String archiveId,String archiveDocId);
  
    List<Document> findByDocIdAndFileName(String docId,String fileName);
    
    Document findOneByArchiveDocIdAndFileName(String docId,String fileName);
    
}


