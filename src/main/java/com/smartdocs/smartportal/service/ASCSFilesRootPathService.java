package com.smartdocs.smartportal.service;

import java.util.List;

import com.smartdocs.smartportal.domain.ASCSFilesRootPath;


//@RemoteServiceRelativePath("ascsRootPath")
public interface ASCSFilesRootPathService {
	public ASCSFilesRootPath getFileSystemRootpaths(String compnay,String systemId,String repositoryID);
	public boolean deleteFileSystemRootpath(String compnay,String systemId,String repositoryID) ;
	public boolean createFileSystemRootpath(ASCSFilesRootPath rootPath);
	public boolean updateFileSystemRootpath(ASCSFilesRootPath rootPath);
	public List getFileSystemRootpathsList() throws RuntimeException;
	public List getFileSystemRootpathList(String compnay,String systemId,String repositoryID) throws RuntimeException;
	public List getFileSystemRootpathList(String compnay,String systemId) throws RuntimeException;
	public ASCSFilesRootPath getFileSystemRootpath(String compnay,String systemId) throws RuntimeException;
}
