package com.smartdocs.smartportal.service;



import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.smartdocs.smartportal.domain.Systems;

public interface SystemsService {
	
	
	Systems save(Systems systems);
	Page<Systems> findAll(Pageable pageable);
	Systems findOne(String id);
	void delete(String id);
	Optional<Systems> findOneBySystemName(String systemName);
	Systems findOneBySystemAuthId(String authId);
	
	Systems findOneBySystemId(String systemId);
	
}

