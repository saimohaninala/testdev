package com.smartdocs.smartportal.service;

import java.util.List;

import com.smartdocs.smartportal.domain.BrandSetting;

public interface BrandService {

	BrandSetting save(BrandSetting brand);
	List<BrandSetting> get();
}
