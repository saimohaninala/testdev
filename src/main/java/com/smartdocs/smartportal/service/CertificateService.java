package com.smartdocs.smartportal.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.smartdocs.smartportal.domain.Certificate;



public interface CertificateService   {
	
	 /**
     * Save a systems.
     *
     * @param systemsDTO the entity to save
     * @return the persisted entity
     */
	Certificate save(Certificate certificate);

    /**
     *  Get all the systems.
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<Certificate> findAll(Pageable pageable);

    /**
     *  Get the "id" systems.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    Certificate findOne(String id);

    /**
     *  Delete the "id" systems.
     *
     *  @param id the id of the entity
     */
    void delete(String id);
	
	
	 Certificate findCertificateBySystemId(String systemId);
     void deleteCertificateBySystemId(String systemId);
}
