package com.smartdocs.smartportal.service;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.smartdocs.smartportal.domain.Repository;


public interface RepositoryService {
	Repository save(Repository repository);
	Page<Repository> findAll(Pageable pageable);
	void delete(String repositoryId);
	 Optional<Repository> findOneByName(String name);
	 Repository findOneByrepositoryId(String repositoryId);
	List<Repository> findOneByRepositoryId(String repositoryId);
	
	List<Repository> findOneBySystemId(String systemId);
	List<Repository> findAllList();
	

}
