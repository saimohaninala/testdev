package com.smartdocs.smartportal.service;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AdminFuncitonsAPI {
	public HttpServletResponse setPutCert(HttpServletRequest request,HttpServletResponse response) throws IOException, Exception;
	public HttpServletResponse serverInfo(HttpServletRequest request,HttpServletResponse response) throws IOException;
}
