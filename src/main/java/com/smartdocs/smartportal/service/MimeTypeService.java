package com.smartdocs.smartportal.service;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.smartdocs.smartportal.domain.MimeType;


public interface MimeTypeService {
	
	
	MimeType save(MimeType mimeType);
	Page<MimeType> findAll(Pageable pageable);
	//Company findOne(String id);
	void delete(String id);
	 MimeType findOneByMimeType(String mimeType);
	 Optional<MimeType> findOneBymimeType(String mimeType);
	 List<MimeType>  findMimeTypes();
	 MimeType  findMimeTypeByCompanyId(String companyId);
	MimeType findOneByfileExtension(String fileExtension);
}
