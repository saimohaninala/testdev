package com.smartdocs.smartportal.service;

import java.io.IOException;

import com.google.api.client.http.HttpResponse;

public interface OneDriveClientApi {
	
	 public HttpResponse get(String url) throws IOException;
	 public HttpResponse post(String url, Object data) throws IOException;
	 public HttpResponse put(String url, Object data) throws IOException;
	 public HttpResponse delete(String url) throws IOException;
}
