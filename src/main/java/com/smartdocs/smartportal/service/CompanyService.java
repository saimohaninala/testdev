package com.smartdocs.smartportal.service;

import com.smartdocs.smartportal.domain.Company;



import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
public interface CompanyService {

	
	Company save(Company company);
	Page<Company> findAll(Pageable pageable);
	Company findOne(String id);
	void delete(String companyId);
	 Optional<Company> findOneByName(String name);
	 Company findOneByCompanyId(String companyId);

}
