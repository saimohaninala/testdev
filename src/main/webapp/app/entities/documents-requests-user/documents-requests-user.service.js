(function() {
    'use strict';
    angular
        .module('smartportalApp')
        .factory('DocumentsRequestsUser', DocumentsRequestsUser);

    DocumentsRequestsUser.$inject = ['$resource'];

    function DocumentsRequestsUser ($resource) {
        var resourceUrl =  'api/documents-requests-users/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
