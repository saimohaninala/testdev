(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('DocumentsRequestsUserDetailController', DocumentsRequestsUserDetailController);

    DocumentsRequestsUserDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DocumentsRequestsUser'];

    function DocumentsRequestsUserDetailController($scope, $rootScope, $stateParams, previousState, entity, DocumentsRequestsUser) {
        var vm = this;

        vm.documentsRequestsUser = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:documentsRequestsUserUpdate', function(event, result) {
            vm.documentsRequestsUser = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
