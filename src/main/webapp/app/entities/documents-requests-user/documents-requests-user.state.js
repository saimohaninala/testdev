(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('documents-requests-user', {
            parent: 'entity',
            url: '/documents-requests-user',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.documentsRequestsUser.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/documents-requests-user/documents-requests-users.html',
                    controller: 'DocumentsRequestsUserController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('documentsRequestsUser');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('documents-requests-user-detail', {
            parent: 'documents-requests-user',
            url: '/documents-requests-user/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.documentsRequestsUser.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/documents-requests-user/documents-requests-user-detail.html',
                    controller: 'DocumentsRequestsUserDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('documentsRequestsUser');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'DocumentsRequestsUser', function($stateParams, DocumentsRequestsUser) {
                    return DocumentsRequestsUser.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'documents-requests-user',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('documents-requests-user-detail.edit', {
            parent: 'documents-requests-user-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/documents-requests-user/documents-requests-user-dialog.html',
                    controller: 'DocumentsRequestsUserDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DocumentsRequestsUser', function(DocumentsRequestsUser) {
                            return DocumentsRequestsUser.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('documents-requests-user.new', {
            parent: 'documents-requests-user',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/documents-requests-user/documents-requests-user-dialog.html',
                    controller: 'DocumentsRequestsUserDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                requestId: null,
                                requestType: null,
                                user: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('documents-requests-user', null, { reload: 'documents-requests-user' });
                }, function() {
                    $state.go('documents-requests-user');
                });
            }]
        })
        .state('documents-requests-user.edit', {
            parent: 'documents-requests-user',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/documents-requests-user/documents-requests-user-dialog.html',
                    controller: 'DocumentsRequestsUserDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DocumentsRequestsUser', function(DocumentsRequestsUser) {
                            return DocumentsRequestsUser.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('documents-requests-user', null, { reload: 'documents-requests-user' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('documents-requests-user.delete', {
            parent: 'documents-requests-user',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/documents-requests-user/documents-requests-user-delete-dialog.html',
                    controller: 'DocumentsRequestsUserDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DocumentsRequestsUser', function(DocumentsRequestsUser) {
                            return DocumentsRequestsUser.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('documents-requests-user', null, { reload: 'documents-requests-user' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
