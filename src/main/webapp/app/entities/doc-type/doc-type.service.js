(function() {
    'use strict';
    angular
        .module('smartportalApp')
        .factory('DocType', DocType);

    DocType.$inject = ['$resource'];

    function DocType ($resource) {
        var resourceUrl =  'api/doc-types/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
