(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('doc-type', {
            parent: 'entity',
            url: '/doc-type',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.docType.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/doc-type/doc-types.html',
                    controller: 'DocTypeController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('docType');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('doc-type-detail', {
            parent: 'doc-type',
            url: '/doc-type/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.docType.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/doc-type/doc-type-detail.html',
                    controller: 'DocTypeDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('docType');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'DocType', function($stateParams, DocType) {
                    return DocType.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'doc-type',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('doc-type-detail.edit', {
            parent: 'doc-type-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/doc-type/doc-type-dialog.html',
                    controller: 'DocTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DocType', function(DocType) {
                            return DocType.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('doc-type.new', {
            parent: 'doc-type',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/doc-type/doc-type-dialog.html',
                    controller: 'DocTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                description: null,
                                formId: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('doc-type', null, { reload: 'doc-type' });
                }, function() {
                    $state.go('doc-type');
                });
            }]
        })
        .state('doc-type.edit', {
            parent: 'doc-type',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/doc-type/doc-type-dialog.html',
                    controller: 'DocTypeDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['DocType', function(DocType) {
                            return DocType.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('doc-type', null, { reload: 'doc-type' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('doc-type.delete', {
            parent: 'doc-type',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/doc-type/doc-type-delete-dialog.html',
                    controller: 'DocTypeDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['DocType', function(DocType) {
                            return DocType.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('doc-type', null, { reload: 'doc-type' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
