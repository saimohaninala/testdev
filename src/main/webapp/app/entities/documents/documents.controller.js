(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('DocumentsController', DocumentsController);

    DocumentsController.$inject = ['$scope', '$state', 'Documents'];

    function DocumentsController ($scope, $state, Documents) {
        var vm = this;

        vm.documents = [];

        loadAll();

        function loadAll() {
            Documents.query(function(result) {
                vm.documents = result;
                vm.searchQuery = null;
            });
        }
    }
})();
