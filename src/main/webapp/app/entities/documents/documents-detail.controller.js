(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('DocumentsDetailController', DocumentsDetailController);

    DocumentsDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Documents'];

    function DocumentsDetailController($scope, $rootScope, $stateParams, previousState, entity, Documents) {
        var vm = this;

        vm.documents = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:documentsUpdate', function(event, result) {
            vm.documents = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
