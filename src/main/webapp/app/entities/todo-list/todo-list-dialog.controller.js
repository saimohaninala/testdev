(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('TodoListDialogController', TodoListDialogController);

    TodoListDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'TodoList'];

    function TodoListDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, TodoList) {
        var vm = this;

        vm.todoList = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.todoList.id !== null) {
                TodoList.update(vm.todoList, onSaveSuccess, onSaveError);
            } else {
                TodoList.save(vm.todoList, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:todoListUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
