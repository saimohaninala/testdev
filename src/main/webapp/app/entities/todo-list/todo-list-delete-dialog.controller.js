(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('TodoListDeleteController',TodoListDeleteController);

    TodoListDeleteController.$inject = ['$uibModalInstance', 'entity', 'TodoList'];

    function TodoListDeleteController($uibModalInstance, entity, TodoList) {
        var vm = this;

        vm.todoList = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            TodoList.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
