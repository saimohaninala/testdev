(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('TodoListDetailController', TodoListDetailController);

    TodoListDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'TodoList'];

    function TodoListDetailController($scope, $rootScope, $stateParams, previousState, entity, TodoList) {
        var vm = this;

        vm.todoList = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:todoListUpdate', function(event, result) {
            vm.todoList = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
