(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('workflow', {
            parent: 'entity',
            url: '/workflow',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.workflow.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/workflow/workflows.html',
                    controller: 'WorkflowController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('workflow');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('workflow-detail', {
            parent: 'workflow',
            url: '/workflow/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.workflow.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/workflow/workflow-detail.html',
                    controller: 'WorkflowDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('workflow');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'Workflow', function($stateParams, Workflow) {
                    return Workflow.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'workflow',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('workflow-detail.edit', {
            parent: 'workflow-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/workflow/workflow-dialog.html',
                    controller: 'WorkflowDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Workflow', function(Workflow) {
                            return Workflow.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('workflow.new', {
            parent: 'workflow',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/workflow/workflow-dialog.html',
                    controller: 'WorkflowDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                application: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('workflow', null, { reload: 'workflow' });
                }, function() {
                    $state.go('workflow');
                });
            }]
        })
        .state('workflow.edit', {
            parent: 'workflow',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/workflow/workflow-dialog.html',
                    controller: 'WorkflowDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['Workflow', function(Workflow) {
                            return Workflow.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('workflow', null, { reload: 'workflow' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('workflow.delete', {
            parent: 'workflow',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/workflow/workflow-delete-dialog.html',
                    controller: 'WorkflowDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['Workflow', function(Workflow) {
                            return Workflow.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('workflow', null, { reload: 'workflow' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
