(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('RequestActivityController', RequestActivityController);

    RequestActivityController.$inject = ['$scope', '$state', 'RequestActivity'];

    function RequestActivityController ($scope, $state, RequestActivity) {
        var vm = this;

        vm.requestActivities = [];

        loadAll();

        function loadAll() {
            RequestActivity.query(function(result) {
                vm.requestActivities = result;
                vm.searchQuery = null;
            });
        }
    }
})();
