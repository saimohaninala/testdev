(function() {
    'use strict';
    angular
        .module('smartportalApp')
        .factory('RequestActivity', RequestActivity);

    RequestActivity.$inject = ['$resource'];

    function RequestActivity ($resource) {
        var resourceUrl =  'api/request-activities/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
