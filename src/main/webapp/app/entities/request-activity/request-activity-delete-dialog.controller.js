(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('RequestActivityDeleteController',RequestActivityDeleteController);

    RequestActivityDeleteController.$inject = ['$uibModalInstance', 'entity', 'RequestActivity'];

    function RequestActivityDeleteController($uibModalInstance, entity, RequestActivity) {
        var vm = this;

        vm.requestActivity = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            RequestActivity.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
