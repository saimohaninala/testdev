(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('BinderDialogController', BinderDialogController);

    BinderDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'Binder'];

    function BinderDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, Binder) {
        var vm = this;

        vm.binder = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.binder.id !== null) {
                Binder.update(vm.binder, onSaveSuccess, onSaveError);
            } else {
                Binder.save(vm.binder, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:binderUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
