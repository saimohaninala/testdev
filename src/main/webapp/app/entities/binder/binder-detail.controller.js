(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('BinderDetailController', BinderDetailController);

    BinderDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Binder'];

    function BinderDetailController($scope, $rootScope, $stateParams, previousState, entity, Binder) {
        var vm = this;

        vm.binder = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:binderUpdate', function(event, result) {
            vm.binder = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
