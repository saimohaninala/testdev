(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('SiteDetailController', SiteDetailController);

    SiteDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'Site'];

    function SiteDetailController($scope, $rootScope, $stateParams, previousState, entity, Site) {
        var vm = this;

        vm.site = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:siteUpdate', function(event, result) {
            vm.site = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
