(function() {
    'use strict';
    angular
        .module('smartportalApp')
        .factory('DocumentsRequest', DocumentsRequest);

    DocumentsRequest.$inject = ['$resource'];

    function DocumentsRequest ($resource) {
        var resourceUrl =  'api/documents-requests/:id';

        return $resource(resourceUrl, {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    if (data) {
                        data = angular.fromJson(data);
                    }
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    }
})();
