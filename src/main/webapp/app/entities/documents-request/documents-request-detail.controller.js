(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('DocumentsRequestDetailController', DocumentsRequestDetailController);

    DocumentsRequestDetailController.$inject = ['$scope', '$rootScope', '$stateParams', 'previousState', 'entity', 'DocumentsRequest'];

    function DocumentsRequestDetailController($scope, $rootScope, $stateParams, previousState, entity, DocumentsRequest) {
        var vm = this;

        vm.documentsRequest = entity;
        vm.previousState = previousState.name;

        var unsubscribe = $rootScope.$on('smartportalApp:documentsRequestUpdate', function(event, result) {
            vm.documentsRequest = result;
        });
        $scope.$on('$destroy', unsubscribe);
    }
})();
