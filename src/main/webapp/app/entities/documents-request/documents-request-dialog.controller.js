(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('DocumentsRequestDialogController', DocumentsRequestDialogController);

    DocumentsRequestDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'DocumentsRequest'];

    function DocumentsRequestDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, DocumentsRequest) {
        var vm = this;

        vm.documentsRequest = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.documentsRequest.id !== null) {
                DocumentsRequest.update(vm.documentsRequest, onSaveSuccess, onSaveError);
            } else {
                DocumentsRequest.save(vm.documentsRequest, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:documentsRequestUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
