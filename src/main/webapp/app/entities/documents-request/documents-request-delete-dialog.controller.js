(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('DocumentsRequestDeleteController',DocumentsRequestDeleteController);

    DocumentsRequestDeleteController.$inject = ['$uibModalInstance', 'entity', 'DocumentsRequest'];

    function DocumentsRequestDeleteController($uibModalInstance, entity, DocumentsRequest) {
        var vm = this;

        vm.documentsRequest = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            DocumentsRequest.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
