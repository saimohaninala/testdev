(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('SubstituteController', SubstituteController);

    SubstituteController.$inject = ['$scope', '$state', 'Substitute'];

    function SubstituteController ($scope, $state, Substitute) {
        var vm = this;

        vm.substitutes = [];

        loadAll();

        function loadAll() {
            Substitute.query(function(result) {
                vm.substitutes = result;
                vm.searchQuery = null;
            });
        }
    }
})();
