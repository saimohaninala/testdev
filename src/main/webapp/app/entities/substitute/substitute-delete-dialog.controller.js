(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('SubstituteDeleteController',SubstituteDeleteController);

    SubstituteDeleteController.$inject = ['$uibModalInstance', 'entity', 'Substitute'];

    function SubstituteDeleteController($uibModalInstance, entity, Substitute) {
        var vm = this;

        vm.substitute = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            Substitute.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
