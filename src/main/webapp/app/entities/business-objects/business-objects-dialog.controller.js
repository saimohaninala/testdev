(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('BusinessObjectsDialogController', BusinessObjectsDialogController);

    BusinessObjectsDialogController.$inject = ['$timeout', '$scope', '$stateParams', '$uibModalInstance', 'entity', 'BusinessObjects'];

    function BusinessObjectsDialogController ($timeout, $scope, $stateParams, $uibModalInstance, entity, BusinessObjects) {
        var vm = this;

        vm.businessObjects = entity;
        vm.clear = clear;
        vm.save = save;

        $timeout(function (){
            angular.element('.form-group:eq(1)>input').focus();
        });

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function save () {
            vm.isSaving = true;
            if (vm.businessObjects.id !== null) {
                BusinessObjects.update(vm.businessObjects, onSaveSuccess, onSaveError);
            } else {
                BusinessObjects.save(vm.businessObjects, onSaveSuccess, onSaveError);
            }
        }

        function onSaveSuccess (result) {
            $scope.$emit('smartportalApp:businessObjectsUpdate', result);
            $uibModalInstance.close(result);
            vm.isSaving = false;
        }

        function onSaveError () {
            vm.isSaving = false;
        }


    }
})();
