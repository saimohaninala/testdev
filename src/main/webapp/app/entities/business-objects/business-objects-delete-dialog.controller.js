(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .controller('BusinessObjectsDeleteController',BusinessObjectsDeleteController);

    BusinessObjectsDeleteController.$inject = ['$uibModalInstance', 'entity', 'BusinessObjects'];

    function BusinessObjectsDeleteController($uibModalInstance, entity, BusinessObjects) {
        var vm = this;

        vm.businessObjects = entity;
        vm.clear = clear;
        vm.confirmDelete = confirmDelete;

        function clear () {
            $uibModalInstance.dismiss('cancel');
        }

        function confirmDelete (id) {
            BusinessObjects.delete({id: id},
                function () {
                    $uibModalInstance.close(true);
                });
        }
    }
})();
