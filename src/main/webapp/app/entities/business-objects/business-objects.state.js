(function() {
    'use strict';

    angular
        .module('smartportalApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider
        .state('business-objects', {
            parent: 'entity',
            url: '/business-objects?page&sort&search',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.businessObjects.home.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/business-objects/business-objects.html',
                    controller: 'BusinessObjectsController',
                    controllerAs: 'vm'
                }
            },
            params: {
                page: {
                    value: '1',
                    squash: true
                },
                sort: {
                    value: 'id,asc',
                    squash: true
                },
                search: null
            },
            resolve: {
                pagingParams: ['$stateParams', 'PaginationUtil', function ($stateParams, PaginationUtil) {
                    return {
                        page: PaginationUtil.parsePage($stateParams.page),
                        sort: $stateParams.sort,
                        predicate: PaginationUtil.parsePredicate($stateParams.sort),
                        ascending: PaginationUtil.parseAscending($stateParams.sort),
                        search: $stateParams.search
                    };
                }],
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('businessObjects');
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        })
        .state('business-objects-detail', {
            parent: 'business-objects',
            url: '/business-objects/{id}',
            data: {
                authorities: ['ROLE_USER'],
                pageTitle: 'smartportalApp.businessObjects.detail.title'
            },
            views: {
                'content@': {
                    templateUrl: 'app/entities/business-objects/business-objects-detail.html',
                    controller: 'BusinessObjectsDetailController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                translatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                    $translatePartialLoader.addPart('businessObjects');
                    return $translate.refresh();
                }],
                entity: ['$stateParams', 'BusinessObjects', function($stateParams, BusinessObjects) {
                    return BusinessObjects.get({id : $stateParams.id}).$promise;
                }],
                previousState: ["$state", function ($state) {
                    var currentStateData = {
                        name: $state.current.name || 'business-objects',
                        params: $state.params,
                        url: $state.href($state.current.name, $state.params)
                    };
                    return currentStateData;
                }]
            }
        })
        .state('business-objects-detail.edit', {
            parent: 'business-objects-detail',
            url: '/detail/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/business-objects/business-objects-dialog.html',
                    controller: 'BusinessObjectsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['BusinessObjects', function(BusinessObjects) {
                            return BusinessObjects.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('^', {}, { reload: false });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('business-objects.new', {
            parent: 'business-objects',
            url: '/new',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/business-objects/business-objects-dialog.html',
                    controller: 'BusinessObjectsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: function () {
                            return {
                                name: null,
                                description: null,
                                id: null
                            };
                        }
                    }
                }).result.then(function() {
                    $state.go('business-objects', null, { reload: 'business-objects' });
                }, function() {
                    $state.go('business-objects');
                });
            }]
        })
        .state('business-objects.edit', {
            parent: 'business-objects',
            url: '/{id}/edit',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/business-objects/business-objects-dialog.html',
                    controller: 'BusinessObjectsDialogController',
                    controllerAs: 'vm',
                    backdrop: 'static',
                    size: 'lg',
                    resolve: {
                        entity: ['BusinessObjects', function(BusinessObjects) {
                            return BusinessObjects.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('business-objects', null, { reload: 'business-objects' });
                }, function() {
                    $state.go('^');
                });
            }]
        })
        .state('business-objects.delete', {
            parent: 'business-objects',
            url: '/{id}/delete',
            data: {
                authorities: ['ROLE_USER']
            },
            onEnter: ['$stateParams', '$state', '$uibModal', function($stateParams, $state, $uibModal) {
                $uibModal.open({
                    templateUrl: 'app/entities/business-objects/business-objects-delete-dialog.html',
                    controller: 'BusinessObjectsDeleteController',
                    controllerAs: 'vm',
                    size: 'md',
                    resolve: {
                        entity: ['BusinessObjects', function(BusinessObjects) {
                            return BusinessObjects.get({id : $stateParams.id}).$promise;
                        }]
                    }
                }).result.then(function() {
                    $state.go('business-objects', null, { reload: 'business-objects' });
                }, function() {
                    $state.go('^');
                });
            }]
        });
    }

})();
