(function () {
    'use strict';
    // DO NOT EDIT THIS FILE, EDIT THE GULP TASK NGCONSTANT SETTINGS INSTEAD WHICH GENERATES THIS FILE
    angular
        .module('smartportalApp')
        .constant('VERSION', "2.0.3")
        .constant('DEBUG_INFO_ENABLED', true)
;
})();
